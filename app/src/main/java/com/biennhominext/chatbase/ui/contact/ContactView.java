package com.biennhominext.chatbase.ui.contact;

import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BaseView;

/**
 * Created by bien on 7/19/2017.
 */

public interface ContactView extends BaseView {
    void addContact(User user);
}
