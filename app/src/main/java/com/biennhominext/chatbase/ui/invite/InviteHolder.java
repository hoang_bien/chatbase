package com.biennhominext.chatbase.ui.invite;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.data.model.User;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by hbien on 8/10/2017.
 */

public class InviteHolder extends RecyclerView.ViewHolder {
    private TextView mTitleTextView;
    private SimpleDraweeView mAvatarView;
    private TextView mStatusTextView;
    private TextView mMessageTextView;
    private Button mIgnoreButton;
    private Button mAcceptButton;

    public InviteHolder(View itemView) {
        super(itemView);
        mTitleTextView = itemView.findViewById(R.id.text_title);
        mAvatarView = itemView.findViewById(R.id.image_avatar);
        mStatusTextView = itemView.findViewById(R.id.text_status);
        mMessageTextView = itemView.findViewById(R.id.text_message);
        mIgnoreButton = itemView.findViewById(R.id.btn_ignore);
        mAcceptButton = itemView.findViewById(R.id.btn_accept);
    }

    public void bind(Message message, InviteAdapter.OnItemInviteClickListener mListener) {
        Context context = itemView.getContext();
        User user = message.getUser();
        mAvatarView.setImageURI(user.getProfileImage());
        mTitleTextView.setText(user.getName());
        String content = null;
        String status = null;
        switch (message.getMessageType()) {
            case Message.TYPE_MEDIA_IMAGE:
                status = context.getString(R.string.invites_image_message_label);
                content = message.getContent();
                break;
            case Message.TYPE_STICKER:
                status = context.getString(R.string.invites_sticker_message_label);
                content = context.getString(R.string.invites_view_message_label);
                break;
            case Message.TYPE_LOCATION:
                status = context.getString(R.string.invites_location_message_label);
                content = context.getString(R.string.invites_view_message_label);
                break;
            case Message.TYPE_PLAIN_TEXT:
                status = context.getString(R.string.invites_text_message_label);
                content = context.getString(R.string.invites_view_message_label);
                break;
        }
        mStatusTextView.setText(status);
        mMessageTextView.setText(content);

        if (message.getMessageType().equals(Message.TYPE_PLAIN_TEXT)) {
            mStatusTextView.setText(context.getString(R.string.invites_text_message_label));
            mMessageTextView.setText(message.getContent());
        } else if (message.getMessageType().equals(Message.TYPE_MEDIA_IMAGE)) {
            mStatusTextView.setText(context.getString(R.string.invites_image_message_label));
            mMessageTextView.setText(context.getString(R.string.invites_view_message_label));
        }

        itemView.setOnClickListener(view -> {
            if (mListener != null) {
                mListener.onItemClick(message);
            }
        });

        mIgnoreButton.setOnClickListener(view -> {
            if (mListener != null) {
                mListener.onItemIgnoreClick(message, getAdapterPosition());
            }
        });

        mAcceptButton.setOnClickListener(view -> {
            if (mListener != null) {
                mListener.onItemAcceptClick(message, getAdapterPosition());
            }
        });

    }
}
