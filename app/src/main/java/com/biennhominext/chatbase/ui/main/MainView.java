package com.biennhominext.chatbase.ui.main;

import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BaseView;

/**
 * Created by bien on 7/17/2017.
 */

public interface MainView extends BaseView {
    void openLoginActivity();
    void openProfileEditActivity();
    void updateUserAccount(User user);
    void setInviteBadgeCount(long count);
}
