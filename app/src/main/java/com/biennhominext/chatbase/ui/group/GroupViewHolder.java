package com.biennhominext.chatbase.ui.group;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.custom.MultiDraweeView;

import java.util.ArrayList;

/**
 * Created by hbien on 7/31/2017.
 */

public class GroupViewHolder extends RecyclerView.ViewHolder {
    MultiDraweeView avatarDraweeView;
    TextView titleTextView;
    TextView peopleTextView;
    TextView lastActiveTextView;

    public GroupViewHolder(View itemView) {
        super(itemView);
        avatarDraweeView = itemView.findViewById(R.id.image_group_avatar);
        titleTextView = itemView.findViewById(R.id.text_group_title);
        peopleTextView = itemView.findViewById(R.id.text_group_member);
        lastActiveTextView = itemView.findViewById(R.id.text_last_active);
    }

    public void bind(Group group) {
        StringBuffer buffer = new StringBuffer();
        ArrayList<String> list = new ArrayList<>(3);
        int i = 0;
        for (User user : group.getUsers().values()) {
            buffer.append(user.getName() + ", ");
            if (i < 3) {
                list.add(user.getProfileImage());
                i++;
            }
        }
        if (buffer.length() > 0) {
            buffer.setLength(buffer.length() - 2);
        }
        avatarDraweeView.setImageUris(list);
        titleTextView.setText(group.getTitle());
        peopleTextView.setText(buffer.toString());
        Context context = lastActiveTextView.getContext();
        if (System.currentTimeMillis() - group.getTimestamp() < DateUtils.MINUTE_IN_MILLIS) {

            lastActiveTextView.setText(context.getString(R.string.format_group_last_active, "now"));
        } else {
            String timeRelative = DateUtils.getRelativeTimeSpanString(
                    group.getTimestamp(), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS).toString();

            lastActiveTextView.setText(context.getString(R.string.format_group_last_active, timeRelative));
        }
    }
}
