package com.biennhominext.chatbase.ui.group.addmember;

import com.biennhominext.chatbase.ui.base.BasePresenter;

import java.util.List;

/**
 * Created by hbien on 7/29/2017.
 */

public interface AddMemberPresenter<V extends AddMemberView> extends BasePresenter<V>{

    void init(String groupId, boolean isFirstCreated);

    void addMembers(List<String> memberIds);
}
