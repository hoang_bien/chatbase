package com.biennhominext.chatbase.ui.group.addmember;

import android.util.Log;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by hbien on 7/29/2017.
 */

public class AddMemberPresenterImpl<V extends AddMemberView> extends BasePresenterImpl<V> implements AddMemberPresenter<V> {
    private String mGroupId;
    private boolean mIsNewlyCreated;

    @Inject
    public AddMemberPresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dataManager, scheduler, compositeDisposable);
    }

    @Override
    public void init(String groupId, boolean isNewlyCreated) {
        this.mGroupId = groupId;
        mIsNewlyCreated = isNewlyCreated;
    }

    @Override
    public void addMembers(List<String> memberIds) {
        getDataManager().addMembersToGroup(memberIds, mGroupId, mIsNewlyCreated);
        getView().toConversationActivity();
    }

    @Override
    public void onAttach(V view) {
        super.onAttach(view);
        if (mGroupId == null) {
            Log.d(AddMemberPresenterImpl.class.getSimpleName(), "Group id not exists");
            return;
        }

        getDisposable().add(getDataManager().getGroupInfo(mGroupId)
                .subscribeOn(getScheduler().io())
                .observeOn(getScheduler().ui())
                .subscribe(getView()::updateGroupInfo));

        getDisposable().add(getDataManager().getContactsNotInGroup(mGroupId).subscribeOn(getScheduler().computation())
                .observeOn(getScheduler().ui())
                .subscribe(getView()::addUser));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
