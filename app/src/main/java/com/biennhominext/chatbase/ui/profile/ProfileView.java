package com.biennhominext.chatbase.ui.profile;

import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BaseView;

/**
 * Created by bien on 7/20/2017.
 */

public interface ProfileView extends BaseView {
    void updateUserInfo(User user);

    void toLogin();

}
