package com.biennhominext.chatbase.ui.conversation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.utils.Dates;
import com.facebook.drawee.view.SimpleDraweeView;

import timber.log.Timber;

/**
 * Created by hbien on 8/19/2017.
 */

public class ConversationNotificationHolder extends RecyclerView.ViewHolder {
    TextView mNotificationTextView;
    TextView mTimeTextView;
    SimpleDraweeView mAvatar;
    private Context mContext;

    public ConversationNotificationHolder(View itemView) {
        super(itemView);
        mNotificationTextView = itemView.findViewById(R.id.text_notification);
        mTimeTextView = itemView.findViewById(R.id.text_timestamp);
        mAvatar = itemView.findViewById(R.id.image_avatar);
        mContext = itemView.getContext();
    }

    public void bind(Message message) {
        mAvatar.setVisibility(View.GONE);
        mTimeTextView.setVisibility(View.VISIBLE);
        mTimeTextView.setText(Dates.getDateAndTime(mContext, message.getTimestamp()));
        if (message.getMessageType().equals(Message.TYPE_ACTION_CREATE_GROUP)) {
            String userName = message.getUser() == null ?
                    mContext.getString(R.string.conversation_self_label) : message.getUser().getName();
            mNotificationTextView.setText(mContext.getString(R.string.format_message_create_group, userName));
        } else if (message.getMessageType().equals(Message.TYPE_ACTION_LEAVE_GROUP)) {
            String userName = message.getUser() == null ?
                    mContext.getString(R.string.conversation_self_label) : message.getUser().getName();
            mNotificationTextView.setText(mContext.getString(R.string.format_message_leave_group, userName));
        } else if (message.getMessageType().equals(Message.TYPE_ACTION_ADD_MEMBER)) {
            Timber.i(message.getContent());
            mAvatar.setVisibility(View.VISIBLE);
            mTimeTextView.setVisibility(View.GONE);
            String userName = message.getUser() == null ?
                    mContext.getString(R.string.conversation_self_label) : message.getUser().getName();
            final User user = (User) message.getPayload();
            mAvatar.setImageURI(user.getProfileImage());
            mNotificationTextView.setText(mContext.getString(R.string.format_message_add_member, userName, user.getName()));
        }
    }
}
