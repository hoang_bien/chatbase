package com.biennhominext.chatbase.ui.findcontact;

import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.OnItemClickListener;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hbien on 8/2/2017.
 */

public class ContactResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int VIEW_TYPE_TITLE = 1;
    public static final int VIEW_TYPE_CONTENT = 2;
    private List<User> mFriendList;
    private List<User> mUserList;
    private OnItemClickListener mListener;

    public ContactResultAdapter() {
        mUserList = new ArrayList<>();
        mFriendList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_TITLE:
                View titleView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_find_contact_title, parent, false);
                return new TitleResultHolder(titleView);
            case VIEW_TYPE_CONTENT:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_find_contact_result, parent, false);
                ContactResultHolder contactResultHolder = new ContactResultHolder(view);
                return contactResultHolder;
            default:
                break;
        }
        return null;
    }

    public void setListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final int friendCount = mFriendList.size();
        if (holder instanceof TitleResultHolder) {
            TitleResultHolder titleResultHolder = (TitleResultHolder) holder;
            if (position == 0 && friendCount != 0) {
                titleResultHolder.bind(R.string.find_contact_result_your_contacts_title);
            } else {
                titleResultHolder.bind(R.string.find_contact_result_public_user_title);
            }
        } else {
            ContactResultHolder contactResultHolder = (ContactResultHolder) holder;
            if (friendCount == 0) {
                contactResultHolder.bind(mUserList.get(position - 1), mListener);
            } else if (friendCount != 0 && position <= friendCount) {
                contactResultHolder.bind(mFriendList.get(position - 1), mListener);
            } else {
                contactResultHolder.bind(mUserList.get(position - 2 - friendCount), mListener);
            }
        }
    }

    @Override
    public int getItemCount() {
        final int friendSize = mFriendList.size();
        final int userSize = mUserList.size();
        final int shouldHasFriendTitle = friendSize > 0 ? 1 : 0;
        final int shouldHasUserTitle = userSize > 0 ? 1 : 0;
        return mUserList.size() + mFriendList.size() + shouldHasFriendTitle + shouldHasUserTitle;
    }

    @Override
    public int getItemViewType(int position) {
        final int friendCount = mFriendList.size();
        final int userCount = mUserList.size();
        if (friendCount != 0 && userCount != 0) {
            return (position == 0 || position == friendCount + 1) ? VIEW_TYPE_TITLE : VIEW_TYPE_CONTENT;
        } else {
            return position == 0 ? VIEW_TYPE_TITLE : VIEW_TYPE_CONTENT;
        }
    }

    public void addUser(User user) {
        if (user.getFriendStatus() == null || user.getFriendStatus() != User.FLAG_FRIEND) {
            //            final int friendCount = mFriendList.size();
//            final int offset = friendCount == 0 ? 1 : friendCount + 2;
            int i;
            for (i = 0; i < mUserList.size(); i++) {
                if (mUserList.get(i).equals(user)) {
                    mUserList.set(i, user);
                    break;
                }
            }
            if (i == mUserList.size()) {
                mUserList.add(user);
            }
        } else {
            int i;
            for (i = 0; i < mFriendList.size(); i++) {
                if (mFriendList.get(i).equals(user)) {
                    mFriendList.set(i, user);
                    break;
                }
            }
            if (i == mFriendList.size()) {
                mFriendList.add(user);
            }

        }
        notifyDataSetChanged();

    }

    public void clear() {
        mUserList.clear();
        mFriendList.clear();
        notifyDataSetChanged();
    }


    public static class ContactResultHolder extends RecyclerView.ViewHolder {
        TextView mNameTextView;
        TextView mEmailTextView;
        SimpleDraweeView mAvatarImageView;

        public ContactResultHolder(View itemView) {
            super(itemView);
            mNameTextView = itemView.findViewById(R.id.text_name);
            mEmailTextView = itemView.findViewById(R.id.text_email);
            mAvatarImageView = itemView.findViewById(R.id.image_avatar);
        }

        public void bind(User user, OnItemClickListener listener) {
            mNameTextView.setText(user.getName());
            mEmailTextView.setText(user.getEmail());
            mAvatarImageView.setImageURI(user.getProfileImage());
            itemView.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onItemClick(user.getUid(), 0 /* unused*/);
                }
            });
        }
    }

    public static class TitleResultHolder extends RecyclerView.ViewHolder {
        public TitleResultHolder(View itemView) {
            super(itemView);
        }

        public void bind(@StringRes int resId) {
            String text = itemView.getContext().getString(resId);
            ((TextView) itemView).setText(text);

        }
    }


}
