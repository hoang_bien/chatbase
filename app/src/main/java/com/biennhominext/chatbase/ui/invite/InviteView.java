package com.biennhominext.chatbase.ui.invite;

import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.ui.base.BaseView;

/**
 * Created by hbien on 8/10/2017.
 */

public interface InviteView extends BaseView {
    void showInviteMessage(Message message);
}
