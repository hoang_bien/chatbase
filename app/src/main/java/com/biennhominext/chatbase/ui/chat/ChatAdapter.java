package com.biennhominext.chatbase.ui.chat;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.custom.AvatarDraweeView;
import com.biennhominext.chatbase.ui.custom.MultiDraweeView;
import com.biennhominext.chatbase.utils.Dates;

import java.util.ArrayList;

/**
 * Created by bien on 7/18/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_USER_MESSAGE = 0;
    private static final int VIEW_TYPE_GROUP_MESSAGE = 1;
    private SortedList<Message> mMessageList;
    private OnItemClickListener mListener;
    private String mCurrentUserId;


    public ChatAdapter(String currentUserId) {
        mCurrentUserId = currentUserId;
        mMessageList = new SortedList<Message>(Message.class, new SortedList.Callback<Message>() {
            @Override
            public int compare(Message o1, Message o2) {
                return Long.compare(o2.getTimestamp(), o1.getTimestamp());
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(Message oldItem, Message newItem) {
                return oldItem.getTimestamp() == newItem.getTimestamp();
            }

            @Override
            public boolean areItemsTheSame(Message item1, Message item2) {
                return item1.getDestination().equals(item2.getDestination());
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);

        ChatHolder holder = new ChatHolder(view);

        return holder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ChatHolder holder = ((ChatHolder) viewHolder);
        Context context = holder.itemView.getContext();

        Message message = mMessageList.get(position);
        if (message.getPayload() instanceof User) {
            final User sender = (User) message.getPayload();
            if (!TextUtils.isEmpty(sender.getProfileImage())) {
                holder.mUserAvatarView.setVisibility(View.VISIBLE);
                holder.mGroupAvatarView.setVisibility(View.GONE);
//                final ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(sender.getProfileImage()))
//                        .setPostprocessor(new CircleDotProcessor())
//                        .build();
//
//                final DraweeController draweeController = Fresco.newDraweeControllerBuilder()
//                        .setOldController(holder.mUserAvatarView.getController())
//                        .setImageRequest(imageRequest)
//                        .build();
                holder.mUserAvatarView.setImageUri(sender.getProfileImage());
                holder.mUserAvatarView.setOnlineStatus(sender.isOnline());


            }

            holder.titleTextView.setText(sender.getName());
        } else if (message.getPayload() instanceof Group) {
            Group group = (Group) message.getPayload();
            holder.mUserAvatarView.setVisibility(View.GONE);
            holder.mGroupAvatarView.setVisibility(View.VISIBLE);

            ArrayList<String> list = new ArrayList<>(3);
            int i = 0;
            for (User user : group.getUsers().values()) {
                if (i < 3) {
                    list.add(user.getProfileImage());
                    i++;
                } else {
                    break;
                }
            }

            holder.mGroupAvatarView.setImageUris(list);
            holder.titleTextView.setText(group.getTitle());
        }

        // set message content
        if (!message.isUnread()) {
            holder.titleTextView.setTypeface(null, Typeface.NORMAL);
            holder.messageTextView.setTypeface(null, Typeface.NORMAL);
            holder.titleTextView.setTextColor(ContextCompat.getColor(context, R.color.colorTextPrimary));
            holder.messageTextView.setTextColor(ContextCompat.getColor(context, R.color.colorTextSecondary));
        } else {
            holder.titleTextView.setTypeface(null, Typeface.BOLD);
            holder.messageTextView.setTypeface(null, Typeface.BOLD);
            holder.titleTextView.setTextColor(Color.BLACK);
            holder.messageTextView.setTextColor(Color.BLACK);
        }
        String userName = message.getSender().equals(mCurrentUserId) ?
                context.getString(R.string.conversation_self_label) : message.getUser().getName();
        String content = null;
        switch (message.getMessageType()) {
            case Message.TYPE_MEDIA_IMAGE:
                content = context.getString(R.string.format_image_meessage_description, userName);
                break;
            case Message.TYPE_STICKER:
                content = context.getString(R.string.format_sticker_message_description, userName);
                break;
            case Message.TYPE_LOCATION:
                content = context.getString(R.string.format_location_message_description);
                break;
            case Message.TYPE_ACTION_CREATE_GROUP:
                content = context.getString(R.string.format_message_create_group, userName);
                break;
            case Message.TYPE_ACTION_ADD_MEMBER:
                content = context.getString(R.string.format_message_add_member_description, userName);
                break;
            case Message.TYPE_ACTION_LEAVE_GROUP:
                content = context.getString(R.string.format_message_leave_group, userName);
                break;
            case Message.TYPE_PLAIN_TEXT:
                if (message.getPayload().getUid().equals(message.getSender())) {
                    content = message.getContent();
                } else {
                    content = userName + ": " + message.getContent();
                }
                break;
        }

        holder.messageTextView.setText(content);
        holder.timeTextView.setText(Dates.getDateAndTime(holder.itemView.getContext(), message.getTimestamp()));
        holder.itemView.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onItemClick(mMessageList.get(holder.getAdapterPosition()));
            }
        });


    }


    public void setListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    public void addMessage(Message msg) {
        mMessageList.add(msg);
    }



    @Override
    public int getItemCount() {
        return mMessageList == null ? 0 : mMessageList.size();
    }


    public void clear() {
        mMessageList.clear();
        notifyDataSetChanged();
    }


    public static class ChatHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        TextView timeTextView;
        TextView messageTextView;
        MultiDraweeView mGroupAvatarView;
        AvatarDraweeView mUserAvatarView;

        public ChatHolder(View itemView) {
            super(itemView);
            messageTextView = itemView.findViewById(R.id.text_message);
            timeTextView = itemView.findViewById(R.id.text_status);
            titleTextView = itemView.findViewById(R.id.text_title);
            mUserAvatarView = itemView.findViewById(R.id.image_user_avatar);
            mGroupAvatarView = itemView.findViewById(R.id.image_group_avatar);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Message msg);
    }
}
