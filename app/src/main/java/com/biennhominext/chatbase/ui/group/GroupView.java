package com.biennhominext.chatbase.ui.group;

import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.ui.base.BaseView;

/**
 * Created by hbien on 7/28/2017.
 */

public interface GroupView extends BaseView {
    void addGroup(Group group);
}
