package com.biennhominext.chatbase.ui.main;

import android.text.TextUtils;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.data.exception.UnInitializedProfileException;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;
import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 7/17/2017.
 */

public class MainPresenterImpl<V extends MainView> extends BasePresenterImpl<V> implements MainPresenter<V> {
    String mFirebaseToken;

    @Inject
    public MainPresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dataManager, scheduler, compositeDisposable);
        mFirebaseToken = FirebaseInstanceId.getInstance().getToken();

    }

    @Override
    public void onPreparedAccount() {
        if (!getDataManager().isUserLogined()) {
            getView().openLoginActivity();
        } else {
            getDisposable().add(getDataManager().getAccountInfo()
                    .observeOn(getScheduler().io())
                    .subscribeOn(getScheduler().ui()).subscribe(
                            user -> {
                                getView().updateUserAccount(user);
                                updateToken();
                            }, e -> {
                                if (e instanceof UnInitializedProfileException) {
                                    getView().openProfileEditActivity();
                                } else {
                                    getView().onError(e.getMessage());
                                }
                            }
                    ));

            getDisposable().add(getDataManager().getCurrentInvitesCount()
                    .observeOn(getScheduler().io())
                    .subscribeOn(getScheduler().ui())
                    .subscribe(getView()::setInviteBadgeCount));
        }
    }

    private void updateToken() {
        getDataManager().initializeOnlinePresence();
        getDataManager().getMyToken()
                .subscribeOn(getScheduler().io())
                .subscribe(token -> {
                    if (TextUtils.isEmpty(token) || !token.equals(mFirebaseToken)) {
                        getDataManager().setMyToken(mFirebaseToken);
                    }
                });
    }
}
