package com.biennhominext.chatbase.ui.conversation;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biennhominext.chatbase.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

/**
 * Created by hbien on 7/26/2017.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageHolder> {
    private Context mContext;
    private String[] mPaths;
    private OnImageItemClickListener mListener;

    public ImageAdapter(Context c, String[] paths) {
        mContext = c;
        this.mPaths = paths;
    }

    public void setListener(OnImageItemClickListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        final String path = mPaths[position];
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(path))
                .setLocalThumbnailPreviewsEnabled(true)
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .setOldController(((SimpleDraweeView) holder.itemView).getController())
                .build();
        ViewCompat.setTransitionName(holder.itemView, mPaths[position]);
        ((SimpleDraweeView) holder.itemView).setController(controller);
        holder.itemView.setOnClickListener(view -> {
            if (mListener != null) {
                mListener.onImageItemClick(mPaths[position], holder.itemView);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPaths.length;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View imageView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_image_, parent, false);
        ImageHolder holder = new ImageHolder(imageView);
        holder.setIsRecyclable(false);

        return holder;
    }


    public static class ImageHolder extends RecyclerView.ViewHolder {

        public ImageHolder(View itemView) {
            super(itemView);
        }
    }


}
