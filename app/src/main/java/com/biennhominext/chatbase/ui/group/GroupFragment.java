package com.biennhominext.chatbase.ui.group;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.di.component.ActivityComponent;
import com.biennhominext.chatbase.ui.OnItemClickListener;
import com.biennhominext.chatbase.ui.base.BaseFragment;
import com.biennhominext.chatbase.ui.conversation.ConversationActivity;
import com.biennhominext.chatbase.utils.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hbien on 7/28/2017.
 */

public class GroupFragment extends BaseFragment implements GroupView, OnItemClickListener {

    @BindView(R.id.recycler_group)
    RecyclerView mGroupList;
    @BindView(R.id.viewgroup_empty)
    View mEmptyView;
    @Inject
    GroupPresenter<GroupView> mPresenter;
    @Inject
    GroupAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group, container, false);
        ActivityComponent activityComponent = getActivityComponent();
        if (activityComponent != null) {
            activityComponent.inject(this);
            setUnBinder(ButterKnife.bind(this, view));

        }
        mGroupList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mGroupList.setAdapter(mAdapter);
        mAdapter.setListener(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapter.clear();
        UiUtils.showIfInvisible(mEmptyView);
        mPresenter.onAttach(this);
    }

    @Override
    public void onStop() {
        mPresenter.onDetach();
        super.onStop();
    }

    @Override
    public void addGroup(Group group) {
        if (mEmptyView.getVisibility() == View.VISIBLE){
            mEmptyView.setVisibility(View.GONE);
        }
        mAdapter.addGroup(group);
    }

    @Override
    public void onItemClick(Object object, int position) {
        Group group = (Group) object;
        Intent intent = new Intent(getActivity(), ConversationActivity.class);
        intent.putExtra(ConversationActivity.EXTRA_ID, group.getUid());
        intent.putExtra(ConversationActivity.EXTRA_IS_GROUP, true);
        startActivity(intent);
    }
}
