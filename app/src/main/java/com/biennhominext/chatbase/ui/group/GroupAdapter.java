package com.biennhominext.chatbase.ui.group;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.ui.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hbien on 7/31/2017.
 */

public class GroupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Group> mGroupList;
    private OnItemClickListener mListener;

    public GroupAdapter(){
        mGroupList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group,parent,false);
        GroupViewHolder holder = new GroupViewHolder(view);
        view.setOnClickListener(view1 -> {
            if (mListener != null) {
                int position = holder.getAdapterPosition();
                mListener.onItemClick(mGroupList.get(position),position);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((GroupViewHolder) holder).bind(mGroupList.get(position));
    }

    public void setListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    public void addGroup(Group group){
        int i;
        for (i = 0; i < mGroupList.size(); i++) {
            if (mGroupList.get(i).equals(group)){
                mGroupList.set(i,group);
                notifyItemChanged(i);
                break;
            }
        }
        if (i == mGroupList.size()){
            mGroupList.add(group);
            notifyItemInserted(i);
        }
    }

    @Override
    public int getItemCount() {
        return mGroupList.size();
    }

    public void clear() {
        mGroupList.clear();
        notifyDataSetChanged();
    }
}
