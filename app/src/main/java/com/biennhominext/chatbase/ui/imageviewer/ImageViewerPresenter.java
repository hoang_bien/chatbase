package com.biennhominext.chatbase.ui.imageviewer;

import com.biennhominext.chatbase.ui.base.BasePresenter;

/**
 * Created by hbien on 8/9/2017.
 */

public interface ImageViewerPresenter<V extends ImageViewerView> extends BasePresenter<V> {

}
