package com.biennhominext.chatbase.ui.imageviewer;

import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.SharedElementCallback;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.transition.AutoTransition;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.ui.base.BaseActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.photodraweeview.PhotoDraweeView;

/**
 * Created by hbien on 8/9/2017.
 */

public class ImageViewerActivity extends BaseActivity implements ImageViewerView {
    public static final String ARG_CONVERSATION_NAME = "sender";
    public static final String ARG_IMAGE_URI = "image_uri";
    public static final String ARG_TRANSITION_NAME = "transition_name";
    @BindView(R.id.image)
    PhotoDraweeView mImageView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @Inject
    ImageViewerPresenter<ImageViewerView> mPresentner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWindowTransitions();
        setContentView(R.layout.activity_image_viewer);
        setUnbinder(ButterKnife.bind(this));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String imageTransitionName = getIntent().getStringExtra(ARG_TRANSITION_NAME);
        ViewCompat.setTransitionName(mImageView, imageTransitionName);

        getActivityComponent().inject(this);
        String imageUri = getIntent().getStringExtra(ARG_IMAGE_URI);
        String senderName = getIntent().getStringExtra(ARG_CONVERSATION_NAME);
        setTitle(senderName);

        mImageView.setImageURI(imageUri);
        mPresentner.onAttach(this);
        mImageView.setPhotoUri(Uri.parse(imageUri));

        hideToolbarWithDelay();
        mImageView.setOnPhotoTapListener((view, x, y) -> {
            showToolbar();
            hideToolbarWithDelay();
        });

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void initWindowTransitions() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            AutoTransition transition = null;
            transition = new AutoTransition();

            getWindow().setSharedElementEnterTransition(transition);
            getWindow().setSharedElementExitTransition(transition);
            ActivityCompat.setEnterSharedElementCallback(this, new SharedElementCallback() {
                @Override
                public void onSharedElementEnd(List<String> sharedElementNames,
                                               List<View> sharedElements, List<View> sharedElementSnapshots) {
                    for (final View view : sharedElements) {
                        if (view instanceof PhotoDraweeView) {
                            ((PhotoDraweeView) view).setScale(1f, true);
                        }
                    }
                }
            });
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return (super.onOptionsItemSelected(item));
    }

    private void hideToolbarWithDelay() {
        Handler handler = new Handler();
        handler.postDelayed(this::hideToolbar, 3000);
    }

    private void hideToolbar() {
        if (mToolbar == null) {
            return;
        }
        mToolbar.animate().translationY(-mToolbar.getBottom())
                .setInterpolator(new AccelerateInterpolator()).start();
    }

    private void showToolbar() {
        mToolbar.animate().translationY(0)
                .setInterpolator(new AccelerateInterpolator()).start();
    }

    @Override
    protected void onDestroy() {
        mToolbar.clearAnimation();
        super.onDestroy();
    }
}
