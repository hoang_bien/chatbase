package com.biennhominext.chatbase.ui.group.addmember;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.User;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by hbien on 7/30/2017.
 */

public class MemberPickerAdapter extends RecyclerView.Adapter<MemberPickerAdapter.MemberPickerHolder> {
    private List<User> mUserList;
    private HashSet<String> mSelectionList;
    private OnItemCheckedChangeListener mListener;

    public MemberPickerAdapter() {
        mUserList = new ArrayList<>();
        mSelectionList = new HashSet<>();
    }

    @Override
    public MemberPickerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_member_picker,parent,false);
        MemberPickerHolder holder = new MemberPickerHolder(view);
        holder.itemView.setOnClickListener(v->{
            boolean val = !holder.mUserCheckbox.isChecked();
            int pos = holder.getAdapterPosition();
            String id = mUserList.get(pos).getUid();
            toggleChecked(pos,id,val);
        });
        return holder;
    }

    private void toggleChecked(int adapterPosition,String uid, boolean val) {
        if (val){
            mSelectionList.add(uid);
        }else {
            mSelectionList.remove(uid);
        }
        notifyItemChanged(adapterPosition);
        if (mListener != null) {
            mListener.onItemCheckedChanged(mUserList.get(adapterPosition),val);
        }

    }

    public void setmListener(OnItemCheckedChangeListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onBindViewHolder(MemberPickerHolder holder, int position) {
        User user = mUserList.get(position);
        holder.bind(user,mSelectionList.contains(user.getUid()));
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public void add(User user) {
        int i;
        for (i = 0; i < mUserList.size(); i++) {
            if (mUserList.get(i).equals(user)){
                mUserList.set(i,user);
                notifyItemChanged(i);
                break;
            }
        }
        if (i == mUserList.size()){
            mUserList.add(user);
            notifyItemInserted(i);
        }
    }

    public void setItemSelected(Object id, boolean checked) {
        if (checked){
            mSelectionList.add(id.toString());
        }else {
            mSelectionList.remove(id.toString());
        }
        for (int i = 0; i < mUserList.size(); i++) {
            if (mUserList.get(i).getUid().equals(id)){
                notifyItemChanged(i);
            }
        }
    }

    public static class MemberPickerHolder  extends RecyclerView.ViewHolder {
        TextView mNameTextView;
        TextView mEmailTextView;
        SimpleDraweeView mAvatarImageView;
        CheckBox mUserCheckbox;

        public MemberPickerHolder(View itemView) {
            super(itemView);
            mNameTextView = itemView.findViewById(R.id.text_name);
            mEmailTextView = itemView.findViewById(R.id.text_email);
            mAvatarImageView = itemView.findViewById(R.id.image_avatar);
            mUserCheckbox = itemView.findViewById(R.id.checkbox_user);
        }

        public void bind(User user, boolean selected){
            mNameTextView.setText(user.getName());
            mEmailTextView.setText(user.getEmail());
            mAvatarImageView.setImageURI(user.getProfileImage());
            mUserCheckbox.setChecked(selected);
        }
    }
    public interface OnItemCheckedChangeListener {
        void onItemCheckedChanged(User user, boolean isChecked);
    }
}
