package com.biennhominext.chatbase.ui.conversation;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.utils.Dates;
import com.biennhominext.chatbase.utils.FirebaseUtils;
import com.biennhominext.chatbase.utils.MapUtils;
import com.biennhominext.chatbase.utils.UiUtils;
import com.bumptech.glide.Glide;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;

import vc908.stickerfactory.StickersManager;


/**
 * Created by bien on 7/24/2017.
 */

public class ConversationHolder extends RecyclerView.ViewHolder {
    private static final int MSG_INCOMING = 0;
    private static final int MSG_OUTGOING = 1;
    private static final int MSG_CONTENT_IMAGE = 2;
    private static final int MSG_CONTENT_TEXT_PLAIN = 4;
    ViewGroup mContainer;
    View mContentContainer;
    TextView mStatusTextView;
    TextView mMessageTextView;
    SimpleDraweeView mAvatarImageView;
    LinearLayout mMessageContainer;
    RecyclerView mAttachmentsRecycler;
    SimpleDraweeView mAttachmentImageView;
    ViewStub mAttachmentsStub;
    ViewStub mImageAttachmentStub;
    ViewStub mStickerAttachmentStub;
    TextView mDateTextView;
    ImageView mStickerImageView;
    TextView mNameTextView;
    private Context mContext;

    public ConversationHolder(View itemView) {
        super(itemView);
        this.mContext = itemView.getContext();
        mStatusTextView = itemView.findViewById(R.id.text_status);
        mMessageTextView = itemView.findViewById(R.id.text_message);
        mAvatarImageView = itemView.findViewById(R.id.image_avatar);
        mMessageContainer = itemView.findViewById(R.id.container_msg);
        mDateTextView = itemView.findViewById(R.id.text_timestamp);
        mContainer = itemView.findViewById(R.id.container);
        mContentContainer = itemView.findViewById(R.id.container_msg);
        mNameTextView = itemView.findViewById(R.id.text_username);
    }

    public void bind(Message msg, Message previousMessage, Message nextMsg, String currentUserId,
                     OnImageItemClickListener listener, boolean isLastMessage, boolean isOneOne) {
        mStatusTextView.setText(Dates.getDateAndTime(mContext, msg.getTimestamp()));
        final boolean isIncomingMessage = !(msg.getSender().equals(currentUserId));
        final int messageBubblePadding = mContext.getResources().getDimensionPixelOffset(R.dimen.message_bubble_content_padding);
        final int messageTopPaddingClustered = mContext.getResources().getDimensionPixelOffset(R.dimen.message_padding_clustered);
        final int messageTopPaddingDefault = mContext.getResources().getDimensionPixelOffset(R.dimen.message_padding_default);
        final boolean canClusterWithPreviousMessage = canClusterWithMessage(msg, previousMessage);
        final boolean canClusterWithNextMessage = canClusterWithMessage(msg, nextMsg);

        if (canClusterWithPreviousMessage) {
            mContainer.setPadding(0, messageTopPaddingClustered, 0, 0);

        } else {
            mContainer.setPadding(0, messageTopPaddingDefault, 0, 0);
        }

        if (isDiffTimeSmall(msg, previousMessage)) {
            mDateTextView.setVisibility(View.GONE);
        } else {
            mDateTextView.setText(Dates.getDateAndTime(mContext, msg.getTimestamp()));
            mDateTextView.setVisibility(View.VISIBLE);
        }

        if (!msg.isSent()) {
            mStatusTextView.setVisibility(View.VISIBLE);
            mStatusTextView.setText(mContext.getText(R.string.conversation_sending_status));
        } else if (isLastMessage) {
            mStatusTextView.setVisibility(View.VISIBLE);
            mStatusTextView.setText(Dates.getTime(mContext, msg.getTimestamp()));
        } else {
            mStatusTextView.setVisibility(View.GONE);
        }

        if (!isOneOne && isIncomingMessage && !canClusterWithPreviousMessage){
            mNameTextView.setVisibility(View.VISIBLE);
            mNameTextView.setText(msg.getUser().getName());
        }else {
            mNameTextView.setVisibility(View.GONE);
        }

        if (isIncomingMessage) {
            String path = msg.getUser().getProfileImage();
            if (canClusterWithPreviousMessage || TextUtils.isEmpty(path)) {
                mAvatarImageView.setVisibility(View.INVISIBLE);
            } else {


                mAvatarImageView.setImageURI(Uri.parse(path));
                mAvatarImageView.setVisibility(View.VISIBLE);
            }
            mMessageContainer.setPadding(0, 0, messageBubblePadding, 0);
            mMessageContainer.setGravity(Gravity.LEFT);


        } else {
            mAvatarImageView.setVisibility(View.GONE);
            mMessageContainer.setGravity(Gravity.RIGHT);

            mMessageContainer.setPadding(messageBubblePadding, 0, 0, 0);
        }

        if (mAttachmentsRecycler != null) mAttachmentsRecycler.setVisibility(View.GONE);
        if (mAttachmentImageView != null) mAttachmentImageView.setVisibility(View.GONE);
        if (mStickerImageView != null) mStickerImageView.setVisibility(View.GONE);
        mMessageTextView.setVisibility(View.GONE);

        if (msg.getMessageType().equals(Message.TYPE_PLAIN_TEXT)) {
            mMessageTextView.setVisibility(View.VISIBLE);
            if (isIncomingMessage) {
                mMessageTextView.setTextColor(0xff263238);
                if (canClusterWithNextMessage && canClusterWithPreviousMessage) {
                    mMessageTextView.setBackgroundResource(R.drawable.msg_bubble_incoming_middle);
                } else if (canClusterWithNextMessage) {
                    mMessageTextView.setBackgroundResource(R.drawable.msg_bubble_incoming_start);
                } else if (canClusterWithPreviousMessage) {
                    mMessageTextView.setBackgroundResource(R.drawable.msg_bubble_incoming_end);
                } else {
                    mMessageTextView.setBackgroundResource(R.drawable.msg_bubble_incomming);
                }
            } else {
                mMessageTextView.setTextColor(0xffffffff);
                if (canClusterWithNextMessage && canClusterWithPreviousMessage) {
                    mMessageTextView.setBackgroundResource(R.drawable.msg_bubble_outgoing_middle);
                } else if (canClusterWithNextMessage) {
                    mMessageTextView.setBackgroundResource(R.drawable.msg_bubble_outgoing_start);
                } else if (canClusterWithPreviousMessage) {
                    mMessageTextView.setBackgroundResource(R.drawable.msg_bubble_outgoing_end);
                } else {
                    mMessageTextView.setBackgroundResource(R.drawable.msg_bubble_outgoing);
                }
            }

            mMessageTextView.setText(msg.getContent());

        } else if (msg.getMessageType().equals(Message.TYPE_MEDIA_IMAGE)) {
            mMessageTextView.setVisibility(View.GONE);
            String[] paths = FirebaseUtils.extractImageMessage(msg.getContent());
            if (paths.length == 1) {
                loadSingleImageItem(msg.getContent(), listener);

            } else {

                setIsRecyclable(false);
                mAttachmentsStub = itemView.findViewById(R.id.attachments_stub);
                if (mAttachmentsStub != null) {
                    mAttachmentsRecycler = (RecyclerView) mAttachmentsStub.inflate();
                }
                mAttachmentsRecycler.setVisibility(View.VISIBLE);
                mAttachmentsRecycler.setLayoutManager(new GridLayoutManager(mContext, paths.length > 3 ? 3 : paths.length));
                ImageAdapter adapter = new ImageAdapter(mContext, paths);
                adapter.setListener((path, view) -> {
                    if (listener != null) {
                        listener.onImageItemClick(path, view);
                    }
                });
                mAttachmentsRecycler.setHasFixedSize(true);
                mAttachmentsRecycler.setAdapter(adapter);

            }

        } else if (msg.getMessageType().equals(Message.TYPE_STICKER)) {
            mMessageTextView.setVisibility(View.GONE);
            mStickerAttachmentStub = itemView.findViewById(R.id.sticker_attachment_stub);
            if (mStickerAttachmentStub != null) {
                mStickerImageView = (ImageView) mStickerAttachmentStub.inflate();
            }
            mStickerImageView.setVisibility(View.VISIBLE);
            StickersManager.with(mContext)
                    .loadSticker(msg.getContent())
                    .into(mStickerImageView);
            // because we also use this view for displaying map, we should clear the map action
            mStickerImageView.setOnClickListener(null);
        } else if (msg.getMessageType().equals(Message.TYPE_LOCATION)) {
            mMessageTextView.setVisibility(View.GONE);
            mStickerAttachmentStub = itemView.findViewById(R.id.sticker_attachment_stub);
            if (mStickerAttachmentStub != null) {
                mStickerImageView = (ImageView) mStickerAttachmentStub.inflate();
            }
            mStickerImageView.setVisibility(View.VISIBLE);
            final int mapSizeDimen = mContext.getResources().getDimensionPixelSize(R.dimen.conversation_message_sticker_size);
            Glide.with(mContext)
                    .load(MapUtils.getStaticMapUri(msg.getContent(),"18",mapSizeDimen))
                    .into(mStickerImageView);
            mStickerImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UiUtils.openMapActivity(view.getContext(),msg.getContent());
                }
            });


        }

    }

    private boolean canClusterWithMessage(Message msg, Message otherMsg) {
        if (otherMsg == null || msg == null) {
            return false;
        }
        if (Message.TYPE_PLAIN_TEXT.equals(msg.getMessageType())
                && Message.TYPE_PLAIN_TEXT.equals(otherMsg.getMessageType())) {
            if (!msg.getSender().equals(otherMsg.getSender())) {
                return false;
            }
            return isDiffTimeSmall(msg, otherMsg);
        }

        return false;

    }

    private boolean isDiffTimeSmall(Message msg, Message otherMsg) {
        if (otherMsg == null || msg == null) {
            return false;
        }
        long diffTime = Math.abs(msg.getTimestamp() - otherMsg.getTimestamp());
        if (diffTime < 5 * DateUtils.MINUTE_IN_MILLIS) {
            return true;
        }
        return false;
    }

    private void loadSingleImageItem(String content, OnImageItemClickListener onImageItemClickListener) {
        mImageAttachmentStub = itemView.findViewById(R.id.image_attachment_stub);
        if (mImageAttachmentStub != null) {
            mAttachmentImageView = (SimpleDraweeView) mImageAttachmentStub.inflate();

        }
        mAttachmentImageView.setVisibility(View.VISIBLE);


        ControllerListener listener = new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet(String id, @Nullable ImageInfo imageInfo, @Nullable Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                updateViewSize(imageInfo);
            }

            @Override
            public void onIntermediateImageSet(String id, @Nullable ImageInfo imageInfo) {
                super.onIntermediateImageSet(id, imageInfo);
                updateViewSize(imageInfo);
            }
        };
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(mAttachmentImageView.getController())
                .setUri(content)
                .setControllerListener(listener)
                .build();
        mAttachmentImageView.setController(controller);
        ViewCompat.setTransitionName(mAttachmentImageView, content);
        mAttachmentImageView.setOnClickListener(view -> {
            if (onImageItemClickListener != null) {
                onImageItemClickListener.onImageItemClick(content, mAttachmentImageView);
            }
        });
    }


    void updateViewSize(ImageInfo imageInfo) {
        if (imageInfo != null) {
            mAttachmentImageView.setAspectRatio((float) imageInfo.getWidth() / imageInfo.getHeight());
        }
    }


}

