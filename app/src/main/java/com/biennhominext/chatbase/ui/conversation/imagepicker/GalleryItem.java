package com.biennhominext.chatbase.ui.conversation.imagepicker;

import android.provider.MediaStore.Images.Media;

/**
 * Created by hbien on 8/14/2017.
 */

public class GalleryItem {
    public static final String[] IMAGE_PROJECTION = new String[]{
            Media._ID,
            Media.DATA,
            Media.WIDTH,
            Media.HEIGHT,
            Media.MIME_TYPE,
            Media.DATE_MODIFIED};



    public static final int INDEX_ID = 0;

    public static final int INDEX_DATA_PATH = 1;
    public static final int INDEX_WIDTH = 2;
    public static final int INDEX_HEIGHT = 3;
    public static final int INDEX_MIME_TYPE = 4;
    public static final int INDEX_DATE_MODIFIED = 5;


}
