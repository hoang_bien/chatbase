package com.biennhominext.chatbase.ui.profileedit;

import android.content.Context;
import android.net.Uri;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.di.ActivityContext;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;
import com.facebook.common.util.UriUtil;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 7/17/2017.
 */

public class ProfileEditPresenterImpl<T extends ProfileEditView> extends BasePresenterImpl<T>
        implements ProfileEditPresenter<T> {
    @Inject
    public ProfileEditPresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable
            , @ActivityContext Context context) {
        super(dataManager, scheduler, compositeDisposable);
    }

    @Override
    public void onAttach(T view) {
        super.onAttach(view);
        final String profileImage = getDataManager().getCurrentProfileImage();
        if (profileImage != null) {
            getView().setProfileImage(profileImage);
        }

        final String userName = getDataManager().getCurrentUserName();
        if (userName != null) {
            view.setUserName(userName);
        }


        final String email = getDataManager().getCurrentUSerEmail();
        if (userName != null) {
            view.setUserEmail(email);
        }


        final int gender = getDataManager().getCurrentUserGender();
        view.setUserGender(gender);


    }


    @Override
    public void updateUserInfo(String username, String email, int gender, Uri imageUri) {
        if (UriUtil.isNetworkUri(imageUri)) {
            User userInfo = new User.Builder()
                    .setUid(getDataManager().getCurrentUserId())
                    .setProfileImage(imageUri.toString())
                    .setName(username)
                    .setEmail(email)
                    .setGender(gender).createUser();
            getDataManager().updateUserInfo(userInfo).subscribe(user -> {
                getView().toMainActivity();
            }, e -> getView().onError(e.getMessage()));
        } else {
            getView().showDialog();
            getDataManager().uploadImage(imageUri.getPath())
                    .subscribeOn(getScheduler().io())
                    .observeOn(getScheduler().ui())
                    .flatMap(path -> {
                        User user = new User.Builder()
                                .setUid(getDataManager().getCurrentUserId())
                                .setProfileImage(path)
                                .setName(username)
                                .setEmail(email)
                                .setGender(gender).createUser();
                        return getDataManager().updateUserInfo(user);
                    }).subscribe(user -> {
                getView().toMainActivity();
            }, e -> getView().onError(e.getMessage()));
        }


    }


}
