package com.biennhominext.chatbase.ui.invite;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.ui.base.BaseActivity;
import com.biennhominext.chatbase.ui.conversation.ConversationActivity;
import com.biennhominext.chatbase.ui.custom.SpacingItemDecoration;
import com.biennhominext.chatbase.utils.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hbien on 8/10/2017.
 */

public class InviteActivity extends BaseActivity implements InviteView, InviteAdapter.OnItemInviteClickListener {
    @BindView(R.id.recycler_invite)
    RecyclerView mInviteList;
    @BindView(R.id.viewgroup_empty)
    View mEmptyView;
    @Inject
    InvitePresenter<InviteView> mPresenter;

    private InviteAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);

        setUnbinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mAdapter = new InviteAdapter();
        mAdapter.setListener(this);
        mInviteList.setLayoutManager(new LinearLayoutManager(this));
        mInviteList.setAdapter(mAdapter);
        SpacingItemDecoration itemDecoration = new SpacingItemDecoration(getResources(), 8);
        mInviteList.addItemDecoration(itemDecoration);
        mPresenter.onAttach(this);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void showInviteMessage(Message message) {
        UiUtils.hideViewIfVisible(mEmptyView);
        mAdapter.addInviteMessage(message);
    }

    @Override
    public void onItemClick(Message message) {
        Intent intent = new Intent(this, ConversationActivity.class);
        intent.putExtra(ConversationActivity.EXTRA_ID, message.getSender());
        intent.putExtra(ConversationActivity.EXTRA_IS_GROUP,false);
        startActivity(intent);
    }

    @Override
    public void onItemIgnoreClick(Message message, int position) {
        new MaterialDialog.Builder(this)
                .title(R.string.dialog_invites_ingore_title)
                .content(R.string.dialog_invites_ingore_content)
                .negativeText(R.string.action_cancel)
                .positiveText(R.string.action_ok)
                .positiveColorRes(R.color.colorAccent)
                .onPositive((dialog, which) -> {
                    mPresenter.ignoreInvitation(message.getUser().getUid());
                    mAdapter.removeInviteMessage(position);
                    if (mAdapter.getItemCount() == 0){
                        UiUtils.showIfInvisible(mEmptyView);
                    }
                })
                .show();
    }

    @Override
    public void onItemAcceptClick(Message message, int position) {
        mPresenter.acceptInvitation(message.getUser().getUid());
        onItemClick(message);
        UiUtils.cancelNotification(this,message.getSender().hashCode());
    }

}
