package com.biennhominext.chatbase.ui.conversation.imagepicker;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.ui.OnItemClickListener;
import com.biennhominext.chatbase.ui.custom.CursorRecyclerViewAdapter;

import timber.log.Timber;

/**
 * Created by hbien on 8/14/2017.
 */

public class GalleryAdapter extends CursorRecyclerViewAdapter<GalleryViewHolder> {
    private int mSlectedPosition = -1;
    private OnItemClickListener mListener;


    public GalleryAdapter(Context context, Cursor cursor) {
        super(context, cursor);
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery_grid, parent, false);
        GalleryViewHolder holder = new GalleryViewHolder(view);
        return holder;
    }

    public void setListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        Cursor oldCursor = super.swapCursor(newCursor);
        if (oldCursor != newCursor) {
            mSlectedPosition = -1;
        }
        return oldCursor;
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder viewHolder, Cursor cursor) {
        final int pos = cursor.getPosition();
        viewHolder.bind(cursor, mSlectedPosition);
        viewHolder.itemView.setOnClickListener(view -> {
            if (mSlectedPosition == pos) {
                return;
            }
            notifyItemChanged(mSlectedPosition);
            notifyItemChanged(pos);
            mSlectedPosition = pos;
        });

        viewHolder.mSendBtn.setOnClickListener(view -> {
            if (mListener != null) {
                Timber.i("Onlick at" + pos);
                mListener.onItemClick(cursor.getString(GalleryItem.INDEX_DATA_PATH),pos);
            }
            notifyItemChanged(mSlectedPosition);
            mSlectedPosition = -1;
        });
    }
}
