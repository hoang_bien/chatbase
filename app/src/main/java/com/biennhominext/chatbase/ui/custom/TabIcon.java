package com.biennhominext.chatbase.ui.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.biennhominext.chatbase.R;

/**
 * Created by hbien on 8/18/2017.
 */

public class TabIcon extends FrameLayout {
    ImageButton mIcon;
    View mIndicator;
    private boolean mSelected;

    public TabIcon(Context context) {
        super(context);
    }

    public TabIcon(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TabIcon(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mIcon = findViewById(R.id.tab);
        mIndicator = findViewById(R.id.indicator);
    }

    public void setOnTabClickListener(OnClickListener listener){
        mIcon.setOnClickListener(listener);
    }
    public void setSelected(boolean selected){
        mSelected = selected;
        if (selected){
            mIcon.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent));
            mIndicator.setVisibility(VISIBLE);
        }else {
            mIcon.setColorFilter(null);
            mIndicator.setVisibility(GONE);
        }
    }
    public boolean isSelected(){
        return mSelected;
    }
}
