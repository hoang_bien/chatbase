package com.biennhominext.chatbase.ui.login;

import android.content.Intent;

import com.biennhominext.chatbase.ui.base.BasePresenter;

/**
 * Created by bien on 7/6/2017.
 */

public interface LoginPresenter<V extends LoginView> extends BasePresenter<V> {
    void doGoogleLogin();
    void doLoginWithEmailAndPassword(String email, String password);
    boolean onActivityResult(int requestCode, int resultCode, Intent data);
}
