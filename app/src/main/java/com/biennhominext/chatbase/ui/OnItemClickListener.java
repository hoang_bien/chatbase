package com.biennhominext.chatbase.ui;

/**
 * Created by hbien on 7/27/2017.
 */

public interface OnItemClickListener {
    void onItemClick(Object object, int position);
}
