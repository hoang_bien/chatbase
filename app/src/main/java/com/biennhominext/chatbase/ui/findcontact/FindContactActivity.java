package com.biennhominext.chatbase.ui.findcontact;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.OnItemClickListener;
import com.biennhominext.chatbase.ui.base.BaseActivity;
import com.biennhominext.chatbase.ui.conversation.ConversationActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hbien on 8/2/2017.
 */

public class FindContactActivity extends BaseActivity implements FindContactView, OnItemClickListener, SearchView.OnQueryTextListener {
    @BindView(R.id.recycler_user)
    RecyclerView mUserList;
    @Inject
    FindContactPresenter<FindContactView> mPresenter;
    @Inject
    ContactResultAdapter mAdapter;
    private static final String TAG = FindContactActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        getActivityComponent().inject(this);
        setUnbinder(ButterKnife.bind(this));
        mUserList.setLayoutManager(new LinearLayoutManager(this));
        mUserList.setAdapter(mAdapter);
        mAdapter.setListener(this);
        mUserList.addItemDecoration(new ContactResultItemDecoration(this));
        mPresenter.onAttach(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_contact_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setIconified(false);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void displayUser(User user) {
        mAdapter.addUser(user);
    }

    @Override
    public void onItemClick(Object object, int position) {
        Intent intent = new Intent(this, ConversationActivity.class);
        intent.putExtra(ConversationActivity.EXTRA_ID, (String) object);
        intent.putExtra(ConversationActivity.EXTRA_IS_GROUP, false);
        startActivity(intent);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.i(TAG, newText);
        mAdapter.clear();
        if (!newText.isEmpty()) {
            mPresenter.findUserByName(newText);
        }
        return true;
    }
}
