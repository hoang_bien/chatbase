package com.biennhominext.chatbase.ui.profile;

import com.biennhominext.chatbase.ui.base.BasePresenter;

/**
 * Created by bien on 7/20/2017.
 */

public interface ProfilePresenter<V extends ProfileView> extends BasePresenter<V> {
    void logout();
}
