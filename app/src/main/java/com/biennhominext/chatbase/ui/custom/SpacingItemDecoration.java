package com.biennhominext.chatbase.ui.custom;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.biennhominext.chatbase.utils.UiUtils;

/**
 * Created by hbien on 8/10/2017.
 */

public class SpacingItemDecoration extends RecyclerView.ItemDecoration {
    int mOffset;

    public SpacingItemDecoration(Resources resource, int offset) {
        mOffset = UiUtils.dpToPx(resource, offset);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position == 0) {
            outRect.set(mOffset, mOffset, mOffset, mOffset);
        } else {
            outRect.set(mOffset, 0, mOffset, mOffset);
        }
    }
}
