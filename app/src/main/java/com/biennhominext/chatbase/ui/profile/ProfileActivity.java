package com.biennhominext.chatbase.ui.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.biennhominext.chatbase.ui.base.BaseActivity;

/**
 * Created by hbien on 8/21/2017.
 */

public class ProfileActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(android.R.id.content, new ProfileFragment())
                    .commit();
        }

    }
}
