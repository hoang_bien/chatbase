package com.biennhominext.chatbase.ui.custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.DraweeHolder;

/**
 * Created by hbien on 8/21/2017.
 */

public class AvatarDraweeView extends View {
    private static int color = 0xff388E3C;
    private Paint mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint mBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    DraweeHolder<GenericDraweeHierarchy> mDraweeHolder;
    private boolean mOnlineStatus = false;

    public AvatarDraweeView(Context context) {
        super(context);
        init();
    }

    public AvatarDraweeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AvatarDraweeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        GenericDraweeHierarchy hierarchy = new GenericDraweeHierarchyBuilder(getResources())
                .setRoundingParams(RoundingParams.asCircle())
                .setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP)
                .build();
        mDraweeHolder = DraweeHolder.create(hierarchy, getContext());
        mDraweeHolder.getTopLevelDrawable().setCallback(this);
        mCirclePaint.setColor(color);
        mBorderPaint.setColor(Color.WHITE);
    }

    public void setImageUri(String uri){
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(uri)
                .setOldController(mDraweeHolder.getController())
                .build();
        mDraweeHolder.setController(controller);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Drawable drawable = mDraweeHolder.getTopLevelDrawable();

        final int w = getWidth();
        final int h = getHeight();
        drawable.setBounds(0,0,w,h);
        drawable.draw(canvas);
         if (mOnlineStatus){
            final int radius = h / 8;
            canvas.drawCircle(w - radius, h - radius, radius + h / 24, mBorderPaint);
            canvas.drawCircle(w - radius, h - radius, radius, mCirclePaint);
        }
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected boolean verifyDrawable(Drawable who) {
        if (who == mDraweeHolder.getTopLevelDrawable()) {
            return true;
        }
        // other logic for other Drawables in your view, if any
        return true;
    }

    public void setOnlineStatus(boolean onlineStatus) {
        this.mOnlineStatus = onlineStatus;
        invalidate();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mDraweeHolder.onDetach();
    }

    @Override
    public void onStartTemporaryDetach() {
        super.onStartTemporaryDetach();
        mDraweeHolder.onDetach();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        mDraweeHolder.onAttach();
    }

    @Override
    public void onFinishTemporaryDetach() {
        super.onFinishTemporaryDetach();
        mDraweeHolder.onAttach();
    }
}
