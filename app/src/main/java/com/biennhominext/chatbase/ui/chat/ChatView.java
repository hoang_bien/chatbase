package com.biennhominext.chatbase.ui.chat;

import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.ui.base.BaseView;

import java.util.List;

/**
 * Created by bien on 7/19/2017.
 */

public interface ChatView extends BaseView {
     void updateChat(List<Message> msgList);
     void addMessage(Message msg);
     void init(String currentUserId);
}
