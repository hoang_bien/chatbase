package com.biennhominext.chatbase.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.ui.base.BaseActivity;
import com.biennhominext.chatbase.ui.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bien on 7/6/2017.
 */

public class LoginActivity extends BaseActivity implements LoginView {
    @Inject
    LoginPresenter<LoginView> mPresenter;
    @BindView(R.id.edit_email)
    EditText mEmailEditext;
    @BindView(R.id.edit_password)
    EditText mPasswordEditext;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FloatingActionButton googleFab = (FloatingActionButton) findViewById(R.id.btn_google_login);

        googleFab.setOnClickListener(view -> {
            mPresenter.doGoogleLogin();
        });
        getActivityComponent().inject(this);
        setUnbinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mPresenter.onActivityResult(requestCode,resultCode,data)){
            super.onActivityResult(requestCode,resultCode,data);
        }
    }

    @Override
    public void toMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @OnClick(R.id.btn_login)
    public void login(View view){
        String email = mEmailEditext.getText().toString();
        String password = mPasswordEditext.getText().toString();
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)){
            showMessage(R.string.login_empty_email_or_password_error);
            return;
        }
        mPresenter.doLoginWithEmailAndPassword(email,password);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}


