package com.biennhominext.chatbase.ui.group.addmember;

import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BaseView;

/**
 * Created by hbien on 7/29/2017.
 */

public interface AddMemberView extends BaseView {

    void addUser(User user);

    void toConversationActivity();

    void updateGroupInfo(Group group);
}
