package com.biennhominext.chatbase.ui.invite;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hbien on 8/10/2017.
 */

public class InviteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Message> mMessageList;
    private OnItemInviteClickListener mListener;

    public InviteAdapter() {
        mMessageList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invitation, parent, false);
        InviteHolder holder = new InviteHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((InviteHolder) holder).bind(mMessageList.get(position),mListener);
    }

    public void setListener(OnItemInviteClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    public void addInviteMessage(Message message) {
        int i;
        for (i = 0; i < mMessageList.size(); i++) {
            if (mMessageList.get(i).equals(message)) {
                mMessageList.set(i, message);
                if (i > 0) {
                    notifyItemChanged(i - 1);
                }
                notifyItemChanged(i);
                break;
            }
        }
        if (i == mMessageList.size()) {
            mMessageList.add(message);
            if (i > 0) {
                notifyItemChanged(i - 1);
            }
            notifyItemInserted(i);
        }
    }

    public void removeInviteMessage(int position) {
        mMessageList.remove(position);
        notifyItemRemoved(position);
    }

    public interface OnItemInviteClickListener {
        void onItemClick(Message message);

        void onItemIgnoreClick(Message message, int position);

        void onItemAcceptClick(Message message, int position);
    }
}
