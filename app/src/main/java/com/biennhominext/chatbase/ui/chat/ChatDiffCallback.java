package com.biennhominext.chatbase.ui.chat;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import com.biennhominext.chatbase.data.model.Message;

import java.util.List;

/**
 * Created by bien on 7/24/2017.
 */

public class ChatDiffCallback extends DiffUtil.Callback {
    List<Message> oldMsgList;
    List<Message> newMsgList;

    public ChatDiffCallback(List<Message> oldMsgList, List<Message> newMsgList) {
        this.oldMsgList = oldMsgList;
        this.newMsgList = newMsgList;
    }

    @Override
    public int getOldListSize() {
        return oldMsgList.size();
    }

    @Override
    public int getNewListSize() {
        return newMsgList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldMsgList.get(oldItemPosition).getUid().equals(newMsgList.get(newItemPosition).getUid());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldMsgList.get(oldItemPosition).getTimestamp() == newMsgList.get(newItemPosition).getTimestamp();
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        //TODO : implement this
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
