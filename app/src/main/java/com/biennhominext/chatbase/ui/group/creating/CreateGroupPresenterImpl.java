package com.biennhominext.chatbase.ui.group.creating;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by hbien on 7/28/2017.
 */

public class CreateGroupPresenterImpl<V extends CreateGroupView> extends BasePresenterImpl<V> implements CreateGroupPresenter<V> {
    @Inject
    public CreateGroupPresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dataManager, scheduler, compositeDisposable);
    }

    @Override
    public void createGroup(String groupName, String description) {
        String id = getDataManager().createNewGroup(groupName,description);
        getView().openAddMemberActivity(id);
    }
}
