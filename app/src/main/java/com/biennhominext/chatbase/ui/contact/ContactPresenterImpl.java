package com.biennhominext.chatbase.ui.contact;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 7/19/2017.
 */

public class ContactPresenterImpl<V extends ContactView> extends BasePresenterImpl<V> implements ContactPresenter<V> {
    @Inject
    public ContactPresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dataManager, scheduler, compositeDisposable);
    }

    @Override
    public void onAttach(V view) {
        super.onAttach(view);
        getDisposable().add(getDataManager().getMyContacts()
                .subscribeOn(getScheduler().io())
                .subscribeOn(getScheduler().ui())
                .subscribe(getView()::addContact));
    }
}
