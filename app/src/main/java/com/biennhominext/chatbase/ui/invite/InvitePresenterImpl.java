package com.biennhominext.chatbase.ui.invite;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by hbien on 8/10/2017.
 */

public class InvitePresenterImpl<V extends InviteView> extends BasePresenterImpl<V> implements InvitePresenter<V> {
    @Inject
    public InvitePresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dataManager, scheduler, compositeDisposable);
    }

    @Override
    public void onAttach(V view) {
        super.onAttach(view);
        getDisposable().add(getDataManager().getMyInvites().subscribeOn(getScheduler().io())
                .observeOn(getScheduler().ui())
                .subscribe(getView()::showInviteMessage));

    }

    @Override
    public void ignoreInvitation(String userId) {
        getDataManager().ignoreInvitation(userId);
    }

    @Override
    public void acceptInvitation(String userId) {
        getDataManager().acceptInvitation(userId);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
