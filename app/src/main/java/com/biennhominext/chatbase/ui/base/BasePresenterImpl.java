package com.biennhominext.chatbase.ui.base;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 7/4/2017.
 */

public class BasePresenterImpl<V extends BaseView> implements BasePresenter<V> {
    private final DataManager mDb;
    private final SchedulerProvider mScheduler;
    private final CompositeDisposable mDisposable;
    private V mView;

    @Inject
    public BasePresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable){
        mDb = dataManager;
        mScheduler = scheduler;
        mDisposable = compositeDisposable;
    }

    @Override
    public void onAttach(V view) {
        mView = view;
    }

    public void onDetach(){
        mDisposable.clear();
        mView = null;
    }


    public CompositeDisposable getDisposable() {
        return mDisposable;
    }

    public DataManager getDataManager(){
        return mDb;
    }

    public SchedulerProvider getScheduler() {
        return mScheduler;
    }

    public V getView() {
        return mView;
    }
}
