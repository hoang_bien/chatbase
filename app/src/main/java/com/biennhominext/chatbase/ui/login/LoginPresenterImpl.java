package com.biennhominext.chatbase.ui.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 7/6/2017.
 */

public class LoginPresenterImpl<V extends LoginView> extends BasePresenterImpl<V> implements LoginPresenter<V> {
    private static final int RC_SIGN_IN = 1001;
    private GoogleApiClient googleApiClient;
    private AppCompatActivity mActivity;

    @Inject
    public LoginPresenterImpl(DataManager database, SchedulerProvider scheduler, CompositeDisposable compositeDisposable, AppCompatActivity activity) {
        super(database, scheduler, compositeDisposable);
        mActivity = activity;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(activity)
                .addOnConnectionFailedListener(result -> getView().onError(result.getErrorMessage()))
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    @Override
    public void doGoogleLogin() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        mActivity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void doLoginWithEmailAndPassword(String email, String password) {
        getDisposable().add(getDataManager().doLoginWithEmailAndPassword(email, password)
                .subscribe(() -> getView().toMainActivity()));
    }

    private void doGoogleLogin(GoogleSignInAccount account) {
        //TODO: continue here
        getDisposable().add(getDataManager().doGoogleLoginCall(account)
                .subscribe(() -> getView().toMainActivity(),
                        e -> getView().onError(R.string.login_google_failed)
                ));
    }


    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                doGoogleLogin(account);
            } else {
                getView().onError(R.string.login_google_failed);
            }
            return true;
        }
        return false;
    }
}
