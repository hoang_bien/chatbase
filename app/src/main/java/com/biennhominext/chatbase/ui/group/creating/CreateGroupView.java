package com.biennhominext.chatbase.ui.group.creating;

import com.biennhominext.chatbase.ui.base.BaseView;

/**
 * Created by hbien on 7/28/2017.
 */

public interface CreateGroupView extends BaseView {
    void openAddMemberActivity(String groupId);
    void dismiss();

}
