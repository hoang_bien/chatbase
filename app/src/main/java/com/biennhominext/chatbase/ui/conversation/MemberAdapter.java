package com.biennhominext.chatbase.ui.conversation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.custom.AvatarDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hbien on 8/6/2017.
 */

public class MemberAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<User> mUserList;

    public MemberAdapter() {
        mUserList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_member,parent,false);
        MemberHolder holder = new MemberHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final  User user = mUserList.get(position);
        MemberHolder memberHolder = (MemberHolder) holder;
        memberHolder.avatar.setOnlineStatus(user.isOnline());
        memberHolder.avatar.setImageUri(user.getProfileImage());
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public void add(User user) {
        int i = 0;
        while (i < mUserList.size()){
            if (mUserList.get(i).equals(user)){
                mUserList.set(i,user);
                notifyItemChanged(i);
                break;
            }
            i++;
        }
        if (i == mUserList.size()){
            mUserList.add(user);
            notifyItemInserted(i);
        }
    }

    public void remove(String sender) {
        for (int i = 0; i < mUserList.size(); i++) {
            if (mUserList.get(i).getUid().equals(sender)){
                mUserList.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public static class MemberHolder extends RecyclerView.ViewHolder{
        AvatarDraweeView avatar;

        public MemberHolder(View itemView) {
            super(itemView);
            this.avatar = (AvatarDraweeView) itemView;
        }
    }
}
