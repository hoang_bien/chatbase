package com.biennhominext.chatbase.ui.conversation.imagepicker;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.ui.OnItemClickListener;
import com.hwangjr.rxbus.RxBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by hbien on 8/11/2017.
 */

public class ImagePickerSubFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,OnItemClickListener {
    private static final int LOADER_GALLERY = 101;
    @BindView(R.id.recycler_gallery)
    RecyclerView mGalleryListView;
    private GalleryAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_picker, container, false);
        ButterKnife.bind(this,view);
        mAdapter = new GalleryAdapter(getActivity(), null);
        mGalleryListView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mGalleryListView.setAdapter(mAdapter);
        mAdapter.setListener(this);
        EasyImage.configuration(getActivity())
                .setImagesFolderName(getString(R.string.app_name))
                .setAllowMultiplePickInGallery(true);
        getLoaderManager().initLoader(LOADER_GALLERY, null, this);
        return view;
    }

    @OnClick(R.id.btn_open_gallery)
    public void doOpenGallery() {
        EasyImage.openGallery(this, 0);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new GalleryLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onItemClick(Object object, int position) {

        String path = (String) object;
        RxBus.get().post(path);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                ArrayList<String> paths = new ArrayList<>();
                for (int i = 0; i < imageFiles.size(); i++) {
                    paths.add(imageFiles.get(i).getAbsolutePath());
                }
                RxBus.get().post(paths);
            }
        });

    }
}
