package com.biennhominext.chatbase.ui.main;

import com.biennhominext.chatbase.ui.base.BasePresenter;

/**
 * Created by bien on 7/17/2017.
 */

public interface MainPresenter<V extends MainView> extends BasePresenter<V> {
    void onPreparedAccount();
}
