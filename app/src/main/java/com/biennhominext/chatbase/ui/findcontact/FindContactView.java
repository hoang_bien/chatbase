package com.biennhominext.chatbase.ui.findcontact;

import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BaseView;

/**
 * Created by hbien on 8/2/2017.
 */

public interface FindContactView extends BaseView {
    void displayUser(User user);
}
