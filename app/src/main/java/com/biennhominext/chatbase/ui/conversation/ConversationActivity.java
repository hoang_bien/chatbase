package com.biennhominext.chatbase.ui.conversation;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.ui.base.BaseActivity;

/**
 * Created by bien on 7/24/2017.
 */

public class ConversationActivity extends BaseActivity {
    public static final String EXTRA_IS_GROUP = "is_group";
    public static final String EXTRA_ID = "id";
    private static final String FRAG_CONVERSATION = "frag_conversation";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        String id = null;
        boolean isGroup = false;
        if (savedInstanceState == null){
            id= getIntent().getStringExtra(EXTRA_ID);
            isGroup= getIntent().getBooleanExtra(EXTRA_IS_GROUP, false);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        getActivityComponent().inject(this);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(android.R.id.content, ConversationFragment.newInstance(id, isGroup),FRAG_CONVERSATION)
                    .commit();
        }



    }

    @Override
    public void onBackPressed() {
        ConversationFragment fragment = (ConversationFragment) getSupportFragmentManager()
                .findFragmentByTag(FRAG_CONVERSATION);
        if (fragment.shouldAllowBackPressed()){
            super.onBackPressed();
        }
    }
}
