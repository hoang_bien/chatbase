package com.biennhominext.chatbase.ui.group.addmember;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BaseActivity;
import com.biennhominext.chatbase.ui.conversation.ConversationActivity;
import com.biennhominext.chatbase.utils.UiUtils;
import com.facebook.common.executors.UiThreadImmediateExecutorService;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.pchmn.materialchips.ChipsInput;
import com.pchmn.materialchips.model.ChipInterface;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hbien on 7/29/2017.
 */

public class AddMemberActivity extends BaseActivity implements AddMemberView, MemberPickerAdapter.OnItemCheckedChangeListener, ChipsInput.ChipsListener {
    public static final String EXTRA_GROUP_ID = "group_id";
    public static final String EXTRA_IS_NEWLY_CREATED = "is_first_created";
    @BindView(R.id.chips_user)
    ChipsInput mUserChips;
    @BindView(R.id.recycler_contact)
    RecyclerView mContactList;
    @Inject
    AddMemberPresenter<AddMemberView> mPresenter;
    @Inject
    MemberPickerAdapter mAdapter;
    @BindView(R.id.viewgroup_empty)
    View mEmptyView;

    private String mGroupId;
    private boolean mIsNewlyCreated;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getActivityComponent().inject(this);
        setUnbinder(ButterKnife.bind(this));
        mUserChips.addChipsListener(new ChipsInput.ChipsListener() {
            @Override
            public void onChipAdded(ChipInterface chipInterface, int i) {

            }

            @Override
            public void onChipRemoved(ChipInterface chipInterface, int i) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence) {
                Log.d("TAg", charSequence.toString());
            }
        });
        mGroupId = getIntent().getStringExtra(EXTRA_GROUP_ID);
        mIsNewlyCreated = getIntent().getBooleanExtra(EXTRA_IS_NEWLY_CREATED, false);

        mPresenter.init(mGroupId, mIsNewlyCreated);
        mPresenter.onAttach(this);
        mContactList.setLayoutManager(new LinearLayoutManager(this));
        mContactList.setAdapter(mAdapter);
        mAdapter.setmListener(this);
        mUserChips.addChipsListener(this);


    }

    @Override
    public void addUser(User user) {
        UiUtils.hideViewIfVisible(mEmptyView);
        mAdapter.add(user);
    }

    @Override
    public void toConversationActivity() {
        Intent intent = new Intent(this, ConversationActivity.class);
        intent.putExtra(ConversationActivity.EXTRA_ID, mGroupId);
        intent.putExtra(ConversationActivity.EXTRA_IS_GROUP, true);
        startActivity(intent);

    }

    @Override
    public void updateGroupInfo(Group group) {
        setTitle(group.getTitle());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onItemCheckedChanged(User user, boolean isChecked) {
        boolean isValidUrl = !TextUtils.isEmpty(user.getProfileImage());
        if (isChecked && isValidUrl) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(user.getProfileImage()))
                    .build();

            ImagePipeline imagePipeline = Fresco.getImagePipeline();
            DataSource<CloseableReference<CloseableImage>> dataSource =
                    imagePipeline.fetchDecodedImage(imageRequest, this);
            dataSource.subscribe(new BaseBitmapDataSubscriber() {
                @Override
                protected void onNewResultImpl(@Nullable Bitmap bitmap) {
                    BitmapDrawable drawable = new BitmapDrawable(getResources(), bitmap);
                    mUserChips.addChip(user.getUid(), drawable, user.getName(), user.getEmail());
                }

                @Override
                protected void onFailureImpl(DataSource<CloseableReference<CloseableImage>> dataSource) {

                }
            }, UiThreadImmediateExecutorService.getInstance());
        } else if (isChecked && !isValidUrl) {
            //TODO: drawable holder
            ColorDrawable colorDrawable = new ColorDrawable(ContextCompat.getColor(this, R.color.colorAccent));
            mUserChips.addChip(user.getUid(), colorDrawable, user.getName(), user.getEmail());
        } else {
            mUserChips.removeChipById(user.getUid());
        }
    }

    @OnClick(R.id.btn_add_to_group)
    public void onAddToGroupClick() {
        List<? extends ChipInterface> userSelectedList = mUserChips.getSelectedChipList();
        List<String> memberIds = new ArrayList<>();
        for (int i = 0; i < userSelectedList.size(); i++) {
            memberIds.add(userSelectedList.get(i).getId().toString());
        }
        if (memberIds.size() == 0){
            showMessage(R.string.add_member_empty_error);
        }else {
            mPresenter.addMembers(memberIds);
        }
    }


    @Override
    public void onChipAdded(ChipInterface chipInterface, int i) {
    }

    @Override
    public void onChipRemoved(ChipInterface chipInterface, int i) {
        mAdapter.setItemSelected(chipInterface.getId(), false);
    }

    @Override
    public void onTextChanged(CharSequence charSequence) {
        //TODO : implement filter list
    }
}
