package com.biennhominext.chatbase.ui.conversation;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.di.component.ActivityComponent;
import com.biennhominext.chatbase.ui.base.BaseFragment;
import com.biennhominext.chatbase.ui.conversation.imagepicker.ImagePickerSubFragment;
import com.biennhominext.chatbase.ui.custom.TabIcon;
import com.biennhominext.chatbase.ui.group.addmember.AddMemberActivity;
import com.biennhominext.chatbase.ui.imageviewer.ImageViewerActivity;
import com.biennhominext.chatbase.utils.OsUtils;
import com.biennhominext.chatbase.utils.UiUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.webianks.library.PopupBubble;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import pl.aprilapps.easyphotopicker.EasyImage;
import timber.log.Timber;
import vc908.stickerfactory.ui.OnStickerSelectedListener;
import vc908.stickerfactory.ui.fragment.StickersFragment;

/**
 * Created by bien on 7/24/2017.
 */

public class ConversationFragment extends BaseFragment implements ConversationView,
        OnImageItemClickListener, View.OnClickListener, TextWatcher, PopupBubble.PopupBubbleClickListener {
    private static final String ARG_IS_GROUP = "is_group";
    private static final String ARG_ID = "id";
    public static final int PLACE_PICKER_REQUEST = 1;
    private static final int RC_READ_STORAGE = 101;
    private int newMessageCount = 0;
    @Inject
    ConversationPresenter<ConversationView> mPresenter;
    @BindView(R.id.recycler_conversation)
    RecyclerView mConversationList;
    @BindView(R.id.edit_compose)
    EditText mComposeEditText;
    @BindView(R.id.btn_send)
    FloatingActionButton mSendButton;
    @BindView(R.id.btn_open_camera)
    TabIcon mCameraButton;
    @BindView(R.id.btn_choose_image)
    TabIcon mImageChooserButton;
    @BindView(R.id.btn_choose_sticker)
    TabIcon mStickerChooserButton;
    @BindView(R.id.btn_choose_location)
    TabIcon mLocationChooserButton;
    @BindView(R.id.view_divider)
    View mDividerView;
    @BindView(R.id.text_status)
    TextView mStatusTextView;
    @BindView(R.id.compose_message_container)
    ViewGroup mComposeMessageContainer;
    @BindView(R.id.request_message_container)
    ViewGroup mRequestMessageContainer;
    @BindView(R.id.media_chooser_panel)
    ViewGroup mMediaPanel;
    @BindDrawable(R.drawable.ic_send)
    Drawable mSendDrawable;
    @BindView(R.id.btn_ignore)
    Button mIgnoreButton;
    @BindView(R.id.btn_accept)
    Button mAcceptButton;
    @BindView(R.id.pb_new_message_count)
    PopupBubble mNewMessageCountBubble;
    private ConversationAdapter mAdapter;
    private MemberAdapter mMemberAdapter;
    String mConversationId;
    private String mConversationName = "";

    private boolean mCanSendTextMsg = false;
    private int mAttachmentSeletedIndex = -1;
    private boolean mIsGroup;

    boolean isKeyboardOpened = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conversation, container, false);
        ActivityComponent activityComponent = getActivityComponent();
        if (activityComponent != null) {
            activityComponent.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            Toolbar toolbar = view.findViewById(R.id.toolbar);
            AppCompatActivity activity = ((AppCompatActivity) getActivity());
            activity.setSupportActionBar(toolbar);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            RxBus.get().register(this);

        }
        mConversationId = getArguments().getString(ARG_ID);
        mIsGroup = getArguments().getBoolean(ARG_IS_GROUP);
        if (mIsGroup) {
            setupMemberView(view);
            setHasOptionsMenu(true);
        }

        mSendButton.setOnClickListener(v -> {
            if (TextUtils.isEmpty(mComposeEditText.getText())) {
                showMessage(R.string.conversation_send_empty_text);
            } else {
                mPresenter.sendPlainTextMessage(mComposeEditText.getText().toString());
                mComposeEditText.setText("");
            }
        });
        mComposeEditText.addTextChangedListener(this);

        mCameraButton.setOnTabClickListener(v -> {
            EasyImage.openCamera(this, 0);
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        mConversationList.setLayoutManager(layoutManager);
        mConversationList.addOnScrollListener(listener);

        mRequestMessageContainer.setOnClickListener(this);
        mImageChooserButton.setOnTabClickListener(this::openImageChooser);
        mStickerChooserButton.setOnTabClickListener(this::openStickerChooser);
        mLocationChooserButton.setOnTabClickListener(this::openPlacePicker);
        view.setOnKeyListener(((v, keycode, keyEvent) -> {
            if (keycode == KeyEvent.KEYCODE_BACK) {
                Log.d("TAG", "Back");
                return true;
            }
            return false;
        }));

        mPresenter.onAttach(this);
        mPresenter.onConversationPrepared(mConversationId, mIsGroup);
        mIgnoreButton.setOnClickListener(this);
        mAcceptButton.setOnClickListener(this);
        mNewMessageCountBubble.hide();
        mNewMessageCountBubble.setPopupBubbleListener(this);

        setListnerToRootView();
        return view;

    }

    public void setListnerToRootView() {
        final View activityRootView = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {

            int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
            if (heightDiff > 100) { // 99% of the time the height diff will be due to a keyboard.

                if (isKeyboardOpened == false) {
                    shouldAllowBackPressed();
                }
                isKeyboardOpened = true;
            } else if (isKeyboardOpened == true) {
                isKeyboardOpened = false;
            }
        });
    }

//    private void removeListenerToRootView(){
//        final View activityRootView = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
//        activityRootView.getViewTreeObserver().removeOnGlobalLayoutListener();
//    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.conversation, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_people:
                Intent intent = new Intent(getActivity(), AddMemberActivity.class);
                intent.putExtra(AddMemberActivity.EXTRA_GROUP_ID, mConversationId);
                startActivity(intent);
                break;
            case R.id.action_leave_group:
                new MaterialDialog.Builder(getActivity())
                        .title(R.string.dialog_conversation_leave_group_title)
                        .content(R.string.dialog_conversation_leave_group_content)
                        .positiveText(R.string.dialog_btn_positive)
                        .onPositive((dialog, which) -> {
                            mPresenter.leaveGroup();
                            getActivity().finish();
                        })
                        .negativeText(R.string.dialog_button_negative)
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupMemberView(View view) {
        ViewStub stub = view.findViewById(R.id.member_stub);
        RecyclerView memberRecyclerView = stub.inflate().findViewById(R.id.recycler_member);
        memberRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mMemberAdapter = new MemberAdapter();
        memberRecyclerView.setAdapter(mMemberAdapter);
    }


    @Override
    public void init(String currentUserId, boolean isOneOne) {
        mAdapter = new ConversationAdapter(currentUserId, isOneOne);
        mAdapter.setListener(this);
        mConversationList.setAdapter(mAdapter);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mPresenter.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RC_READ_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    openImageChooser(null);

                }
            }
            return;
            default:
                break;
        }
    }

    @Override
    public void addMessage(Message message) {
        boolean scrollToBottom = false;
        if (isScrolledToBottom() || message.getSender().equals(mPresenter.getCurrentUserId())) {
            scrollToBottom = true;
        }

        mAdapter.addMessage(message);
        if (scrollToBottom) {
            mConversationList.scrollToPosition(mAdapter.getItemCount() - 1);
        } else {
            newMessageCount++;
            updateBubbleView();
        }

        if (message.getMessageType().equals(Message.TYPE_ACTION_LEAVE_GROUP)) {
            mMemberAdapter.remove(message.getSender());
        }
    }

    @Subscribe
    public void onImagePicked(String path) {
        mPresenter.sendSingleImageMessage(path);
    }

    @Subscribe
    public void onImagesPicked(ArrayList<String> paths) {
        mPresenter.sendMultipleImagesMessage(paths);
    }

    @Override
    public void updateConversationInfo(User user) {
        mConversationName = user.getName();
        getActivity().setTitle(mConversationName);
        if (user.isOnline()) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(R.string.user_status_online);
        } else {
            String timeRelative = DateUtils.getRelativeTimeSpanString(
                    user.getLastOnline(), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE).toString();
            ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getString(R.string.format_user_last_active, timeRelative));
        }
        //FIXME: fix request message problem

        //update status
        if (user.getFriendStatus() == null) {
            // this user hasn't in your contact list
            mStatusTextView.setVisibility(View.VISIBLE);
            mStatusTextView.setText(getString(R.string.format_conversation_with_stranger_status, user.getName()));
            mRequestMessageContainer.setVisibility(View.GONE);
            mComposeMessageContainer.setVisibility(View.VISIBLE);
        } else if (user.getFriendStatus() == User.FLAG_FRIEND_INVITED) {
            // you sent this user an invite
            mRequestMessageContainer.setVisibility(View.GONE);
            mComposeMessageContainer.setVisibility(View.VISIBLE);
            mStatusTextView.setVisibility(View.VISIBLE);
            mStatusTextView.setText(getString(R.string.format_conversation_request_unaccepted_status, user.getName()));
        } else if (user.getFriendStatus() == User.FLAG_FRIEND_SENT) {
            // this user sent you an invite
            mStatusTextView.setVisibility(View.GONE);
            mRequestMessageContainer.setVisibility(View.VISIBLE);
            mComposeMessageContainer.setVisibility(View.GONE);
        } else {
            mStatusTextView.setVisibility(View.GONE);
            mRequestMessageContainer.setVisibility(View.GONE);
            mComposeMessageContainer.setVisibility(View.VISIBLE);
        }
    }

    public boolean shouldAllowBackPressed() {
        if (mAttachmentSeletedIndex == -1) {
            return true;
        }
        mMediaPanel.setVisibility(View.GONE);
        getChildFragmentManager().popBackStack();
        clearAllTabsSelected();
        return false;
    }

    private void clearAllTabsSelected() {
        switch (mAttachmentSeletedIndex) {
            case 0:
                mImageChooserButton.setSelected(false);
                break;
            case 2:
                mStickerChooserButton.setSelected(false);
                break;
            case 3:
                mLocationChooserButton.setSelected(false);
        }
        mAttachmentSeletedIndex = -1;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.setCurrentConversationVisible(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.setCurrentConversationVisible(false);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        RxBus.get().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void updateConversationInfo(Group group) {
        mRequestMessageContainer.setVisibility(View.GONE);
        mComposeMessageContainer.setVisibility(View.VISIBLE);
        mConversationName = group.getTitle();
        getActivity().setTitle(mConversationName);
        if (System.currentTimeMillis() - group.getTimestamp() < DateUtils.MINUTE_IN_MILLIS) {
            ((AppCompatActivity) getActivity()).getSupportActionBar()
                    .setSubtitle(getString(R.string.format_group_last_active, "now"));
        } else {
            String timeRelative = DateUtils.getRelativeTimeSpanString(
                    group.getTimestamp(), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE).toString();
            ((AppCompatActivity) getActivity()).getSupportActionBar()
                    .setSubtitle(getString(R.string.format_group_last_active, timeRelative));
        }

    }

    @Override
    public void addPeople(User user) {
        mMemberAdapter.add(user);
    }

    public static ConversationFragment newInstance(String id, boolean isGroup) {
        ConversationFragment fragment = new ConversationFragment();
        Bundle bundle = new Bundle();

        bundle.putString(ARG_ID, id);
        bundle.putBoolean(ARG_IS_GROUP, isGroup);
        fragment.setArguments(bundle);
        return fragment;
    }

    private boolean isScrolledToBottom() {
        if (mConversationList.getChildCount() == 0) {
            return true;
        }
        final View lastView = mConversationList.getChildAt(mConversationList.getChildCount() - 1);
        int lastVisibleItem = ((LinearLayoutManager) mConversationList
                .getLayoutManager()).findLastVisibleItemPosition();
        if (lastVisibleItem < 0) {
            // If the recyclerView height is 0, then the last visible item position is -1
            // Try to compute the position of the last item, even though it's not visible
            final long id = mConversationList.getChildItemId(lastView);
            final RecyclerView.ViewHolder holder = mConversationList.findViewHolderForItemId(id);
            if (holder != null) {
                lastVisibleItem = holder.getAdapterPosition();
            }
        }
        final int totalItemCount = mConversationList.getAdapter().getItemCount();
        final boolean isAtBottom = (lastVisibleItem + 1) == totalItemCount;
        return isAtBottom && lastView.getBottom() <= mConversationList.getHeight();
    }

    private final RecyclerView.OnScrollListener listener = new RecyclerView.OnScrollListener() {
        private boolean mWasScrolledToBottom = true;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (mWasScrolledToBottom != isScrolledToBottom()) {

                mDividerView.animate().alpha(isScrolledToBottom() ? 0f : 1f);
                mWasScrolledToBottom = isScrolledToBottom();
                if (mWasScrolledToBottom) {
                    newMessageCount = 0;
                    mNewMessageCountBubble.hide();
                } else {
                    mNewMessageCountBubble.show();
                    updateBubbleView();
                }
            }

        }
    };

    private void updateBubbleView() {
        String text;
        if (newMessageCount == 0) {
            text = getString(R.string.conversation_scroll_to_bottom_tooltip);
        } else {
            text = getResources().getQuantityString(R.plurals.conversation_number_of_new_messages_tooltip, newMessageCount, newMessageCount);
        }
        mNewMessageCountBubble.updateText(text);
    }

    @Override
    public void onImageItemClick(String path, View view) {
        String transtionName = ViewCompat.getTransitionName(view);
        Intent intent = new Intent(getActivity(), ImageViewerActivity.class);
        intent.putExtra(ImageViewerActivity.ARG_CONVERSATION_NAME, mConversationName);
        intent.putExtra(ImageViewerActivity.ARG_TRANSITION_NAME, transtionName);
        intent.putExtra(ImageViewerActivity.ARG_IMAGE_URI, path);

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                getActivity(),
                view,
                transtionName);

        startActivity(intent, options.toBundle());

        // FIX BUG https://github.com/facebook/fresco/issues/1445
        ActivityCompat.setExitSharedElementCallback(getActivity(),
                new android.support.v4.app.SharedElementCallback() {
                    @Override
                    public void onSharedElementEnd(List<String> sharedElementNames,
                                                   List<View> sharedElements, List<View> sharedElementSnapshots) {
                        super.onSharedElementEnd(sharedElementNames, sharedElements,
                                sharedElementSnapshots);
                        for (final View view : sharedElements) {
                            if (view instanceof SimpleDraweeView) {
                                view.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });

        ActivityCompat.startActivity(getActivity(),
                intent,
                options.toBundle());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ignore:
                new MaterialDialog.Builder(getActivity())
                        .title(R.string.dialog_invites_ingore_title)
                        .content(R.string.dialog_invites_ingore_content)
                        .negativeText(R.string.action_cancel)
                        .positiveText(R.string.action_ok)
                        .positiveColorRes(R.color.colorAccent)
                        .onPositive((dialog, which) -> {
                            mPresenter.ignoreInvitation(mConversationId);
                            getActivity().finish();
                        })
                        .show();
                break;
            case R.id.btn_accept:
                mPresenter.acceptInvitation(mConversationId);
                break;
            default:
                break;
        }
    }


    private void openImageChooser(View view) {
        if (!OsUtils.hasReadStoragePermission(getActivity())) {
            OsUtils.requestPermission(getActivity(), RC_READ_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
            return;
        }
        if (mAttachmentSeletedIndex == 0) {
            return;
        }
        if (mMediaPanel.getVisibility() != View.VISIBLE) {
            mMediaPanel.setVisibility(View.VISIBLE);
        }
        clearAllTabsSelected();
        mImageChooserButton.setSelected(true);
        UiUtils.hideImeKeyboard(getActivity(), mComposeEditText);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.media_chooser_panel, new ImagePickerSubFragment())
                .addToBackStack(null)
                .commit();
        mAttachmentSeletedIndex = 0;
    }

    private void openStickerChooser(View view) {
        if (mAttachmentSeletedIndex == 2) {
            return;
        }
        if (mMediaPanel.getVisibility() != View.VISIBLE) {
            mMediaPanel.setVisibility(View.VISIBLE);
        }
        clearAllTabsSelected();
        mStickerChooserButton.setSelected(true);
        UiUtils.hideImeKeyboard(getActivity(), mComposeEditText);
        StickersFragment stickersFragment = new StickersFragment();
        stickersFragment.setOnStickerSelectedListener(stickerSelectedListener);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.media_chooser_panel, stickersFragment)
                .addToBackStack(null)
                .commit();
        mAttachmentSeletedIndex = 2;
    }

    private void openPlacePicker(View view) {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            Timber.e(e.getMessage());
            showMessage(e.getMessage());
        } catch (GooglePlayServicesNotAvailableException e) {
            Timber.e(e.getMessage());
            showMessage(e.getMessage());
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        boolean canSendTextMsg;
        if (TextUtils.isEmpty(charSequence)) {
            canSendTextMsg = false;
        } else {
            canSendTextMsg = true;
        }

        if (canSendTextMsg && !mCanSendTextMsg) {
            mSendButton.hide();
            new Handler().postDelayed(() -> {
                Drawable enabledSendDrawable = mSendDrawable.getConstantState().newDrawable();
                enabledSendDrawable.mutate().setColorFilter(0xffffffff, PorterDuff.Mode.SRC_ATOP);
                mSendButton.setImageDrawable(enabledSendDrawable);
                mSendButton.setBackgroundTintList(ColorStateList.valueOf(0xff03A9F4));
                mSendButton.show();
            }, 150);
            mSendButton.setEnabled(true);
            mCanSendTextMsg = canSendTextMsg;

        } else if (!canSendTextMsg && mCanSendTextMsg) {
            mSendButton.hide();
            new Handler().postDelayed(() -> {
                mSendButton.setImageDrawable(mSendDrawable);
                mSendButton.setBackgroundTintList(ColorStateList.valueOf(0xffECEFF1));
                mSendButton.show();
            }, 150);
            mSendButton.setEnabled(false);
            mCanSendTextMsg = canSendTextMsg;
        }

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private OnStickerSelectedListener stickerSelectedListener = new OnStickerSelectedListener() {
        @Override
        public void onStickerSelected(String code) {
            mPresenter.sendStickerMessage(code);
        }

        @Override
        public void onEmojiSelected(String emoji) {
            mComposeEditText.append(emoji);
        }
    };


    @Override
    public void bubbleClicked(Context context) {
        mConversationList.scrollToPosition(mAdapter.getItemCount() - 1);
    }
}
