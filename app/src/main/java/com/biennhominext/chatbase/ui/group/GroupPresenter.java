package com.biennhominext.chatbase.ui.group;

import com.biennhominext.chatbase.ui.base.BasePresenter;

/**
 * Created by hbien on 7/28/2017.
 */

public interface GroupPresenter<V extends GroupView> extends BasePresenter<V> {

}
