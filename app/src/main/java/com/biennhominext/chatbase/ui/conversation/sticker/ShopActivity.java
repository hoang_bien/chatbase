package com.biennhominext.chatbase.ui.conversation.sticker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;

import com.biennhominext.chatbase.R;

import io.reactivex.annotations.NonNull;
import vc908.stickerfactory.StickersManager;
import vc908.stickerfactory.billing.PricePoint;
import vc908.stickerfactory.ui.activity.ShopWebViewActivity;

/**
 * Created by hbien on 8/15/2017.
 */

public class ShopActivity extends ShopWebViewActivity {
    protected void onPurchase(String packTitle, final String packName, PricePoint pricePoint) {
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Charge dialog")
                .setMessage("Purchase " + packTitle + " for " + pricePoint.getLabel() + "?")
                .setPositiveButton("Purchase", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        StickersManager.onPackPurchased(packName);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        dialog.dismiss();
                        onPurchaseFail();
                    }
                })
                .setCancelable(false)
                .create();
        dialog.setOnShowListener(dialogInterface -> {
            int primaryColor = ContextCompat.getColor(ShopActivity.this, R.color.colorPrimary);
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(primaryColor);
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(primaryColor);
        });
        dialog.show();
    }
}
