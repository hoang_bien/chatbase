package com.biennhominext.chatbase.ui.group.creating;

import com.biennhominext.chatbase.ui.base.BasePresenter;

/**
 * Created by hbien on 7/28/2017.
 */

public interface CreateGroupPresenter<V extends CreateGroupView> extends BasePresenter<V> {
    void createGroup(String groupName, String description);
}
