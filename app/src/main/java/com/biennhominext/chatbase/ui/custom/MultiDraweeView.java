package com.biennhominext.chatbase.ui.custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.DraweeHolder;
import com.facebook.drawee.view.MultiDraweeHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hbien on 7/31/2017.
 */

public class MultiDraweeView extends View {
    private static final int MAX_COUNT = 3;
    private boolean mIsCircle = true;
    private Path mPath = new Path();
    private RectF mRect = new RectF();

    private MultiDraweeHolder<GenericDraweeHierarchy> mMultiDraweeHolder;
    private ArrayList<GenericDraweeHierarchy> hierarchies;

    public MultiDraweeView(Context context) {
        super(context);
        init();
    }

    public MultiDraweeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MultiDraweeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mMultiDraweeHolder = new MultiDraweeHolder<>();
        hierarchies = new ArrayList<>();
        for (int i = 0; i < MAX_COUNT; i++) {
            hierarchies.add(new GenericDraweeHierarchyBuilder(getResources())
                    .build());
        }
    }

    public void setCircle(boolean circle) {
        mIsCircle = circle;
    }

    public void setImageUris(List<String> uris) {
        mMultiDraweeHolder.clear();
        int length = Math.min(MAX_COUNT, uris.size());
        for (int i = 0; i < length; i++) {
            final DraweeHolder<GenericDraweeHierarchy> draweeHolder = DraweeHolder.create(hierarchies.get(i), getContext());
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setUri(uris.get(i))
                    .setOldController(draweeHolder.getController())
                    .build();
            draweeHolder.setController(controller);
            draweeHolder.getTopLevelDrawable().setCallback(this);
            mMultiDraweeHolder.add(draweeHolder);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        final int w = getWidth();
        final int h = getHeight();
        final int halfWidth = w / 2;
        final int halfHeight = h / 2;
        if (mIsCircle) {
            mPath.reset();
            mRect.set(0, 0, w, h);
            mPath.addOval(mRect, Path.Direction.CW);
            canvas.clipPath(mPath);

        }

        Drawable drawable1 = null;
        Drawable drawable2 = null;
        Drawable drawable3 = null;

        switch (mMultiDraweeHolder.size()) {
            case 1:
                drawable1 = mMultiDraweeHolder.get(0).getTopLevelDrawable();
                drawable1.setBounds(0,0,w,h);
                drawable1.draw(canvas);
                break;
            case 2:
                drawable1 = mMultiDraweeHolder.get(0).getTopLevelDrawable();
                drawable1.setBounds(0,0,halfWidth,h);
                drawable1.draw(canvas);

                drawable2 = mMultiDraweeHolder.get(1).getTopLevelDrawable();
                drawable2.setBounds(halfWidth,0,w,h);
                drawable2.draw(canvas);
                break;
            case 3:
                drawable1 = mMultiDraweeHolder.get(0).getTopLevelDrawable();
                drawable1.setBounds(0,0,halfWidth,h);
                drawable1.draw(canvas);

                drawable2 = mMultiDraweeHolder.get(1).getTopLevelDrawable();
                drawable2.setBounds(halfWidth,0,w,halfHeight);
                drawable2.draw(canvas);

                drawable3 = mMultiDraweeHolder.get(2).getTopLevelDrawable();
                drawable3.setBounds(halfWidth,halfHeight,w,h);
                drawable3.draw(canvas);
                break;
            default:
        }

    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected boolean verifyDrawable(@NonNull Drawable who) {
        return mMultiDraweeHolder.verifyDrawable(who);
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mMultiDraweeHolder.onDetach();
    }

    @Override
    public void onStartTemporaryDetach() {
        super.onStartTemporaryDetach();
        mMultiDraweeHolder.onDetach();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        mMultiDraweeHolder.onAttach();
    }

    @Override
    public void onFinishTemporaryDetach() {
        super.onFinishTemporaryDetach();
        mMultiDraweeHolder.onAttach();
    }


}
