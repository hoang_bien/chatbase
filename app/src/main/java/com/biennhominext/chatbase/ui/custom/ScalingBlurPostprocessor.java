package com.biennhominext.chatbase.ui.custom;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.facebook.common.references.CloseableReference;
import com.facebook.imagepipeline.bitmaps.PlatformBitmapFactory;
import com.facebook.imagepipeline.nativecode.NativeBlurFilter;
import com.facebook.imagepipeline.request.BasePostprocessor;

/**
 * Applies a blur filter using the {@link NativeBlurFilter#iterativeBoxBlur(Bitmap, int, int)} and
 * down-scales the bitmap beforehand.
 */
public class ScalingBlurPostprocessor extends BasePostprocessor {

    /**
     * A scale ration of 4 means that we reduce the total number of pixels to process by factor 16.
     */
    private static final int SCALE_RATIO = 4;
    private static final int BLUR_RADIUS = 25;
    private static final int BLUR_ITERATIONS = 3;

    private final Paint mPaint = new Paint();

    @Override
    public CloseableReference<Bitmap> process(
            Bitmap sourceBitmap,
            PlatformBitmapFactory bitmapFactory) {

        final CloseableReference<Bitmap> bitmapRef = bitmapFactory.createBitmap(
                sourceBitmap.getWidth() / SCALE_RATIO,
                sourceBitmap.getHeight() / SCALE_RATIO);

        try {
            final Bitmap destBitmap = bitmapRef.get();
            final Canvas canvas = new Canvas(destBitmap);

            canvas.drawBitmap(
                    sourceBitmap,
                    null,
                    new Rect(0, 0, destBitmap.getWidth(), destBitmap.getHeight()),
                    mPaint);

            NativeBlurFilter.iterativeBoxBlur(destBitmap,BLUR_ITERATIONS, BLUR_RADIUS / SCALE_RATIO);

            return CloseableReference.cloneOrNull(bitmapRef);
        } finally {
            CloseableReference.closeSafely(bitmapRef);
        }
    }
}