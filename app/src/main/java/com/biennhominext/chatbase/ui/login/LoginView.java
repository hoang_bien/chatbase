package com.biennhominext.chatbase.ui.login;

import com.biennhominext.chatbase.ui.base.BaseView;

/**
 * Created by bien on 7/6/2017.
 */

public interface LoginView extends BaseView {

    void toMainActivity();
}
