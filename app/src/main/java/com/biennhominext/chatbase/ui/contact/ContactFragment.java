package com.biennhominext.chatbase.ui.contact;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.di.component.ActivityComponent;
import com.biennhominext.chatbase.ui.OnItemClickListener;
import com.biennhominext.chatbase.ui.base.BaseFragment;
import com.biennhominext.chatbase.ui.conversation.ConversationActivity;
import com.biennhominext.chatbase.utils.UiUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bien on 7/19/2017.
 */

public class ContactFragment extends BaseFragment implements ContactView, OnItemClickListener {
    @BindView(R.id.recycler_contact)
    RecyclerView mContactList;
    @BindView(R.id.viewgroup_empty)
    View mEmptyView;
    @Inject
    ContactPresenter<ContactView> mPresenter;
    ContactAdapter mAdapter;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact,container,false);
        ActivityComponent activityComponent = getActivityComponent();
        if (activityComponent != null) {
            activityComponent.inject(this);
            setUnBinder(ButterKnife.bind(this,view));
            mPresenter.onAttach(this);
        }
        mAdapter = new ContactAdapter();
        mAdapter.setListener(this);
        mContactList.setLayoutManager(new GridLayoutManager(getActivity(),getResources().getInteger(R.integer.grid_contact_count)));
        mContactList.setAdapter(mAdapter);
        return view;

    }

    @Override
    public void addContact(User user) {
        UiUtils.hideViewIfVisible(mEmptyView);
        mAdapter.add(user);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }



    @Override
    public void onItemClick(Object object, int position) {
        User user = (User) object;
        Intent intent = new Intent(getActivity(), ConversationActivity.class);
        intent.putExtra(ConversationActivity.EXTRA_ID, user.getUid());
        intent.putExtra(ConversationActivity.EXTRA_IS_GROUP,false);
        getActivity().startActivity(intent);
    }


}
