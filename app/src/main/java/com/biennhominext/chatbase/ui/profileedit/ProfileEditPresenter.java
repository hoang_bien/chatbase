package com.biennhominext.chatbase.ui.profileedit;

import android.net.Uri;

import com.biennhominext.chatbase.ui.base.BasePresenter;

/**
 * Created by bien on 7/17/2017.
 */

public interface ProfileEditPresenter<T extends ProfileEditView> extends BasePresenter<T> {
    void updateUserInfo(String username, String email, int gender, Uri imageFile);
}
