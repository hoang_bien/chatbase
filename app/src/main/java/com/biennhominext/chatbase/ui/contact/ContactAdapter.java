package com.biennhominext.chatbase.ui.contact;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.OnItemClickListener;
import com.biennhominext.chatbase.ui.custom.AvatarDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bien on 7/19/2017.
 */

public class ContactAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<User> mContacts;
    private OnItemClickListener mListener;


    public ContactAdapter() {
        this.mContacts = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact,parent,false);
        ContactHolder holder =  new ContactHolder(view);
        view.setOnClickListener(view1 -> {
            if (mListener != null) {
                int position = holder.getAdapterPosition();
                mListener.onItemClick(mContacts.get(position),position);
            }
        });
        return holder;
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ContactHolder contactHolder = (ContactHolder) viewHolder;
        User user = mContacts.get(position);
        contactHolder.textName.setText(user.getName());
        if (!TextUtils.isEmpty(user.getProfileImage())){
            contactHolder.imageAvatar.setOnlineStatus(user.isOnline());
            contactHolder.imageAvatar.setImageUri(user.getProfileImage());
        }
    }

    public void setListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    public void add(User user) {
        int i = 0;
        while (i < mContacts.size()){
            if (mContacts.get(i).equals(user)){
                mContacts.set(i,user);
                notifyItemChanged(i);
                break;
            }
            i++;
        }
        if (i == mContacts.size()){
            mContacts.add(user);
            notifyItemInserted(i);
        }

    }

    public static class ContactHolder extends RecyclerView.ViewHolder{
        AvatarDraweeView imageAvatar;
        TextView textName;
        public ContactHolder(View itemView) {
            super(itemView);
            imageAvatar = itemView.findViewById(R.id.image_avatar);
            textName = itemView.findViewById(R.id.text_name);
        }
    }

}
