package com.biennhominext.chatbase.ui.profile;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 7/20/2017.
 */

public class ProfilePresenterImpl<V extends ProfileView> extends BasePresenterImpl<V> implements ProfilePresenter<V> {
    @Inject
    public ProfilePresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dataManager, scheduler, compositeDisposable);
    }

    @Override
    public void onAttach(V view) {
        super.onAttach(view);
        getDisposable().add(getDataManager().getAccountInfo()
                .subscribeOn(getScheduler().io())
                .observeOn(getScheduler().ui())
        .subscribe(getView()::updateUserInfo));
    }

    @Override
    public void logout() {
        getView().toLogin();
        getDataManager().logout();
    }
}
