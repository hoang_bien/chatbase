package com.biennhominext.chatbase.ui.imageviewer;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by hbien on 8/9/2017.
 */

public class ImageViewerPresenterImpl<V extends ImageViewerView> extends BasePresenterImpl<V> implements ImageViewerPresenter<V> {

    @Inject
    public ImageViewerPresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dataManager, scheduler, compositeDisposable);
    }

}
