package com.biennhominext.chatbase.ui.custom;

import android.graphics.Bitmap;

import com.facebook.imagepipeline.request.BasePostprocessor;

/**
 * Created by hbien on 7/31/2017.
 */

public class DarkenPostProcessor extends BasePostprocessor {
    private float mFraction;

    public DarkenPostProcessor(float fraction) {
        this.mFraction = fraction;
    }

    @Override
    public void process(Bitmap bitmap) {
        final int w = bitmap.getWidth();
        final int h = bitmap.getHeight();
        int[] pixels = new int[w*h];
        bitmap.getPixels(pixels,0,w,0,0,w,h);

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                final int offset = j * w + i;
                pixels[offset] = getDarkenColor(pixels[offset]);
            }
        }

        bitmap.setPixels(pixels,0,w,0,0,w,h);
    }

    private int getDarkenColor(int color){
        final int alpha = color & 0xFF000000;
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = color & 0xFF;

        r *= mFraction;
        g *= mFraction;
        b *= mFraction;

        return alpha | r << 16 | g << 8 | b;
    }
}
