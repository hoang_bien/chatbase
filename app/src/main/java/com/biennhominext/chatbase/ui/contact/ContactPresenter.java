package com.biennhominext.chatbase.ui.contact;

import com.biennhominext.chatbase.ui.base.BasePresenter;

/**
 * Created by bien on 7/19/2017.
 */

public interface ContactPresenter<V extends ContactView> extends BasePresenter<V>{

}
