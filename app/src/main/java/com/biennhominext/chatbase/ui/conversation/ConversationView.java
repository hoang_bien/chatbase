package com.biennhominext.chatbase.ui.conversation;

import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BaseView;

/**
 * Created by bien on 7/24/2017.
 */

public interface ConversationView extends BaseView {
    void init(String isOneOne, boolean b);

    void addMessage(Message message);

    void updateConversationInfo(User user);
    void updateConversationInfo(Group group);

    void addPeople(User user);
}