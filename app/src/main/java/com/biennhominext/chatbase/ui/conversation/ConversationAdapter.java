package com.biennhominext.chatbase.ui.conversation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bien on 7/24/2017.
 */

public class ConversationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int MSG_TYPE_NOFICATION = 1;
    private static final int MSG_TYPE_NORMAL = 0;
    private List<Message> mMessageList;
    private String mCurrentUserId;

    private OnImageItemClickListener mListener;
    private boolean mIsOneOne;
    private Message mTrackingMessage = null;

    public ConversationAdapter(String currentUserId, boolean isOneOne) {
        mMessageList = new ArrayList<>();
        mCurrentUserId = currentUserId;
        mIsOneOne = isOneOne;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == MSG_TYPE_NORMAL) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_conversation_message, parent, false);
            return new ConversationHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_conversation_notification, parent, false);
            return new ConversationNotificationHolder(view);
        }
    }

    public void setListener(OnImageItemClickListener mListener) {
        this.mListener = mListener;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ConversationHolder) {
            ConversationHolder conversationHolder = (ConversationHolder) holder;
            Message previousMessage = null;
            Message nextMessage = null;
            if (position > 0) {
                previousMessage = mMessageList.get(position - 1);
            }
            if (position + 1 < mMessageList.size()) {
                nextMessage = mMessageList.get(position + 1);
            }
            conversationHolder.bind(mMessageList.get(position), previousMessage, nextMessage,
                    mCurrentUserId, mListener, position + 1 == mMessageList.size(), mIsOneOne);
        } else {
            ConversationNotificationHolder notificationHolder = (ConversationNotificationHolder) holder;
            notificationHolder.bind(mMessageList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }


    public void addMessage(Message message) {
        int i;
        for (i = 0; i < mMessageList.size(); i++) {
            if (mMessageList.get(i).equals(message)) {
                mMessageList.set(i, message);
                if (i > 0) {
                    notifyItemChanged(i - 1);
                }
                notifyItemChanged(i);
                break;
            }
        }
        if (i == mMessageList.size()) {
            mMessageList.add(message);
            if (i > 0) {
                notifyItemChanged(i - 1);
            }
            notifyItemInserted(i);
        }
    }

    @Override
    public int getItemViewType(int position) {
        final String messageType = mMessageList.get(position).getMessageType();
        if (messageType.startsWith("group")) {
            return MSG_TYPE_NOFICATION;
        } else {
            return MSG_TYPE_NORMAL;
        }
    }
}
