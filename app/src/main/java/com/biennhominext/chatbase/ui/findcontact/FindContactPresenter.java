package com.biennhominext.chatbase.ui.findcontact;

import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BasePresenter;

/**
 * Created by hbien on 8/2/2017.
 */

public interface FindContactPresenter<V extends FindContactView> extends BasePresenter<V> {

    void addOrRemoveFriend(User object);

    void findUserByName(String query);
}
