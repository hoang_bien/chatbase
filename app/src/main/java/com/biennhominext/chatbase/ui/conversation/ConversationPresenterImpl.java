package com.biennhominext.chatbase.ui.conversation;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by bien on 7/24/2017.
 */

public class ConversationPresenterImpl<V extends ConversationView> extends BasePresenterImpl<V> implements ConversationPresenter<V> {
    private String mTargetId;
    private boolean mIsGroup;
    private Integer friendStatus;
    private Disposable lastMessageStatusDisposable;

    @Inject
    public ConversationPresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dataManager, scheduler, compositeDisposable);
    }

    @Override
    public void onAttach(V view) {
        super.onAttach(view);

    }

    @Override
    public void onConversationPrepared(String targetId, boolean isGroup) {
        mTargetId = targetId;
        mIsGroup = isGroup;
        getView().init(getDataManager().getCurrentUserId(), !mIsGroup);
        if (!isGroup) {
            getDisposable().add(getDataManager()
                    .getPublicUserInfo(targetId)
                    .subscribeOn(getScheduler().computation())
                    .observeOn(getScheduler().ui())
                    .doAfterNext(user -> {
                        Log.d("ConversationFragment", "truy to get invite message");
                        if (user.getFriendStatus() != null && user.getFriendStatus() == User.FLAG_FRIEND_SENT) {
                            // this user sent you an invite, try to get this invite message
                            getDataManager().getInviteMessageFromUser(user.getUid())
                                    .subscribe(getView()::addMessage);
                        }
                    })
                    .subscribe(getView()::updateConversationInfo));


            getDisposable().add(getDataManager()
                    .getConversationForUser(targetId)
                    .subscribeOn(getScheduler().computation())
                    .observeOn(getScheduler().ui())
                    .doAfterNext(message -> {
                        if (mTargetId.equals(message.getSender())) {
                            getDataManager().markUserMessageAsRead(message.getSender(), message.getUid());
                        }
                        //getLastMessageStatus(message.getUid());
                    })
                    .subscribe(getView()::addMessage));


        } else {
            getDisposable().add(getDataManager()
                    .getGroupInfo(targetId)
                    .subscribeOn(getScheduler().computation())
                    .observeOn(getScheduler().ui())
                    .subscribe(getView()::updateConversationInfo));
            getDisposable().add(getDataManager()
                    .getGroupConversationForUser(targetId)
                    .subscribeOn(getScheduler().computation())
                    .observeOn(getScheduler().ui())
                    .doAfterNext(message -> {
                        if (!getDataManager().getCurrentUserId().equals(message.getSender())) {
                            getDataManager().markGroupMessageAsRead(mTargetId, message.getUid());
                        }
//                        if (!message.getMessageType().startsWith("group")){
//                            getLastMessageStatus(message.getUid());
//                        }
                    })
                    .subscribe(getView()::addMessage));
            getDisposable().add(getDataManager().getUsersInGroup(mTargetId)
                    .subscribeOn(getScheduler().computation())
                    .observeOn(getScheduler().ui())
                    .subscribe(getView()::addPeople));


        }
    }

    @Override
    public boolean onActivityResult(int reqCode, int resultCode, Intent data) {
//        EasyImage.handleActivityResult(reqCode, resultCode, data, ((Fragment) getView()).getActivity(), new DefaultCallback() {
//            @Override
//            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
//                List<String> paths = new ArrayList<>();
//                for (int i = 0; i < imageFiles.size(); i++) {
//                    paths.add(imageFiles.get(i).getAbsolutePath());
//                }
//                final Message message = getDataManager().sendMultipleImageMessage(paths, mTargetId, mIsGroup);
//                Timber.i("Sending images: "+ message.getContent());
//                getView().addMessage(message);
//            }
//        });
        if (reqCode == ConversationFragment.PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(((Fragment) getView()).getActivity(), data);
                LatLng latLng = place.getLatLng();
                getDataManager().sendLocationMessage(latLng.latitude, latLng.longitude, mTargetId, mIsGroup);


                return true;
            }
        }
        return false;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void ignoreInvitation(String mConversationId) {
        getDataManager().ignoreInvitation(mConversationId);
    }

    @Override
    public void acceptInvitation(String mConversationId) {
        getDataManager().acceptInvitation(mConversationId);
    }

    @Override
    public void sendSingleImageMessage(String path) {
        Message tempMessage = getDataManager().sendSingleImageMessage(path, mTargetId, mIsGroup);
        getView().addMessage(tempMessage);
    }

    @Override
    public void sendMultipleImagesMessage(List<String> paths) {
        final Message message = getDataManager().sendMultipleImageMessage(paths, mTargetId, mIsGroup);
        getView().addMessage(message);
    }

    @Override
    public void sendStickerMessage(String code) {
        getDataManager().sendStickerMessage(code, mTargetId, mIsGroup);
    }

    @Override
    public void leaveGroup() {
        getDataManager().leaveGroup(mTargetId);
    }

    @Override
    public void setCurrentConversationVisible(boolean visible) {
        if (visible) {
            getDataManager().setCurrentConversationId(mTargetId);
        } else {
            getDataManager().clearCurrentConversationId();
        }
    }

    @Override
    public String getCurrentUserId() {
        return getDataManager().getCurrentUserId();
    }

    @Override
    public void sendPlainTextMessage(String message) {
        if (mIsGroup) {
            getDataManager().sendPlainTextMessageToGroup(message, mTargetId);
        } else {
            getDataManager().sendPlainTextMessageToUser(message, mTargetId);
        }
    }

    private void getLastMessageStatus(String messageId) {
        if (lastMessageStatusDisposable != null) {
            getDisposable().remove(lastMessageStatusDisposable);
        }
        lastMessageStatusDisposable = getDataManager().getLastMessageStatus(messageId, mTargetId, mIsGroup)
                .subscribe(profileUrl->getView());
        getDisposable().add(lastMessageStatusDisposable);
    }
}
