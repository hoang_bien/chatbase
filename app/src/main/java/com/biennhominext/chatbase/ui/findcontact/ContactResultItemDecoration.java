package com.biennhominext.chatbase.ui.findcontact;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.biennhominext.chatbase.di.ActivityContext;
import com.biennhominext.chatbase.utils.UiUtils;

import javax.inject.Inject;

/**
 * Created by hbien on 8/9/2017.
 */

public class ContactResultItemDecoration extends RecyclerView.ItemDecoration {
    private Rect mBounds;
    private ColorDrawable mDivider;
    private Context mContext;
    private int mDividerHeight;

    @Inject
    public ContactResultItemDecoration(@ActivityContext Context context) {
        mContext = context;
        mBounds = new Rect();
        mDividerHeight = UiUtils.dpToPx(context.getResources(),1);
        mDivider = new ColorDrawable(0xffe0e0e0);

    }

    @Override
    public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        canvas.save();
        final int right = parent.getWidth();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount -1; i++) {
            final View child = parent.getChildAt(i);
            int position = parent.getChildAdapterPosition(child);
            int viewType = parent.getAdapter().getItemViewType(position);

            if (viewType == ContactResultAdapter.VIEW_TYPE_CONTENT){
                parent.getDecoratedBoundsWithMargins(child, mBounds);
                final int top = mBounds.bottom + Math.round(child.getTranslationY());
                final int bottom = top + mDividerHeight;
                mDivider.setBounds(0, top, right, bottom);
                mDivider.draw(canvas);
            }

        }
        canvas.restore();
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        int viewType = parent.getAdapter().getItemViewType(position);
        if (viewType == ContactResultAdapter.VIEW_TYPE_CONTENT) {
            outRect.set(0, 0, 0, mDividerHeight);
        } else {
            outRect.setEmpty();
        }
    }
}
