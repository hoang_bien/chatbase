package com.biennhominext.chatbase.ui.conversation;

import android.content.Intent;

import com.biennhominext.chatbase.ui.base.BasePresenter;

import java.util.List;

/**
 * Created by bien on 7/24/2017.
 */

public interface ConversationPresenter<V extends ConversationView> extends BasePresenter<V> {
    void onConversationPrepared(String targetId, boolean isGroup);

    void sendPlainTextMessage(String message);

    boolean onActivityResult(int requsetCode, int resultCode, Intent data);

    void ignoreInvitation(String mConversationId);

    void acceptInvitation(String mConversationId);

    void sendSingleImageMessage(String path);

    void sendMultipleImagesMessage(List<String> paths);

    void sendStickerMessage(String code);

    void leaveGroup();

    void setCurrentConversationVisible(boolean visible);

    String getCurrentUserId();
}
