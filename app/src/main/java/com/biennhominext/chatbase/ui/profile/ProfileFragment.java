package com.biennhominext.chatbase.ui.profile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.di.component.ActivityComponent;
import com.biennhominext.chatbase.ui.base.BaseFragment;
import com.biennhominext.chatbase.ui.login.LoginActivity;
import com.biennhominext.chatbase.ui.profileedit.ProfileEditActivity;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bien on 7/20/2017.
 */

public class ProfileFragment extends BaseFragment  implements ProfileView{
    @Inject
     ProfilePresenter<ProfileView> mPresenter;
    @BindView(R.id.image_avatar)
    SimpleDraweeView mProfileImageView;
    @BindView(R.id.text_name)
    TextView mNameTextView;
    @BindView(R.id.text_email_detail)
    TextView mEmailTextView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile,container,false);
        ActivityComponent activityComponent = getActivityComponent();
        if (activityComponent != null) {
            activityComponent.inject(this);
            setUnBinder(ButterKnife.bind(this,view));
            mPresenter.onAttach(this);
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @OnClick(R.id.btn_edit)
    public void toProfileedit(){
        Intent intent = new Intent(getActivity(), ProfileEditActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.profile,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_logout:
                mPresenter.logout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateUserInfo(User user) {
        mNameTextView.setText(user.getName());
        mEmailTextView.setText(user.getEmail());
        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(user.getProfileImage()))
                .setProgressiveRenderingEnabled(true)
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(imageRequest)
                .setOldController(mProfileImageView.getController())
                .build();
        mProfileImageView.setController(controller);
    }

    @Override
    public void toLogin() {
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }
    
}
