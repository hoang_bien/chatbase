package com.biennhominext.chatbase.ui.group.creating;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.transition.ArcMotion;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.TextView;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.ui.base.BaseActivity;
import com.biennhominext.chatbase.ui.group.addmember.AddMemberActivity;
import com.biennhominext.chatbase.ui.transition.MorphDialogToFab;
import com.biennhominext.chatbase.ui.transition.MorphFabToDialog;
import com.biennhominext.chatbase.ui.transition.MorphTransition;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by hbien on 7/28/2017.
 */

public class CreateActivityGroup extends BaseActivity implements CreateGroupView {
    @Inject
    CreateGroupPresenter<CreateGroupView> mPresenter;
    @BindView(R.id.text_title)
    TextView mTitleTextView;
    @BindView(R.id.text_description)
    TextView mDescriptionTextView;
    @BindView(R.id.container)
    ViewGroup mContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_group);
        getActivityComponent().inject(this);
        setUnbinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setupTransitionIn();
        }
    }

    @OnClick(R.id.btn_positive)
    public void onPositiveButtonClick() {
        String groupName = mTitleTextView.getText().toString();
        String groupDescription = mDescriptionTextView.getText().toString();
        if (validateGroup(groupName, groupDescription)) {
            mPresenter.createGroup(groupName, groupDescription);

        }
    }


    @OnClick(R.id.btn_negative)
    public void onNegativeButtonClick() {
        dismiss();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setupTransitionIn() {
        ArcMotion arcMotion = new ArcMotion();
        arcMotion.setMinimumHorizontalAngle(50f);
        arcMotion.setMinimumVerticalAngle(50f);

        Interpolator easeInOut = AnimationUtils.loadInterpolator(this, android.R.interpolator.fast_out_slow_in);

        MorphFabToDialog sharedEnter = new MorphFabToDialog();
        sharedEnter.setPathMotion(arcMotion);
        sharedEnter.setInterpolator(easeInOut);

        MorphDialogToFab sharedReturn = new MorphDialogToFab();
        sharedReturn.setPathMotion(arcMotion);
        sharedReturn.setInterpolator(easeInOut);

        if (mContainer != null) {
            sharedEnter.addTarget(mContainer);
            sharedReturn.addTarget(mContainer);
        }

        getWindow().setSharedElementEnterTransition(sharedEnter);
        getWindow().setSharedElementReturnTransition(sharedReturn);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setupTransitionOut() {
        ArcMotion arcMotion = new ArcMotion();
        arcMotion.setMinimumHorizontalAngle(50f);
        arcMotion.setMinimumVerticalAngle(50f);

        Interpolator easeInOut = AnimationUtils.loadInterpolator(this, android.R.interpolator.fast_out_slow_in);

        //hujiawei 100是随意给的一个数字，可以修改，需要注意的是这里调用container.getHeight()结果为0
        MorphTransition sharedEnter = new MorphTransition(ContextCompat.getColor(this, R.color.colorAccent),
                ContextCompat.getColor(this, R.color.white), 100, getResources().getDimensionPixelSize(R.dimen.dialog_corners), true);
        sharedEnter.setPathMotion(arcMotion);
        sharedEnter.setInterpolator(easeInOut);

        MorphTransition sharedReturn = new MorphTransition(ContextCompat.getColor(this, R.color.white),
                ContextCompat.getColor(this, R.color.colorAccent), getResources().getDimensionPixelSize(R.dimen.dialog_corners), 100, false);
        sharedReturn.setPathMotion(arcMotion);
        sharedReturn.setInterpolator(easeInOut);

        if (mContainer != null) {
            sharedEnter.addTarget(mContainer);
            sharedReturn.addTarget(mContainer);
        }
        getWindow().setSharedElementEnterTransition(sharedEnter);
        getWindow().setSharedElementReturnTransition(sharedReturn);
    }

    private boolean validateGroup(String groupName, String groupDescription) {
        if (TextUtils.isEmpty(groupName)) {
            mTitleTextView.setError(getString(R.string.creating_group_title_empty_error));
            return false;
        }
        mTitleTextView.setError(null);
        return true;
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void openAddMemberActivity(String groupId) {
        Intent intent = new Intent(this, AddMemberActivity.class);
        intent.putExtra(AddMemberActivity.EXTRA_GROUP_ID,groupId);
        intent.putExtra(AddMemberActivity.EXTRA_IS_NEWLY_CREATED,true);
        startActivity(intent);
        finish();
    }

    @Override
    public void dismiss() {
        supportFinishAfterTransition();
    }
}
