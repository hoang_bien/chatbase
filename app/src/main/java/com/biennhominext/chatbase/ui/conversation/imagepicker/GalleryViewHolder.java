package com.biennhominext.chatbase.ui.conversation.imagepicker;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.ui.custom.ScalingBlurPostprocessor;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.io.File;

/**
 * Created by hbien on 8/14/2017.
 */

public class GalleryViewHolder extends RecyclerView.ViewHolder {
    SimpleDraweeView mImageView;
     ImageButton mSendBtn;

    public GalleryViewHolder(View itemView) {
        super(itemView);
        mImageView = itemView.findViewById(R.id.image);
        mSendBtn = itemView.findViewById(R.id.btn_send);
    }

    public void bind(Cursor cursor, int selectedPosition) {
        String imagePath = cursor.getString(GalleryItem.INDEX_DATA_PATH);
        ImageRequestBuilder builder = ImageRequestBuilder.newBuilderWithSource(Uri.fromFile(new File(imagePath)));
        if (selectedPosition == cursor.getPosition()) {
            builder.setPostprocessor(new ScalingBlurPostprocessor());
            mSendBtn.setVisibility(View.VISIBLE);
        }else {
            mSendBtn.setVisibility(View.GONE);
        }
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(mImageView.getController())
                .setImageRequest(builder.build())
                .build();
        mImageView.setController(controller);

    }

}
