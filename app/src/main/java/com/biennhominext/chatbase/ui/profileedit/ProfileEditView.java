package com.biennhominext.chatbase.ui.profileedit;

import android.net.Uri;

import com.biennhominext.chatbase.ui.base.BaseView;

import java.io.File;

/**
 * Created by bien on 7/17/2017.
 */

public interface ProfileEditView extends BaseView{
    void toMainActivity();

    void setProfileImage(File file);
    void setProfileImage(String url);

    void setUserName(String userName);

    void setUserEmail(String email);

    void setUserGender(int gender);


    void closeActivity();


    File getCacheDirectory();

    void setProfileImage(Uri output);

    void showDialog();
}
