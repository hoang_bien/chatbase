package com.biennhominext.chatbase.ui.chat;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 7/19/2017.
 */

public class ChatPresenterImpl<V extends ChatView> extends BasePresenterImpl<V> implements ChatPresenter<V> {

    @Inject
    public ChatPresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dataManager, scheduler, compositeDisposable);
    }

    @Override
    public void onAttach(V view) {
        super.onAttach(view);
        getView().init(getDataManager().getCurrentUserId());
        getDisposable().add(getDataManager()
                .getMyChat()
                .subscribeOn(getScheduler().io())
                .observeOn(getScheduler().ui())
                .subscribe(getView()::addMessage));

    }
}
