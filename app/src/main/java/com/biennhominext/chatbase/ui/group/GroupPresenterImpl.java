package com.biennhominext.chatbase.ui.group;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by hbien on 7/28/2017.
 */

public class GroupPresenterImpl<V extends GroupView> extends BasePresenterImpl<V> implements GroupPresenter<V> {
    @Inject
    public GroupPresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dataManager, scheduler, compositeDisposable);

    }

    @Override
    public void onAttach(V view) {
        super.onAttach(view);
        getDisposable().add(getDataManager().getMyGroups()
                .subscribeOn(getScheduler().computation())
                .observeOn(getScheduler().ui())
                .subscribe(getView()::addGroup));
    }
}
