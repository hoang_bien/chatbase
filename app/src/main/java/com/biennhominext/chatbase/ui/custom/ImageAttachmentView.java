package com.biennhominext.chatbase.ui.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.biennhominext.chatbase.R;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by hbien on 8/23/2017.
 */

public class ImageAttachmentView extends LinearLayout {

    public ImageAttachmentView(Context context) {
        super(context);
        init();
    }

    public ImageAttachmentView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ImageAttachmentView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    void init(){
        setOrientation(VERTICAL);
    }

    public void setPaths(String[] paths){
        int padding = getResources().getDimensionPixelOffset(R.dimen.message_image_grid_padding);
        GenericDraweeHierarchy hierarchy = GenericDraweeHierarchyBuilder.newInstance(getResources())
                .setDesiredAspectRatio(1)
                .setPlaceholderImage(R.color.white)
                .build();
        LayoutParams params = new LayoutParams(300, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(padding,padding,padding,padding);
        for (int i = 0; i < paths.length; i++) {
            SimpleDraweeView imageView = new SimpleDraweeView(getContext());
            imageView.setLayoutParams(params);
            imageView.setBackgroundResource(R.color.colorAccent);
            imageView.setHierarchy(hierarchy);
            addView(imageView);
        }
    }
}
