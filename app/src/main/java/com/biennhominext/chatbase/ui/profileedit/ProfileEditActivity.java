package com.biennhominext.chatbase.ui.profileedit;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BaseActivity;
import com.biennhominext.chatbase.ui.custom.DarkenPostProcessor;
import com.biennhominext.chatbase.ui.main.MainActivity;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by bien on 7/17/2017.
 */

public class ProfileEditActivity extends BaseActivity implements ProfileEditView {
    @Inject
    ProfileEditPresenter<ProfileEditView> mPresenter;
    @BindView(R.id.iv_profile)
    SimpleDraweeView mProfileView;
    @BindView(R.id.edit_name)
    EditText mNameEditText;
    @BindView(R.id.edit_email)
    EditText mEmailEditText;
    //    @BindView(R.id.radio_male)
//    RadioButton mRadioMale;
//    @BindView(R.id.radio_female)
//    RadioButton mRadioFemale;
    private Uri mCurrentImageUri = null;
    private DarkenPostProcessor mPostProcessor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setUnbinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        toolbar.setNavigationIcon(R.drawable.ic_close);
        EasyImage.configuration(this)
                .setImagesFolderName(getString(R.string.app_name))
                .setAllowMultiplePickInGallery(false);
        mProfileView.setOnClickListener(view -> {
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setFixAspectRatio(true)
                    .start(this);
        });
        mPostProcessor = new DarkenPostProcessor(0.6f);
        mPresenter.onAttach(this);
        toolbar.setNavigationOnClickListener(view -> {
            finish();
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
//            Uri imageUri = CropImage.getPickImageResultUri(this, data);
//
//            // For API >= 23 we need to check specifically that we have permissions to read external storage,
//            // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
//            boolean requirePermissions = false;
//            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
//
//                // request permissions and handle the result in onRequestPermissionsResult()
//                requirePermissions = true;
//                mCurrentImageUri = imageUri;
//                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
//            } else {
//                setProfileImage(imageUri);
//            }
//        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                setProfileImage(result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                showSnackbar(error.getLocalizedMessage());
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCurrentImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setProfileImage(mCurrentImageUri);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            final String username = mNameEditText.getText().toString();
            final String email = mEmailEditText.getText().toString();
            // final int gender = mRadioMale.isChecked() ? User.GENDER_MALE : User.GENDER_FEMALE;
            mPresenter.updateUserInfo(username, email, User.GENDER_MALE, mCurrentImageUri);

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {

        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void toMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void setProfileImage(File file) {
        setProfileImage(Uri.fromFile(file));
    }

    @Override
    public void setProfileImage(String url) {
        setProfileImage(Uri.parse(url));
    }

    public void setProfileImage(Uri uri) {
        mCurrentImageUri = uri;
        final ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithSource(uri)
                .setPostprocessor(mPostProcessor)
                .build();

        final DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                .setOldController(mProfileView.getController())
                .setImageRequest(imageRequest)
                .build();

        mProfileView.setController(draweeController);
    }

    @Override
    public void showDialog() {
        new ProgressDialog.Builder(this)
                .setTitle(R.string.profile_edit_saving_title)
                .setCancelable(false)
                .show();
    }

    @Override
    public void setUserName(String userName) {
        mNameEditText.setText(userName);
    }

    @Override
    public void setUserEmail(String email) {
        mEmailEditText.setText(email);
    }

    @Override
    public void setUserGender(int gender) {
//        if (gender == User.GENDER_MALE) {
//            mRadioMale.setChecked(true);
//        } else if (gender == User.GENDER_FEMALE) {
//            mRadioFemale.setChecked(false);
//        }
    }

    @Override
    public void closeActivity() {
        finish();
    }

    @Override
    public File getCacheDirectory() {
        return getCacheDir();
    }


}
