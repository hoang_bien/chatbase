package com.biennhominext.chatbase.ui.conversation;

import android.view.View;

/**
 * Created by hbien on 8/9/2017.
 */

public interface OnImageItemClickListener {
    void onImageItemClick(String path, View view);
}
