package com.biennhominext.chatbase.ui.chat;

import com.biennhominext.chatbase.ui.base.BasePresenter;

/**
 * Created by bien on 7/19/2017.
 */

public interface ChatPresenter<V extends ChatView> extends BasePresenter<V> {

}
