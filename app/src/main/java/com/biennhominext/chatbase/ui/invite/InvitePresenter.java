package com.biennhominext.chatbase.ui.invite;

import com.biennhominext.chatbase.ui.base.BasePresenter;

/**
 * Created by hbien on 8/10/2017.
 */

public interface InvitePresenter<V extends InviteView> extends BasePresenter<V> {

    void ignoreInvitation(String userId);

    void acceptInvitation(String userId);
}
