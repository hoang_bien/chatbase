package com.biennhominext.chatbase.ui.findcontact;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BasePresenterImpl;
import com.biennhominext.chatbase.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by hbien on 8/2/2017.
 */

public class FindContactPresenterImpl<V extends FindContactView> extends BasePresenterImpl<V> implements FindContactPresenter<V> {
    @Inject
    public FindContactPresenterImpl(DataManager dataManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dataManager, scheduler, compositeDisposable);

    }

    @Override
    public void onAttach(V view) {
        super.onAttach(view);

    }

    @Override
    public void addOrRemoveFriend(User user) {
        getDataManager().toggleFriendship(user);
    }

    @Override
    public void findUserByName(String query) {
        getDisposable().clear();
        getDisposable().add(getDataManager().findUserByName(query)
                .subscribeOn(getScheduler().computation())
                .observeOn(getScheduler().ui())
                .subscribe(getView()::displayUser));
    }
}
