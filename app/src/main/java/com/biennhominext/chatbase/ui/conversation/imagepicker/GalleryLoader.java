package com.biennhominext.chatbase.ui.conversation.imagepicker;

import android.content.Context;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;

import com.biennhominext.chatbase.data.model.Message;

/**
 * Created by hbien on 8/14/2017.
 */

public class GalleryLoader extends CursorLoader {
    public static final String MEDIA_SCANNER_VOLUME_EXTERNAL = "external";
    private static final Uri STORAGE_URI = MediaStore.Files.getContentUri(MEDIA_SCANNER_VOLUME_EXTERNAL);
    private static final String SORT_ORDER = MediaStore.Images.Media.DATE_MODIFIED + " DESC";
    public static final String SELECTION = MediaStore.Images.Media.MIME_TYPE + " IN ('" + Message.ACCEPTABLE_IMAGE_TYPES[0]
            + "','" + Message.ACCEPTABLE_IMAGE_TYPES[1] + "','" + Message.ACCEPTABLE_IMAGE_TYPES[2] + "') AND "
            + MediaStore.Files.FileColumns.MEDIA_TYPE + " IN (" + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE + ")";

    public GalleryLoader(Context context) {
        super(context, STORAGE_URI, GalleryItem.IMAGE_PROJECTION, SELECTION, null, SORT_ORDER);
    }


}