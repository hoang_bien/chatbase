package com.biennhominext.chatbase.ui.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.di.ActivityContext;
import com.biennhominext.chatbase.utils.UiUtils;

import javax.inject.Inject;

/**
 * Created by bien on 7/19/2017.
 */

public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private Rect mBounds;
    private ColorDrawable mDivider;
    private Context mContext;

    @Inject
    public DividerItemDecoration(@ActivityContext Context context) {
        mContext = context;
        mBounds = new Rect();
        mDivider = new ColorDrawable(ContextCompat.getColor(context,R.color.blue_grey_200));
        mDivider.setAlpha(30);

    }

    @Override
    public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        canvas.save();
        final int leftWithMargin = UiUtils.dpToPx(mContext.getResources(),80);
        final int right = parent.getWidth();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            parent.getDecoratedBoundsWithMargins(child, mBounds);
            final int bottom = mBounds.bottom + Math.round(child.getTranslationY());
            final int top = bottom - 4;
            mDivider.setBounds(leftWithMargin, top, right, bottom);
            mDivider.draw(canvas);
        }
        canvas.restore();
    }
}
