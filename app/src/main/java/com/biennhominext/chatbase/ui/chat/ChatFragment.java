package com.biennhominext.chatbase.ui.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.di.component.ActivityComponent;
import com.biennhominext.chatbase.ui.base.BaseFragment;
import com.biennhominext.chatbase.ui.conversation.ConversationActivity;
import com.biennhominext.chatbase.utils.UiUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bien on 7/19/2017.
 */

public class ChatFragment extends BaseFragment implements ChatView, ChatAdapter.OnItemClickListener {
    @BindView(R.id.recycler_chat)
    RecyclerView mChatList;
    @Inject
    ChatPresenter<ChatView> mPresenter;
    @Inject
    RecyclerView.ItemDecoration mDecoration;

    @BindView(R.id.viewgroup_empty)
    View mEmptyView;

    private ChatAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        ActivityComponent activityComponent = getActivityComponent();
        if (activityComponent != null) {
            activityComponent.inject(this);
            setUnBinder(ButterKnife.bind(this, view));

        }

        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onAttach(this);
    }

    @Override
    public void onStop() {
        mPresenter.onDetach();
        super.onStop();
    }

    @Override
    public void updateChat(List<Message> msgList) {
        //Unused
    }

    @Override
    public void addMessage(Message msg) {
        UiUtils.hideViewIfVisible(mEmptyView);
        mAdapter.addMessage(msg);
    }

    @Override
    public void init(String currentUserId) {
        UiUtils.showIfInvisible(mEmptyView);
        mAdapter = new ChatAdapter(currentUserId);
        mAdapter.setListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mChatList.setLayoutManager(layoutManager);
        mChatList.setAdapter(mAdapter);
        mChatList.addItemDecoration(mDecoration);
    }



    @Override
    public void onItemClick(Message msg) {
        Intent intent = new Intent(getActivity(), ConversationActivity.class);
        if (msg.getPayload() instanceof User) {
            intent.putExtra(ConversationActivity.EXTRA_ID, ((User) msg.getPayload()).getUid());
            intent.putExtra(ConversationActivity.EXTRA_IS_GROUP, false);
            getActivity().startActivity(intent);
        } else if (msg.getPayload() instanceof Group) {
            intent.putExtra(ConversationActivity.EXTRA_ID, ((Group) msg.getPayload()).getUid());
            intent.putExtra(ConversationActivity.EXTRA_IS_GROUP, true);
            getActivity().startActivity(intent);
        }
    }

}
