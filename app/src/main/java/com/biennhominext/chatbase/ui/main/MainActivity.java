package com.biennhominext.chatbase.ui.main;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.ui.base.BaseActivity;
import com.biennhominext.chatbase.ui.chat.ChatFragment;
import com.biennhominext.chatbase.ui.contact.ContactFragment;
import com.biennhominext.chatbase.ui.custom.BadgeDrawable;
import com.biennhominext.chatbase.ui.findcontact.FindContactActivity;
import com.biennhominext.chatbase.ui.group.GroupFragment;
import com.biennhominext.chatbase.ui.group.creating.CreateActivityGroup;
import com.biennhominext.chatbase.ui.invite.InviteActivity;
import com.biennhominext.chatbase.ui.login.LoginActivity;
import com.biennhominext.chatbase.ui.profile.ProfileActivity;
import com.biennhominext.chatbase.ui.profileedit.ProfileEditActivity;
import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainView, View.OnClickListener {
    private static int page;
    @BindView(R.id.tabs)
    TabLayout mTabs;
    @BindView(R.id.pager)
    ViewPager mPager;
    @BindView(R.id.btn_new_message)
    FloatingActionButton mNewMessageBtn;
    @BindView(R.id.btn_new_group)
    FloatingActionButton mNewGroupBtn;
    View mDimView;
    @Inject
    MainPresenter<MainView> mPresenter;
    private LayerDrawable mInvitesIcon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setUnbinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        mPresenter.onPreparedAccount();

        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ChatFragment());
        adapter.addFragment(new ContactFragment());
        adapter.addFragment(new GroupFragment());
        mPager.setAdapter(adapter);
        mTabs.setupWithViewPager(mPager);
        mTabs.getTabAt(0).setIcon(R.drawable.ic_time);
        mTabs.getTabAt(1).setIcon(R.drawable.ic_contact);
        mTabs.getTabAt(2).setIcon(R.drawable.ic_group);

        mPager.setCurrentItem(page);

        createNotificationChannel();
        mNewGroupBtn.setOnClickListener(this);
        mNewMessageBtn.setOnClickListener(this);

    }


    public void setBadgeCount(Context context, LayerDrawable icon, String count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem itemInvites = menu.findItem(R.id.action_invites);
        mInvitesIcon = (LayerDrawable) itemInvites.getIcon();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_invites:
                Intent inviteIntent = new Intent(this, InviteActivity.class);
                startActivity(inviteIntent);
                break;
            case R.id.action_account:
                final Intent profileIntent = new Intent(this, ProfileActivity.class);
                startActivity(profileIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_DEFAULT));
        }
    }

    @Override
    public void openLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void openProfileEditActivity() {
        startActivity(new Intent(this, ProfileEditActivity.class));
        finish();
    }

    @Override
    public void updateUserAccount(User user) {
        //TODO: update ui
    }

    @Override
    public void setInviteBadgeCount(long count) {
        if (mInvitesIcon != null) {
            setBadgeCount(this, mInvitesIcon, String.valueOf(count));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_new_group:
                Intent intent = new Intent(this, CreateActivityGroup.class);
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, getString(R.string.transition_dialog));
                startActivity(intent, options.toBundle());
                break;
            case R.id.btn_new_message:
                Intent newMessageIntent = new Intent(this, FindContactActivity.class);
                startActivity(newMessageIntent);
                break;
            default:
                break;

        }
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> mFragments = new ArrayList<>();

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment) {
            mFragments.add(fragment);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
//            Drawable image = context.getResources().getDrawable(imageResId[position]);
//            image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
//            // Replace blank spaces with image icon
//            SpannableString sb = new SpannableString("   " + tabTitles[position]);
//            ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
//            sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }
}
