package com.biennhominext.chatbase.ui.base;

/**
 * Created by bien on 7/4/2017.
 */

public interface BasePresenter<T extends BaseView> {
    void onAttach(T view);
    void onDetach();
}
