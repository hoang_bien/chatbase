package com.biennhominext.chatbase.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.text.TextUtils;
import android.util.Log;

import com.biennhominext.chatbase.ChatApp;
import com.biennhominext.chatbase.R;
import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.di.component.DaggerServiceComponent;
import com.biennhominext.chatbase.di.component.ServiceComponent;
import com.biennhominext.chatbase.ui.conversation.ConversationActivity;
import com.biennhominext.chatbase.ui.invite.InviteActivity;
import com.facebook.common.executors.UiThreadImmediateExecutorService;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.annotations.Nullable;
import timber.log.Timber;

/**
 * Created by hbien on 8/2/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static String REPLY_ACTION = "com.biennhominext.notification.directreply.REPLY_ACTION";
    private static String KEY_REPLY = "key_reply_message";

    private static final String TAG = "MyFirebaseMsgService";
    public static final String MSG_CONTENT = "content";
    public static final String MSG_TIMESTAMP = "timestamp";
    public static final String MSG_SENDER_ID = "sender";
    public static final String MSG_SENDER_PROFILE_IMAGE = "senderProfileImage";
    public static final String MSG_SENDER_NAME = "senderName";
    private static final String MSG_UID = "uid";
    private static final String MSG_NAME = "name";
    private static final String MSG_NOTIFICATION_TYPE = "notificationType";
    private static final String MSG_NOTIFICATION_ID = "notificationId";

    private static final String NOTIFICATION_TYPE_USER_MESSAGE = "user_message";
    private static final String NOTIFICATION_TYPE_GROUP_MESSAGE = "group_message";
    private static final String NOTIFICATION_TYPE_INVITE_MESSAGE = "invite_message";
    private static final String MSG_MESSAGE_TYPE = "messageType";
    @Inject
    DataManager mDataManager;

    @Override
    public void onCreate() {
        super.onCreate();
        ServiceComponent component = DaggerServiceComponent.builder()
                .applicationComponent(((ChatApp) getApplication()).getApplicationComponent())
                .build();
        component.inject(this);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ


        // Check if message contains a notification payload.
        if (remoteMessage.getData() != null && mDataManager.isUserLogined()) {
            Log.d(TAG, "Message Body: " + remoteMessage.getData());
            sendNotification(remoteMessage);
        }

    }

    private void sendNotification(RemoteMessage message) {
        Map<String, String> data = message.getData();
        final String notificationType = data.get(MSG_NOTIFICATION_TYPE);
        if (!notificationType.equals(NOTIFICATION_TYPE_INVITE_MESSAGE) && mDataManager.isConversationVisible(data.get(MSG_NOTIFICATION_ID))) {
            Timber.i(data.get(MSG_NOTIFICATION_ID) + " is opening. Ignore notification");
            return;
        }

        if (notificationType.equals(NOTIFICATION_TYPE_GROUP_MESSAGE)) {
            buildNotification(message.getData(), null, notificationType);
        } else {
            String bitmapUrl = message.getData().get(MSG_SENDER_PROFILE_IMAGE);
            if (TextUtils.isEmpty(bitmapUrl)) {
                buildNotification(message.getData(), null, notificationType);
            } else {
                ImageRequest imageRequest = ImageRequestBuilder
                        .newBuilderWithSource(Uri.parse(bitmapUrl))
                        .build();

                ImagePipeline imagePipeline = Fresco.getImagePipeline();
                DataSource<CloseableReference<CloseableImage>> dataSource =
                        imagePipeline.fetchDecodedImage(imageRequest, this);

                dataSource.subscribe(new BaseBitmapDataSubscriber() {
                    @Override
                    public void onNewResultImpl(@Nullable Bitmap bitmap) {
                        // You can use the bitmap in only limited ways
                        // No need to do any cleanup.
                        buildNotification(message.getData(), bitmap, notificationType);
                    }

                    @Override
                    public void onFailureImpl(DataSource dataSource) {
                        // No cleanup required here.
                    }

                }, UiThreadImmediateExecutorService.getInstance());
            }
        }


    }

    private void buildNotification(Map<String, String> data, @Nullable Bitmap largeIcon, String notficationType) {
        if (notficationType.equals(NOTIFICATION_TYPE_INVITE_MESSAGE)) {
            Intent intent = new Intent(this, InviteActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                    .setSmallIcon(android.R.drawable.stat_notify_chat)
                    .setContentTitle(data.get(MSG_NAME))
                    .setContentText(getString(R.string.invites_new_request_notification))
                    .setColor(getColor(R.color.colorAccent))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            if (largeIcon != null) {
                notificationBuilder.setLargeIcon(largeIcon);
            }
            final int notificationId = data.get(MSG_NOTIFICATION_ID).hashCode();
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(notificationId/* ID of notification */, notificationBuilder.build());

        } else {

            boolean isGroup = notficationType.equals(NOTIFICATION_TYPE_GROUP_MESSAGE);
            final String notificationId = data.get(MSG_NOTIFICATION_ID);

            String content = null;
            final String messageType = data.get(MSG_MESSAGE_TYPE);
            final String senderUid = data.get(MSG_SENDER_ID);
            final String userName = mDataManager.getCurrentUserId().equals(senderUid) ?
                    getString(R.string.conversation_self_label) : data.get(MSG_SENDER_NAME);
            switch (messageType) {
                case Message.TYPE_MEDIA_IMAGE:
                    content = getString(R.string.format_image_meessage_description, userName);
                    break;
                case Message.TYPE_STICKER:
                    content = getString(R.string.format_sticker_message_description, userName);
                    break;
                case Message.TYPE_LOCATION:
                    content = getString(R.string.format_location_message_description);
                    break;
                case Message.TYPE_ACTION_CREATE_GROUP:
                    content = getString(R.string.format_message_create_group, userName);
                    break;
                case Message.TYPE_ACTION_ADD_MEMBER:
                    content = getString(R.string.format_message_add_member_description, userName);
                    break;
                case Message.TYPE_ACTION_LEAVE_GROUP:
                    content = getString(R.string.format_message_leave_group, userName);
                    break;
                case Message.TYPE_PLAIN_TEXT:
                    if (!isGroup) {
                        content = data.get(MSG_CONTENT);
                    } else {
                        content = userName + ": " + data.get(MSG_CONTENT);
                    }
                    break;
                default:
                    break;
            }


            //.setContentIntent(pendingIntent);


            final int notificationIdHash = data.get(MSG_NOTIFICATION_ID).hashCode();

            Intent intent = new Intent(this, ConversationActivity.class);
            intent.putExtra(ConversationActivity.EXTRA_ID, notificationId);
            intent.putExtra(ConversationActivity.EXTRA_IS_GROUP, isGroup);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // start your activity
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent contentPendingIntent = PendingIntent.getActivity(this, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);


            String replyLabel = getString(R.string.notif_action_reply);
            RemoteInput remoteInput = new RemoteInput.Builder(KEY_REPLY)
                    .setLabel(replyLabel)
                    .build();
            PendingIntent replyIntent = null;
            // 2. Build action
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // start a
                // (i)  broadcast receiver which runs on the UI thread or
                // (ii) service for a background task to b executed , but for the purpose of this codelab, will be doing a broadcast receiver
                intent = NotificationBroadcastReceiver.getReplyMessageIntent(this, notificationIdHash, notificationId, isGroup);
                replyIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
            } else {
                replyIntent = contentPendingIntent;
            }

            NotificationCompat.Action replyAction = new NotificationCompat.Action.Builder(
                    R.drawable.ic_send, replyLabel, replyIntent)
                    .addRemoteInput(remoteInput)
                    .setAllowGeneratedReplies(true)
                    .build();


            //Build notification
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                    .setSmallIcon(android.R.drawable.stat_notify_chat)
                    .setContentTitle(data.get(MSG_NAME))
                    .setContentText(content)
                    .setAutoCancel(true)
                    .setContentIntent(contentPendingIntent)
                    .addAction(replyAction)
                    .setColor(getColor(R.color.colorAccent))
                    .setSound(defaultSoundUri);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (largeIcon != null) {
                notificationBuilder.setLargeIcon(largeIcon);
            }

            notificationManager.notify(notificationIdHash/* ID of notification */, notificationBuilder.build());
        }
    }


    // [END receive_message]


    public static CharSequence getReplyMessage(Intent intent) {
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            return remoteInput.getCharSequence(KEY_REPLY);
        }
        return null;
    }

    private void scheduleJob() {
        // [START dispatch_job]
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(MyJobService.class)
//                .setTag("my-job-tag")
//                .build();
//        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }


}
