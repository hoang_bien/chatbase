package com.biennhominext.chatbase.fcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;

import com.biennhominext.chatbase.ChatApp;
import com.biennhominext.chatbase.data.DataManager;

import javax.inject.Inject;

/**
 * Created by hbien on 8/21/2017.
 */

public class NotificationBroadcastReceiver extends BroadcastReceiver {
    private static final String KEY_CONVERSATION_ID = "key_conversation_id";
    private static final String KEY_IS_GROUP = "key_conversation_type";
    private static String KEY_NOTIFICATION_ID = "key_noticiation_id";
    @Inject
    DataManager mDataManager;

    public static Intent getReplyMessageIntent(Context context, int notificationId, String conversationId, boolean isGroup) {
        Intent intent = new Intent(context, NotificationBroadcastReceiver.class);
        intent.setAction(MyFirebaseMessagingService.REPLY_ACTION);
        intent.putExtra(KEY_NOTIFICATION_ID, notificationId);
        intent.putExtra(KEY_CONVERSATION_ID, conversationId);
        intent.putExtra(KEY_IS_GROUP, isGroup);
        return intent;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ((ChatApp) context.getApplicationContext().getApplicationContext())
                .getApplicationComponent().inject(this);
        if (MyFirebaseMessagingService.REPLY_ACTION.equals(intent.getAction())) {
            // do whatever you want with the message. Send to the server or add to the db.
            // for this tutorial, we'll just show it in a toast;
            String message = MyFirebaseMessagingService.getReplyMessage(intent).toString();

            // update notification
            int notifyId = intent.getIntExtra(KEY_NOTIFICATION_ID, 1);
            String conversationId = intent.getStringExtra(KEY_CONVERSATION_ID);
            boolean isGroup = intent.getBooleanExtra(KEY_IS_GROUP, false);

            if (isGroup) {
                mDataManager.sendPlainTextMessageToGroup(message, conversationId);
            } else {
                mDataManager.sendPlainTextMessageToUser(message, conversationId);
            }


            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.cancel(notifyId);
        }
    }
}
