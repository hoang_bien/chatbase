package com.biennhominext.chatbase.utils;

import android.net.Uri;

/**
 * Created by hbien on 8/18/2017.
 */

public class MapUtils {
    public static final String BASE_URL = "http://maps.google.com/maps/api/staticmap";
    public static Uri getStaticMapUri(String latlng,String zoom,int size){
        Uri uri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("center",latlng)
                .appendQueryParameter("zoom",zoom)
                .appendQueryParameter("size",size + "x" + size)
                .appendQueryParameter("markers","red|" + latlng)
                .build();
        return uri;
    }
    public static Uri getStaticMapUri(String latlng){
        return getStaticMapUri(latlng,"17",300);
    }
}
