package com.biennhominext.chatbase.utils;

import android.content.Context;
import android.text.format.DateUtils;

import com.biennhominext.chatbase.data.model.Message;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by bien on 7/24/2017.
 */

public class Dates {
    public static String getDateAndTime(Context context, long timestamp){
        return DateUtils.formatDateTime(context,timestamp,
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME |
                DateUtils.FORMAT_ABBREV_MONTH | DateUtils.FORMAT_NO_YEAR);
    }
    public static String getTime(Context context, long timestamp){
        return DateUtils.formatDateTime(context,timestamp,DateUtils.FORMAT_SHOW_TIME);
    }
    public static String getDate(Context context, long timestamp){
        return DateUtils.formatDateTime(context,timestamp,DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY);
    }

    public static boolean isSameDay(Message msg1, Message msg2) {
       Date date1 = new Date(msg1.getTimestamp());
       Date date2 = new Date(msg2.getTimestamp());
       Calendar calendar1 = Calendar.getInstance();
       Calendar calendar2 = Calendar.getInstance();
       calendar1.setTime(date1);
       calendar2.setTime(date2);
       return calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)
               && calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);
    }
}
