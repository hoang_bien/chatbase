package com.biennhominext.chatbase.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by hbien on 8/21/2017.
 */

public class OsUtils {
    public static boolean hasReadStoragePermission(Activity activity) {
        return ContextCompat.checkSelfPermission(activity,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermission(Activity activity, int requestCode, String permission) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{permission},
                    requestCode);
    }
}
