package com.biennhominext.chatbase.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import com.biennhominext.chatbase.data.firebase.FirebaseHelperImpl;
import com.biennhominext.chatbase.data.firebase.GlideFirebaseLoader;
import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;

import java.util.List;

/**
 * Created by hbien on 7/26/2017.
 */

public class FirebaseUtils {
    public static void loadFirebaseImage(Context context, String path, ImageView target){
        if (TextUtils.isEmpty(path)){
            return;
        }
        Glide.with(context)
                .using(new GlideFirebaseLoader())
                .load(FirebaseStorage.getInstance().getReference(path))
                .crossFade()
                .centerCrop()
                .into(target);
    }

    public static String compressImageMessage(List<String> paths){
        StringBuffer buffer = new StringBuffer();
        final int lastPos = paths.size() - 1;
        for (int i = 0; i < lastPos; i++) {
            buffer.append(paths.get(i)).append(FirebaseHelperImpl.IMG_SEPARATOR);
        }
        if (lastPos>= 0){
            buffer.append(paths.get(lastPos));
        }
        return buffer.toString();
    }
    public static String[] extractImageMessage(String path){
        return path.split(FirebaseHelperImpl.IMG_SEPARATOR);
    }
}
