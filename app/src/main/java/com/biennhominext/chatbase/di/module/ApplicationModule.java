package com.biennhominext.chatbase.di.module;

import android.app.Application;
import android.content.Context;

import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.data.DataMangerImpl;
import com.biennhominext.chatbase.data.firebase.FirebaseHelper;
import com.biennhominext.chatbase.data.firebase.FirebaseHelperImpl;
import com.biennhominext.chatbase.data.prefs.PreferenceHelper;
import com.biennhominext.chatbase.data.prefs.PreferenceHelperImpl;
import com.biennhominext.chatbase.di.ApplicationContext;
import com.biennhominext.chatbase.di.PreferenceInfo;
import com.biennhominext.chatbase.utils.AppConstants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.hwangjr.rxbus.RxBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bien on 7/6/2017.
 */
@Module
public class ApplicationModule {

    private Application mApplication;

    public ApplicationModule(Application application) {
        this.mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }


    @Provides
    @Singleton
    FirebaseDatabase provideFirebaseDatabase() {
        return FirebaseDatabase.getInstance();
    }

    @Provides
    @Singleton
    FirebaseAuth provideFirebaseAuth() {
        return FirebaseAuth.getInstance();
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    PreferenceHelper providePreferenceHelper(PreferenceHelperImpl preferenceHelper) {
        return preferenceHelper;
    }

    @Provides
    @Singleton
    FirebaseStorage provideFirebaseStorage() {
        return FirebaseStorage.getInstance();
    }

    @Provides
    @Singleton
    FirebaseMessaging provideFirebaseMessaging() {
        return FirebaseMessaging.getInstance();
    }


    @Provides
    @Singleton
    FirebaseHelper provideFirebaseHelper(FirebaseHelperImpl firebaseHelper) {
        return firebaseHelper;
    }


    @Provides
    @Singleton
    DataManager provideDataManager(DataMangerImpl dataManger) {
        return dataManger;
    }


    @Provides
    @Singleton
    RxBus provideRxBus() {
        return new RxBus();
    }


}
