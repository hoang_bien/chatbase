package com.biennhominext.chatbase.di.component;

import android.app.Application;
import android.content.Context;

import com.biennhominext.chatbase.ChatApp;
import com.biennhominext.chatbase.data.DataManager;
import com.biennhominext.chatbase.di.ApplicationContext;
import com.biennhominext.chatbase.di.module.ApplicationModule;
import com.biennhominext.chatbase.fcm.NotificationBroadcastReceiver;
import com.hwangjr.rxbus.RxBus;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by bien on 7/6/2017.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(ChatApp chatApp);
    void inject(NotificationBroadcastReceiver notificationBroadcastReceiver);

    @ApplicationContext
    Context context();


    Application application();

    DataManager dataManager();

    RxBus rxBus();

}
