package com.biennhominext.chatbase.di.component;

import com.biennhominext.chatbase.di.PerService;
import com.biennhominext.chatbase.di.module.ServiceModule;
import com.biennhominext.chatbase.fcm.MyFirebaseInstanceIDService;
import com.biennhominext.chatbase.fcm.MyFirebaseMessagingService;

import dagger.Component;

/**
 * Created by hbien on 8/2/2017.
 */

@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {


    void inject(MyFirebaseInstanceIDService myFirebaseInstanceIDService);

    void inject(MyFirebaseMessagingService myFirebaseMessagingService);
}
