package com.biennhominext.chatbase.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.biennhominext.chatbase.di.ActivityContext;
import com.biennhominext.chatbase.di.PerActivity;
import com.biennhominext.chatbase.ui.chat.ChatPresenter;
import com.biennhominext.chatbase.ui.chat.ChatPresenterImpl;
import com.biennhominext.chatbase.ui.chat.ChatView;
import com.biennhominext.chatbase.ui.contact.ContactPresenter;
import com.biennhominext.chatbase.ui.contact.ContactPresenterImpl;
import com.biennhominext.chatbase.ui.contact.ContactView;
import com.biennhominext.chatbase.ui.findcontact.ContactResultAdapter;
import com.biennhominext.chatbase.ui.findcontact.FindContactPresenter;
import com.biennhominext.chatbase.ui.findcontact.FindContactPresenterImpl;
import com.biennhominext.chatbase.ui.findcontact.FindContactView;
import com.biennhominext.chatbase.ui.conversation.ConversationPresenter;
import com.biennhominext.chatbase.ui.conversation.ConversationPresenterImpl;
import com.biennhominext.chatbase.ui.conversation.ConversationView;
import com.biennhominext.chatbase.ui.custom.DividerItemDecoration;
import com.biennhominext.chatbase.ui.group.GroupAdapter;
import com.biennhominext.chatbase.ui.group.GroupPresenter;
import com.biennhominext.chatbase.ui.group.GroupPresenterImpl;
import com.biennhominext.chatbase.ui.group.GroupView;
import com.biennhominext.chatbase.ui.group.addmember.AddMemberPresenter;
import com.biennhominext.chatbase.ui.group.addmember.AddMemberPresenterImpl;
import com.biennhominext.chatbase.ui.group.addmember.AddMemberView;
import com.biennhominext.chatbase.ui.group.addmember.MemberPickerAdapter;
import com.biennhominext.chatbase.ui.group.creating.CreateGroupPresenter;
import com.biennhominext.chatbase.ui.group.creating.CreateGroupPresenterImpl;
import com.biennhominext.chatbase.ui.group.creating.CreateGroupView;
import com.biennhominext.chatbase.ui.imageviewer.ImageViewerPresenter;
import com.biennhominext.chatbase.ui.imageviewer.ImageViewerPresenterImpl;
import com.biennhominext.chatbase.ui.imageviewer.ImageViewerView;
import com.biennhominext.chatbase.ui.invite.InvitePresenter;
import com.biennhominext.chatbase.ui.invite.InvitePresenterImpl;
import com.biennhominext.chatbase.ui.invite.InviteView;
import com.biennhominext.chatbase.ui.login.LoginPresenter;
import com.biennhominext.chatbase.ui.login.LoginPresenterImpl;
import com.biennhominext.chatbase.ui.login.LoginView;
import com.biennhominext.chatbase.ui.main.MainPresenter;
import com.biennhominext.chatbase.ui.main.MainPresenterImpl;
import com.biennhominext.chatbase.ui.main.MainView;
import com.biennhominext.chatbase.ui.profile.ProfilePresenter;
import com.biennhominext.chatbase.ui.profile.ProfilePresenterImpl;
import com.biennhominext.chatbase.ui.profile.ProfileView;
import com.biennhominext.chatbase.ui.profileedit.ProfileEditPresenter;
import com.biennhominext.chatbase.ui.profileedit.ProfileEditPresenterImpl;
import com.biennhominext.chatbase.ui.profileedit.ProfileEditView;
import com.biennhominext.chatbase.utils.SchedulerProvider;
import com.biennhominext.chatbase.utils.SchedulerProviderImpl;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 7/6/2017.
 */
@Module
public class ActivityModule {
    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    SchedulerProvider providerScheduler() {
        return new SchedulerProviderImpl();
    }

    @Provides
    @PerActivity
    LoginPresenter<LoginView> provideLoginPresenter(LoginPresenterImpl<LoginView> loginPresenterImpl) {
        return loginPresenterImpl;
    }

    @Provides
    @PerActivity
    ProfileEditPresenter<ProfileEditView> provideProfileEditPresenter(ProfileEditPresenterImpl<ProfileEditView> profileEditViewProfileEditPresenter) {
        return profileEditViewProfileEditPresenter;
    }

    @Provides
    @PerActivity
    MainPresenter<MainView> provideMainPresenter(MainPresenterImpl<MainView> mainPresenter) {
        return mainPresenter;
    }

    @Provides
    ChatPresenter<ChatView> provideChatPresenter(ChatPresenterImpl<ChatView> chatPresenter) {
        return chatPresenter;
    }

    @Provides
    ContactPresenter<ContactView> provideContactPresenter(ContactPresenterImpl<ContactView> contactPresenter) {
        return contactPresenter;
    }

    @Provides
    @PerActivity
    ProfilePresenter<ProfileView> provideProfilePresenter(ProfilePresenterImpl<ProfileView> profileViewProfilePresenter) {
        return profileViewProfilePresenter;
    }

    @Provides
    ConversationPresenter<ConversationView> provideConversationPresenter(ConversationPresenterImpl<ConversationView> conversationPresenter) {
        return conversationPresenter;
    }

    @Provides
    RecyclerView.ItemDecoration provideItemDecoration(DividerItemDecoration dividerItemDecoration) {
        return dividerItemDecoration;
    }

    @Provides
    @PerActivity
    CreateGroupPresenter<CreateGroupView> provideCreatingGroupPresenter(CreateGroupPresenterImpl<CreateGroupView> creatingGroupPresenter) {
        return creatingGroupPresenter;
    }

    @Provides
    GroupPresenter<GroupView> provideGroupPresenter(GroupPresenterImpl<GroupView> groupPresenter) {
        return groupPresenter;
    }

    @Provides
    @PerActivity
    AddMemberPresenter<AddMemberView> provideAddMemberPresenter(AddMemberPresenterImpl<AddMemberView> addMemberPresenter) {
        return addMemberPresenter;
    }

    @Provides
    @PerActivity
    FindContactPresenter<FindContactView> provideAddContactPresenter(FindContactPresenterImpl<FindContactView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ImageViewerPresenter<ImageViewerView> provideImageViewerPresenter(ImageViewerPresenterImpl<ImageViewerView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    InvitePresenter<InviteView> provideInvitePresenter(InvitePresenterImpl<InviteView> presenter) {
        return presenter;
    }

    @Provides
    ContactResultAdapter provideAddContactAdapter() {
        return new ContactResultAdapter();
    }

    @Provides
    MemberPickerAdapter provideMemberPickerAdapter() {
        return new MemberPickerAdapter();
    }

    @Provides
    GroupAdapter provideGroupAdapter() {
        return new GroupAdapter();
    }


}
