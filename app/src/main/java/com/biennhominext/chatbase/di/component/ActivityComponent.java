package com.biennhominext.chatbase.di.component;

import com.biennhominext.chatbase.di.PerActivity;
import com.biennhominext.chatbase.di.module.ActivityModule;
import com.biennhominext.chatbase.ui.chat.ChatFragment;
import com.biennhominext.chatbase.ui.contact.ContactFragment;
import com.biennhominext.chatbase.ui.conversation.imagepicker.ImagePickerSubFragment;
import com.biennhominext.chatbase.ui.findcontact.FindContactActivity;
import com.biennhominext.chatbase.ui.conversation.ConversationActivity;
import com.biennhominext.chatbase.ui.conversation.ConversationFragment;
import com.biennhominext.chatbase.ui.group.GroupFragment;
import com.biennhominext.chatbase.ui.group.addmember.AddMemberActivity;
import com.biennhominext.chatbase.ui.group.creating.CreateActivityGroup;
import com.biennhominext.chatbase.ui.imageviewer.ImageViewerActivity;
import com.biennhominext.chatbase.ui.invite.InviteActivity;
import com.biennhominext.chatbase.ui.login.LoginActivity;
import com.biennhominext.chatbase.ui.main.MainActivity;
import com.biennhominext.chatbase.ui.profile.ProfileActivity;
import com.biennhominext.chatbase.ui.profile.ProfileFragment;
import com.biennhominext.chatbase.ui.profileedit.ProfileEditActivity;

import dagger.Component;

/**
 * Created by bien on 7/6/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(LoginActivity loginActivity);
    void inject(ProfileEditActivity profileEditActivity);

    void inject(MainActivity mainActivity);

    void inject(ChatFragment chatFragment);

    void inject(ContactFragment contactFragment);

    void inject(ProfileFragment profileFragment);

    void inject(ConversationFragment conversationFragment);

    void inject(ConversationActivity conversationActivity);

    void inject(GroupFragment groupFragment);

    void inject(CreateActivityGroup createGroupActivity);

    void inject(AddMemberActivity addMemberActivity);

    void inject(FindContactActivity findContactActivity);

    void inject(ImageViewerActivity imageViewerActivity);

    void inject(InviteActivity inviteActivity);

    void inject(ImagePickerSubFragment imagePickerSubFragment);

    void inject(ProfileActivity profileActivity);
}
