package com.biennhominext.chatbase.di.module;

import android.app.Service;

import dagger.Module;

/**
 * Created by hbien on 8/2/2017.
 */
@Module
public class ServiceModule {

    private final Service mService;

    public ServiceModule(Service service) {
        mService = service;
    }

}
