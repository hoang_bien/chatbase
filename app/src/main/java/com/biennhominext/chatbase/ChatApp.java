package com.biennhominext.chatbase;

import android.support.multidex.MultiDexApplication;

import com.biennhominext.chatbase.di.component.ApplicationComponent;
import com.biennhominext.chatbase.di.component.DaggerApplicationComponent;
import com.biennhominext.chatbase.di.module.ApplicationModule;
import com.biennhominext.chatbase.ui.conversation.sticker.ShopActivity;
import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.util.ByteConstants;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;

import javax.inject.Inject;

import timber.log.Timber;
import vc908.stickerfactory.StickersManager;
import vc908.stickerfactory.billing.Prices;

/**
 * Created by bien on 7/6/2017.
 */

public class ChatApp extends MultiDexApplication {

    @Inject
    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        DiskCacheConfig diskCacheConfig = DiskCacheConfig.newBuilder(this)
                .setBaseDirectoryName("ChatBase")
                .setMaxCacheSize(100 * ByteConstants.MB)
                .setMaxCacheSizeOnLowDiskSpace(10 * ByteConstants.MB)
                .setMaxCacheSizeOnVeryLowDiskSpace(3 * ByteConstants.MB)
                .setVersion(1)
                .build();

        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setResizeAndRotateEnabledForNetwork(false)
                .setMainDiskCacheConfig(diskCacheConfig)
                .setDownsampleEnabled(true)
                .build();
        Fresco.initialize(this,config);
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        mApplicationComponent.inject(this);

        //Set sticker

        StickersManager.initialize("d3161c01e07d6d33c1709859eb82b70a", this);

        // Set custom shop class
        StickersManager.setShopClass(ShopActivity.class);
        // Set prices
        StickersManager.setPrices(new Prices()
                        .setPricePointB("$0.99", 9.99f)
                        .setPricePointC("$1.99", 19.99f)
                // sku used for inapp purchases
//                .setSkuB("pack_b")
//                .setSkuC("pack_c")
        );
        // licence key for inapp purchases
        StickersManager.setLicenseKey("YOUR LICENCE KEY");
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
