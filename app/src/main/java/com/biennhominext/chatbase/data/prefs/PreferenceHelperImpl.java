package com.biennhominext.chatbase.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.di.ApplicationContext;
import com.biennhominext.chatbase.di.PreferenceInfo;

import javax.inject.Inject;

/**
 * Created by bien on 7/17/2017.
 */

public class PreferenceHelperImpl implements PreferenceHelper {
    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";
    private static final String PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";
    private static final String PREF_KEY_CURRENT_USER_GENDER = "PREF_KEY_CURRENT_USER_GENDER";
    private static final String PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL";
    private static final String PREF_KEY_CURRENT_USER_PROFILE_IMAGE
            = "PREF_KEY_CURRENT_USER_PROFILE_IMAGE";
    private static final String PREF_KEY_CURRENT_CONVERSATION_ID = "PREF_KEY_CURRNET_CONVERSATION_ID";

    private final SharedPreferences mPrefs;

    @Inject
    public PreferenceHelperImpl(@ApplicationContext Context context, @PreferenceInfo String prefName) {
        this.mPrefs = context.getSharedPreferences(prefName,Context.MODE_PRIVATE);
    }

    @Override
    public String getCurrentUserId() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_ID,null);
    }

    @Override
    public void setCurrentUserId(String userId) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_ID,userId).apply();
    }

    @Override
    public String getCurrentUserName() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_NAME,null);
    }

    @Override
    public void setCurrentUserName(String name) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAME,name).apply();
    }

    @Override
    public void setCurrentProfileImage(String url) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PROFILE_IMAGE,url).apply();
    }

    @Override
    public String getCurrentProfileImage() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PROFILE_IMAGE,null);
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_EMAIL,email).apply();
    }


    @Override
    public void setCurrentUserGender(int gender) {
        mPrefs.edit().putInt(PREF_KEY_CURRENT_USER_GENDER,gender).apply();
    }

    @Override
    public int getCurrentUserGender() {
        return mPrefs.getInt(PREF_KEY_CURRENT_USER_GENDER, User.GENDER_UNDEFINED);
    }

    @Override
    public void clearUserData() {
        mPrefs.edit()
                .remove(PREF_KEY_CURRENT_USER_ID)
                .remove(PREF_KEY_CURRENT_USER_NAME)
                .remove(PREF_KEY_CURRENT_USER_GENDER)
                .remove(PREF_KEY_CURRENT_USER_PROFILE_IMAGE)
                .apply();
    }


    @Override
    public void setCurrentConversationId(String currentConversationId) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_CONVERSATION_ID,currentConversationId)
        .apply();
    }

    @Override
    public void clearCurrentConversationId() {
        mPrefs.edit()
                .remove(PREF_KEY_CURRENT_CONVERSATION_ID)
                .apply();
    }

    @Override
    public String getCurrentConversationId() {
        return mPrefs.getString(PREF_KEY_CURRENT_CONVERSATION_ID,"");
    }

    @Override
    public String getCurrentUSerEmail() {
         return mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL,null);
    }
}
