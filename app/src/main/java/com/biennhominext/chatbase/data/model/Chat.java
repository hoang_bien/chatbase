package com.biennhominext.chatbase.data.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by bien on 7/18/2017.
 */
@IgnoreExtraProperties
public class Chat extends BaseObject{
    private String title;
    private String lastMessage;
    private String messageType;
    private String timestamp;
    private String imageUrl;
    private String type;

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Chat)){
            return false;
        }
        return uid.equals(((Chat) obj).uid);
    }
}
