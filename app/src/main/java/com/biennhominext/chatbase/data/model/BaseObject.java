package com.biennhominext.chatbase.data.model;

/**
 * Created by hbien on 8/2/2017.
 */

public class BaseObject {
    protected String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
