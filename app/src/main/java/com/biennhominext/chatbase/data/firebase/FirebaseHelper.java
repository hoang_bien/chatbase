package com.biennhominext.chatbase.data.firebase;

import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.data.model.User;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by bien on 7/17/2017.
 */

public interface FirebaseHelper {


    Single<User> insertUser(User user);

    Single<String> uploadImage(String path);

    Single<List<String>> uploadImages(List<String> filePaths);

    Flowable<User> getUser(String userId);

    Flowable<User> getUserWithFriendStatus(String userId, String currentUserId);


    Flowable<Message> getChatForUser(String userId);

    Flowable<Message> getConversationForUser(String userId, String targetId);

    Flowable<User> getContactsForUser(String userId);

    void sendPlainTextMessageToUser(String msg, String sender, String destination);

    Message sendMultipleImagesMessage(List<String> filePaths, String sender, String destination, boolean isGroup);

    Message sendSingleImageMessage(String filePath, String sender, String destination, boolean isGroup);

    void sendMessageToUser(Message message, String sender, String destination);


    String createNewGroup(String currentUserId, String groupName, String groupDescription);


    Flowable<User> getPeopleNotInGroup(String currentUserId, String groupId);

    void addPeopleToGroup(List<String> memberIds, String groupId, String shouldSendNotification);

    Flowable<Group> getGroupsForUser(String userId);

    Flowable<Group> getGroupInfo(String groupId,String currentUserId);

    Flowable<Message> getGroupConversationForUser(String currentUserId, String groupId);

    void sendPlainTextMessageToGroup(String message, String currentUserId, String groupId);

    Single<String> getUserToken(String currentUserId);

    void setUserToken(String currentUserId, String userToken);

    Flowable<User> getAllUsers(String currentUserId);

    Flowable<User> findUserByName(String currentUserId, String name);

    void toggleFriendship(String currentUserId, String targetUserId, boolean isFriend);


    void markUserMessageAsRead(String currentUserId, String senderId, String msgId);

    Flowable<User> getUsersInGroup(String groupId);

    void initializeOnlinePresence(String currentUserId);


    Flowable<Message> getInvitesForUser(String currentUserId);

    void ignoreInvitation(String currentUserId, String userId);

    void acceptInvitation(String currentUserId, String userId);

    Single<Message> getInviteMessageFromUser(String currentUserId, String uid);

    void sendStickerMessage(String code, String currentUserId, String targetId, boolean isGroup);

    void markGroupMessageAsRead(String currentUserId, String groupId, String msgId);

    void sendLocationMessage(double latitude, double longitude,String currentUserId, String targetId, boolean isGroup);

    Flowable<Long> getInvitesCount(String userId);
    Flowable<String> getLastMessageStateForUser(String currentUserId, String messageId, String targetId, boolean isGroup);

    void leaveGroup(String currentUserId, String groupId);

    void removeToken(String currentUserId);
}
