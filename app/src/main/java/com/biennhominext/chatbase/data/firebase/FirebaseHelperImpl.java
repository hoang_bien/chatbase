package com.biennhominext.chatbase.data.firebase;

import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.biennhominext.chatbase.data.exception.UnInitializedProfileException;
import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.utils.BitmapUtils;
import com.biennhominext.chatbase.utils.FirebaseUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by bien on 7/17/2017.
 */

public class FirebaseHelperImpl implements FirebaseHelper {
    private static final String TAG = FirebaseHelperImpl.class.getSimpleName();
    public static final String IMG_SEPARATOR = ";";
    private static final String FIREBASE_GROUPS = "groups";
    private static final String FIREBASE_USER_CHATS = "userChats";
    private static final String FIREBASE_USERS = "users";
    private static final String FIREBASE_USER_NAME = "name";
    private static final String FIREBASE_USER_PROFILE_IMAGE = "profileImage";
    private static final String FIREBASE_USER_FRIENDS = "friends";
    private static final String FIREBASE_GROUP_MESSAGES = "groupMessages";
    private static final String FIREBASE_FCM = "fcm";
    private static final String FIREBASE_PRESENCE = "presence";
    private static final int IMAGE_MAX_DIMENSION = 1024;
    private static final String FIREBASE_INVITES = "invites";

    FirebaseDatabase mFirebase;
    FirebaseStorage mStorage;
    FirebaseMessaging mMessaging;

    @Inject
    public FirebaseHelperImpl(FirebaseDatabase firebaseDatabase, FirebaseStorage storage, FirebaseMessaging messaging) {
        mFirebase = firebaseDatabase;
        mStorage = storage;
        mMessaging = messaging;
        mFirebase.setPersistenceEnabled(true);
    }

    @Override
    public Single<User> insertUser(User user) {
        return Single.create(
                emitter -> {
                    mFirebase.getReference(FIREBASE_USERS).child(user.getUid()).updateChildren(user.toMap())
                            .addOnSuccessListener((r) -> {
                                emitter.onSuccess(user);
                            })
                            .addOnFailureListener(e -> {
                                emitter.onError(e);
                            });
                }
        );
    }

    @Override
    public Single<String> uploadImage(String imageFile) {
        return Single.create(e -> {
            if (TextUtils.isEmpty(imageFile)) {
                e.onSuccess("");
            }

            Bitmap bitmap = BitmapUtils.decodeSampledBitmapFromFile(imageFile, IMAGE_MAX_DIMENSION, IMAGE_MAX_DIMENSION);
            if (bitmap == null) {
                e.onError(new FileNotFoundException());
                return;
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();
            final String imageName = bitmap.hashCode() + System.currentTimeMillis() + ".jpg";
            mStorage.getReference(imageName).putBytes(data).addOnSuccessListener(
                    taskSnapshot -> {
                        e.onSuccess(taskSnapshot.getDownloadUrl().toString());
                    }
            ).addOnFailureListener(e1 -> {
                e.onError(e1);
            });
        });
    }

    @Override
    public Flowable<User> getUser(String userId) {
        return Flowable.create(emiter -> {
            mFirebase.getReference(FIREBASE_USERS).child(userId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final User user = dataSnapshot.getValue(User.class);
                    if (user == null) {
                        emiter.onError(new UnInitializedProfileException());
                    } else {
                        mFirebase.getReference(FIREBASE_PRESENCE).child(userId).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                user.setOnline(snapshot.exists());
                                emiter.onNext(user);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d(TAG, databaseError.getMessage());
                }
            });
        }, BackpressureStrategy.LATEST);
    }

    @Override
    public Flowable<User> getUserWithFriendStatus(String userId, String currentUserId) {
        return Flowable.create(emiter -> {
            mFirebase.getReference(FIREBASE_USERS).child(userId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final User user = dataSnapshot.getValue(User.class);
                    if (user == null) {
                        emiter.onError(new UnInitializedProfileException());
                    } else {
                        Integer friendStatus = dataSnapshot.child(FIREBASE_USER_FRIENDS).child(currentUserId).getValue(Integer.class);
                        user.setFriendStatus(friendStatus);
                        mFirebase.getReference(FIREBASE_PRESENCE).child(user.getUid()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                user.setOnline(snapshot.exists());
                                emiter.onNext(user);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d(TAG, databaseError.getMessage());
                }
            });
        }, BackpressureStrategy.LATEST);
    }

    @Override
    public Flowable<Message> getChatForUser(String userId) {
        return getLastMessageForUser(userId)
                .mergeWith(getLastGroupMessageForUser(userId)).flatMapSingle(message -> {
                    if (message.getPayload().getUid().equals(message.getSender())) {
                        message.setUser((User) message.getPayload());
                        return Single.just(message);
                    } else {
                        return Single.create(e ->
                                mFirebase.getReference(FIREBASE_USERS).child(message.getSender()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        final User user = dataSnapshot.getValue(User.class);
                                        message.setUser(user);
                                        e.onSuccess(message);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.d(TAG, databaseError.getMessage());
                                    }
                                }));
                    }
                });
    }


    @Override
    public Flowable<Message> getConversationForUser(String userId, String
            targetId) {
        return getUser(targetId).concatMap(user -> Flowable.create(e ->
                mFirebase.getReference(FIREBASE_USER_CHATS)
                        .child(userId).child(targetId)
                        .orderByChild("timestamp")
                        .addChildEventListener(new ChildEventAdapter() {
                                                   @Override
                                                   public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                                       Message msg = dataSnapshot.getValue(Message.class);
                                                       msg.setUser(user);
                                                       e.onNext(msg);
                                                   }

                                                   @Override
                                                   public void onCancelled(DatabaseError databaseError) {
                                                       Log.e(TAG, databaseError.getMessage());
                                                   }
                                               }
                        ), BackpressureStrategy.BUFFER));
    }

    @Override
    public Flowable<User> getContactsForUser(String userId) {
        return getContactIndexForUser(userId)
                .flatMap(this::getUser);
    }

    @Override
    public Single<List<String>> uploadImages(List<String> filePaths) {
        return Flowable.fromIterable(filePaths)
                .flatMapSingle(this::uploadImage)
                .toList();
    }

    @Override
    public void sendPlainTextMessageToUser(String msg, String sender, String
            destination) {
        final String msgId = UUID.randomUUID().toString();
        Message message = new Message.Builder()
                .setContent(msg)
                .setMessageType(Message.TYPE_PLAIN_TEXT)
                .setTimestamp(System.currentTimeMillis())
                .setSender(sender)
                .setUid(msgId)
                .createMessage();
        sendMessageToUser(message, sender, destination);

    }


    @Override
    public Message sendMultipleImagesMessage(List<String> filePaths, String sender, String
            destination, boolean isGroup) {
        final String msgId = UUID.randomUUID().toString();
        uploadImages(filePaths)
                .subscribeOn(Schedulers.computation())
                .subscribe(paths -> {
                    String content = FirebaseUtils.compressImageMessage(paths);
                    Message message = new Message.Builder()
                            .setContent(content)
                            .setMessageType(Message.TYPE_MEDIA_IMAGE)
                            .setTimestamp(System.currentTimeMillis())
                            .setSender(sender)
                            .setUid(msgId)
                            .createMessage();
                    if (!isGroup) {
                        sendMessageToUser(message, sender, destination);
                    } else {
                        sendMessageToGroup(message, sender, destination);
                    }
                });
        StringBuffer buffer = new StringBuffer();
        final int lastPost = filePaths.size() - 1;
        for (int i = 0; i < lastPost; i++) {
            final File file = new File(filePaths.get(i));
            buffer.append(Uri.fromFile(file).toString());
            buffer.append(IMG_SEPARATOR);
        }
        if (lastPost > 0) {
            final File file = new File(filePaths.get(lastPost));
            buffer.append(Uri.fromFile(file).toString());
        }
        final Message message = new Message.Builder()
                .setContent(buffer.toString())
                .setMessageType(Message.TYPE_MEDIA_IMAGE)
                .setTimestamp(System.currentTimeMillis())
                .setSender(sender)
                .setUid(msgId)
                .createMessage();
        message.setSent(false);
        return message;
    }

    @Override
    public Message sendSingleImageMessage(String filePath, String sender, String destination, boolean isGroup) {

        final String msgId = UUID.randomUUID().toString();
        uploadImage(filePath)
                .subscribeOn(Schedulers.computation())
                .subscribe(path -> {
                    Message message = new Message.Builder()
                            .setContent(path)
                            .setMessageType(Message.TYPE_MEDIA_IMAGE)
                            .setTimestamp(System.currentTimeMillis())
                            .setSender(sender)
                            .setUid(msgId)
                            .createMessage();
                    if (!isGroup) {
                        sendMessageToUser(message, sender, destination);
                    } else {
                        sendMessageToGroup(message, sender, destination);
                    }
                });

        Message tempMessage = new Message.Builder()
                .setContent(Uri.fromFile(new File(filePath)).toString())
                .setMessageType(Message.TYPE_MEDIA_IMAGE)
                .setTimestamp(System.currentTimeMillis())
                .setSender(sender)
                .setUid(msgId)
                .createMessage();
        tempMessage.setSent(false);
        return tempMessage;
    }

    @Override
    public void sendMessageToUser(Message message, String sender, String
            destination) {
        message.setUnread(false);
        mFirebase.getReference(FIREBASE_USER_CHATS).child(sender).child(destination)
                .child(message.getUid()).setValue(message);

        mFirebase.getReference(FIREBASE_USERS).child(sender)
                .child(FIREBASE_USER_FRIENDS)
                .child(destination).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Integer status = dataSnapshot.getValue(Integer.class);
                if (status == null) {
                    message.setUnread(true);
                    sendInvitation(message, sender, destination);
                } else if (status == User.FLAG_FRIEND) {
                    message.setUnread(true);
                    mFirebase.getReference(FIREBASE_USER_CHATS).child(destination).child(sender)
                            .child(message.getUid()).setValue(message);
                    sendNotificationMessageToUser(message, destination);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage());
            }
        });
    }

    public void sendMessageToGroup(Message msg, String currentUserId, String groupId) {
        final DatabaseReference messageRef = mFirebase.getReference(FIREBASE_GROUP_MESSAGES)
                .child(groupId)
                .child(msg.getUid());
        messageRef.setValue(msg);
        messageRef.child("readBy").child(currentUserId).setValue(true);

//        final DatabaseReference groupRef = mFirebase.getReference(FIREBASE_GROUP_MESSAGES).child(groupId);
//        groupRef.updateChildren(msg.toMetaDataForGroup());
//        groupRef.child("readBy").child(currentUserId).setValue(true);
    }

    private void sendInvitation(Message message, String sender, String destination) {
        DatabaseReference userRef = mFirebase.getReference(FIREBASE_USERS);
        userRef.child(sender).child(FIREBASE_USER_FRIENDS).child(destination).setValue(User.FLAG_FRIEND_SENT);
        userRef.child(destination).child(FIREBASE_USER_FRIENDS).child(sender).setValue(User.FLAG_FRIEND_INVITED);
        mFirebase.getReference(FIREBASE_INVITES).child(destination).child(sender).setValue(message);
    }

    private void sendNotificationMessageToUser(Message message, String destination) {
        //TODO: implement later
    }

    @Override
    public String createNewGroup(String currentUserId, String groupName, String
            groupDescription) {
        String groupId = mFirebase.getReference(FIREBASE_GROUPS).push().getKey();
        Group group = new Group.Builder()
                .setTitle(groupName)
                .setUid(groupId)
                .setDescription(groupDescription)
                .createGroup();

        mFirebase.getReference(FIREBASE_GROUPS)
                .child(groupId)
                .setValue(group);
        mFirebase.getReference(FIREBASE_USERS).child(currentUserId).child(FIREBASE_GROUPS)
                .child(groupId).setValue(true);

        DatabaseReference groupMessageRef = mFirebase.getReference(FIREBASE_GROUP_MESSAGES).child(groupId);
        Message message = new Message.Builder()
                .setUid(groupMessageRef.push().getKey())
                .setTimestamp(System.currentTimeMillis())
                .setMessageType(Message.TYPE_ACTION_CREATE_GROUP)
                .setSender(currentUserId)
                .createMessage();

        groupMessageRef.child(message.getUid()).setValue(message);

        return groupId;

    }

    @Override
    public Flowable<User> getPeopleNotInGroup(String currentUserId, String groupId) {
        return getPeopleIndexNotInGroup(currentUserId, groupId).flatMap(this::getUser);
    }

    @Override
    public void addPeopleToGroup(List<String> memberIds, String groupId, String currentUserId) {
        DatabaseReference userRef = mFirebase.getReference(FIREBASE_USERS);
        for (int i = 0; i < memberIds.size(); i++) {
            userRef.child(memberIds.get(i)).child("groups").child(groupId).setValue(true);

        }
        if (currentUserId != null) {
            for (String userId :
                    memberIds) {
                final String uid = mFirebase.getReference(FIREBASE_GROUP_MESSAGES).child(groupId).push().getKey();
                Message message = new Message.Builder()
                        .setUid(uid)
                        .setContent(userId)
                        .setTimestamp(System.currentTimeMillis())
                        .setSender(currentUserId)
                        .setMessageType(Message.TYPE_ACTION_ADD_MEMBER)
                        .createMessage();
                sendMessageToGroup(message, currentUserId, groupId);
            }
        }

    }

    @Override
    public Flowable<Group> getGroupsForUser(String userId) {
        return getGroupIndexForUser(userId).flatMap(groupId -> Flowable.create(
                emitter -> mFirebase.getReference(FIREBASE_GROUPS).child(groupId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Group group = dataSnapshot.getValue(Group.class);
                        if (group != null && group.getUsers() != null) {
                            emitter.onNext(group);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, databaseError.getMessage());
                    }
                }), BackpressureStrategy.BUFFER));
    }

    @Override
    public Flowable<Group> getGroupInfo(String groupId, String currentUserId) {
        return Flowable.create(e ->
                        mFirebase.getReference(FIREBASE_GROUPS).child(groupId).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Group group = dataSnapshot.getValue(Group.class);
                                if (group != null && group.getUsers() != null && group.getTimestamp() != 0) {
                                    boolean hasUnreadMessage = !dataSnapshot.child("readBy").child(currentUserId).exists();
                                    group.setHasUnreadMessage(hasUnreadMessage);
                                    e.onNext(group);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.d(TAG, databaseError.getMessage());
                            }
                        })
                , BackpressureStrategy.LATEST);
    }

    @Override
    public Flowable<Message> getGroupConversationForUser(String currentUserId, String groupId) {
        return getMessageForGroup(groupId).concatMap(message -> {
            if (currentUserId.equals(message.getSender())) {
                return Flowable.just(message);
            } else {
                return Flowable.create(e ->
                        mFirebase.getReference(FIREBASE_USERS).child(message.getSender()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                final User user = dataSnapshot.getValue(User.class);
                                message.setUser(user);
                                e.onNext(message);
                                e.onComplete();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.d(TAG, databaseError.getMessage());
                            }
                        }), BackpressureStrategy.LATEST);
            }
        }).concatMap(message -> {
            if (message.getMessageType().equals(Message.TYPE_ACTION_ADD_MEMBER)) {
                return Flowable.create(e -> mFirebase.getReference(FIREBASE_USERS).child(message.getContent())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                message.setPayload(dataSnapshot.getValue(User.class));
                                e.onNext(message);
                                e.onComplete();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        }), BackpressureStrategy.LATEST);
            } else {
                return Flowable.just(message);
            }
        });
    }


    @Override
    public void sendPlainTextMessageToGroup(String message, String
            currentUserId, String groupId) {
        DatabaseReference groupMessageRef = mFirebase.getReference(FIREBASE_GROUP_MESSAGES).child(groupId);
        String messageId = groupMessageRef.push().getKey();
        Message msg = new Message.Builder()
                .setContent(message)
                .setMessageType(Message.TYPE_PLAIN_TEXT)
                .setTimestamp(System.currentTimeMillis())
                .setSender(currentUserId)
                .setUid(messageId)
                .createMessage();

        sendMessageToGroup(msg, currentUserId, groupId);
    }

    @Override
    public Single<String> getUserToken(String currentUserId) {
        return Single.create(e -> {
            mFirebase.getReference(FIREBASE_FCM).child(currentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String val = dataSnapshot.getValue(String.class);
                    if (val == null) {
                        e.onSuccess("");
                    } else {
                        e.onSuccess(val);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d(TAG, databaseError.getMessage());
                }
            });
        });
    }

    @Override
    public void setUserToken(String currentUserId, String token) {
        mFirebase.getReference(FIREBASE_FCM).child(currentUserId).setValue(token);
    }

    @Override
    public Flowable<User> getAllUsers(String currentUserId) {
        return Flowable.create(e ->
                mFirebase.getReference(FIREBASE_USERS)
                        .addChildEventListener(new ChildEventAdapter() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                User user = dataSnapshot.getValue(User.class);
                                if (user.getUid().equals(currentUserId)) {
                                    return;
                                }
                                final Integer isFriend = dataSnapshot.child("friends").child(currentUserId).getValue(Integer.class);
                                user.setFriendStatus(isFriend);
                                e.onNext(user);
                            }
                        }), BackpressureStrategy.BUFFER);
    }

    @Override
    public Flowable<User> findUserByName(String currentUserId, String query) {
        String text = query.toLowerCase().trim();
        return Flowable.create(e ->
                mFirebase.getReference(FIREBASE_USERS)
                        /*.orderByChild("name")
                        .startAt(text)
                        .endAt(text + "\uf8ff")*/
                        .addChildEventListener(new ChildEventAdapter() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                User user = dataSnapshot.getValue(User.class);
                                if (!user.getName().toLowerCase().contains(text)) {
                                    return;
                                }
                                if (user.getUid().equals(currentUserId)) {
                                    return;
                                }
                                final Integer isFriend = dataSnapshot.child("friends").child(currentUserId).getValue(Integer.class);
                                user.setFriendStatus(isFriend);
                                e.onNext(user);
                            }
                        }), BackpressureStrategy.BUFFER);
    }

    @Override
    public void toggleFriendship(String currentUserId, String targetUserId,
                                 boolean isFriend) {
        DatabaseReference friendRef = mFirebase.getReference(FIREBASE_USERS).child(currentUserId).child(FIREBASE_USER_FRIENDS).child(targetUserId);
        DatabaseReference friendRef2 = mFirebase.getReference(FIREBASE_USERS).child(targetUserId).child(FIREBASE_USER_FRIENDS).child(currentUserId);
        if (isFriend) {
            // remove this friend
            friendRef.removeValue();
            friendRef2.removeValue();
        } else {
            // add this friend
            friendRef.setValue(true);
            friendRef2.setValue(true);
        }
    }

    @Override
    public void markUserMessageAsRead(String currentUserId, String senderId, String msgId) {
        mFirebase.getReference(FIREBASE_USER_CHATS).child(currentUserId).child(senderId).child(msgId)
                .child("unread").setValue(false);
    }

    @Override
    public Flowable<User> getUsersInGroup(String groupId) {
        return getMembersIndexInGroup(groupId)
                .flatMap(this::getUser);
    }

    @Override
    public void initializeOnlinePresence(String currentUserId) {
        final DatabaseReference lastOnlineRef = mFirebase.getReference(FIREBASE_USERS).child(currentUserId).child("lastOnline");
        final DatabaseReference myConnectionRef = mFirebase.getReference(FIREBASE_PRESENCE).child(currentUserId);

        final DatabaseReference connectedRef = mFirebase.getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    DatabaseReference con = myConnectionRef;

                    // when this device disconnects, remove it
                    con.onDisconnect().removeValue();
                    con.setValue(true);

                    // when I disconnect, update the last time I was seen online
                    lastOnlineRef.onDisconnect().setValue(ServerValue.TIMESTAMP);

                    // add this device to my connections list
                    // this value could contain info about the device or a timestamp too
                    con.setValue(Boolean.TRUE);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Listener was cancelled at .info/connected");
            }
        });
    }

    @Override
    public Flowable<Message> getInvitesForUser(String currentUserId) {
        return Flowable.create(e -> mFirebase.getReference(FIREBASE_INVITES).child(currentUserId)
                .orderByChild("unread").startAt(true).endAt(true)
                .addChildEventListener(new ChildEventAdapter() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Message message = dataSnapshot.getValue(Message.class);
                        mFirebase.getReference(FIREBASE_USERS).child(message.getSender())
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot userSnapshot) {
                                        final User user = userSnapshot.getValue(User.class);
                                        message.setUser(user);
                                        if (user != null) {
                                            e.onNext(message);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e(TAG, databaseError.getMessage());
                                    }
                                });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                }), BackpressureStrategy.BUFFER);
    }

    @Override
    public void ignoreInvitation(String currentUserId, String userId) {
        mFirebase.getReference(FIREBASE_INVITES).child(currentUserId).child(userId).child("unread").setValue(false);
    }

    @Override
    public void acceptInvitation(String currentUserId, String userId) {
        mFirebase.getReference(FIREBASE_USERS).child(currentUserId).child(FIREBASE_USER_FRIENDS).child(userId).setValue(User.FLAG_FRIEND);
        mFirebase.getReference(FIREBASE_USERS).child(userId).child(FIREBASE_USER_FRIENDS).child(currentUserId).setValue(User.FLAG_FRIEND);
        mFirebase.getReference(FIREBASE_INVITES).child(currentUserId).child(userId).removeValue();
    }

    @Override
    public Single<Message> getInviteMessageFromUser(String currentUserId, String uid) {

        return Single.create(e -> mFirebase.getReference(FIREBASE_INVITES).child(currentUserId).child(uid)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Message message = dataSnapshot.getValue(Message.class);
                        Log.e(TAG, "get null invite message from " + uid);
                        if (message == null) {
                            return;
                        }
                        mFirebase.getReference(FIREBASE_USERS).child(message.getSender())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot userSnapshot) {
                                        final User user = userSnapshot.getValue(User.class);
                                        message.setUser(user);
                                        e.onSuccess(message);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e(TAG, databaseError.getMessage());
                                    }
                                });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                }));
    }

    @Override
    public void sendStickerMessage(String code, String currentUserId, String targetId, boolean isGroup) {
        final String msgId = UUID.randomUUID().toString();
        Message message = new Message.Builder()
                .setContent(code)
                .setMessageType(Message.TYPE_STICKER)
                .setTimestamp(System.currentTimeMillis())
                .setSender(currentUserId)
                .setUid(msgId)
                .createMessage();
        if (isGroup) {
            sendMessageToGroup(message, currentUserId, targetId);
        } else {
            sendMessageToUser(message, currentUserId, targetId);
        }
    }

    @Override
    public void markGroupMessageAsRead(String currentUserId, String groupId, String msgId) {
        mFirebase.getReference(FIREBASE_GROUP_MESSAGES).child(groupId).child(msgId).child("readBy")
                .child(currentUserId).setValue(true);
        mFirebase.getReference(FIREBASE_GROUPS).child(groupId).child("readBy").child(currentUserId).setValue(true);
    }

    @Override
    public void sendLocationMessage(double latitude, double longitude, String currentUserId, String targetId, boolean isGroup) {
        sendMessage(latitude + "," + longitude, Message.TYPE_LOCATION, currentUserId, targetId, isGroup);
    }

    @Override
    public Flowable<Long> getInvitesCount(String userId) {
        return Flowable.create(e -> mFirebase.getReference(FIREBASE_INVITES).child(userId)
                .orderByChild("unread").startAt(true).endAt(true).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        e.onNext(dataSnapshot.getChildrenCount());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }), BackpressureStrategy.LATEST);
    }

    @Override
    public Flowable<String> getLastMessageStateForUser(String currentUserId, String messageId, String targetId, boolean isGroup) {
        if (isGroup) {
            return Flowable.create(e -> mFirebase.getReference(FIREBASE_GROUP_MESSAGES).child(targetId).child(messageId).child("readBy")
                    .addChildEventListener(new ChildEventAdapter() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            final String key = dataSnapshot.getKey();
                            if (!currentUserId.equals(key)) {
                                e.onNext(key);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Timber.e(databaseError.getMessage());
                        }
                    }), BackpressureStrategy.BUFFER)
                    .flatMap(key -> Flowable.create(e -> mFirebase.getReference(FIREBASE_USERS).child(key.toString()).child("profileImage")
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    e.onNext(dataSnapshot.getValue(String.class));
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Timber.e(databaseError.getMessage());
                                }
                            }), BackpressureStrategy.LATEST));
        } else {
            return Flowable.create(e -> mFirebase.getReference(FIREBASE_USER_CHATS).child(targetId).child(currentUserId).child(messageId).child("unread").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue(Boolean.class) == false) {
                        e.onNext(targetId);
                        e.onComplete();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Timber.e(databaseError.getMessage());
                }
            }), BackpressureStrategy.LATEST);
        }
    }

    @Override
    public void leaveGroup(String currentUserId, String groupId) {
        DatabaseReference userRef = mFirebase.getReference(FIREBASE_USERS);
        userRef.child(currentUserId).child("groups").child(groupId).removeValue();
        mFirebase.getReference(FIREBASE_GROUPS).child(groupId).child("users").child(currentUserId).removeValue();

        final String uid = mFirebase.getReference(FIREBASE_GROUP_MESSAGES).child(groupId).getKey();
        Message message = new Message.Builder()
                .setUid(uid)
                .setTimestamp(System.currentTimeMillis())
                .setSender(currentUserId)
                .setMessageType(Message.TYPE_ACTION_LEAVE_GROUP)
                .createMessage();
        sendMessageToGroup(message, currentUserId, groupId);

    }

    @Override
    public void removeToken(String currentUserId) {
        mFirebase.getReference(FIREBASE_FCM).child(currentUserId).removeValue();
    }

    private Flowable<String> getMembersIndexInGroup(String groupId) {
        return Flowable.create(emitter -> mFirebase.getReference(FIREBASE_GROUPS).child(groupId).child(FIREBASE_USERS).addChildEventListener(new ChildEventAdapter() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                emitter.onNext(dataSnapshot.getKey());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage());
            }
        }), BackpressureStrategy.BUFFER);
    }


    //    @Override
//    public void markUserMessageAsRead(String currentUserId, String targetId) {
//        mFirebase.getReference(FIREBASE_USER_CHATS).child(currentUserId)
//                .child(targetId)
//                .orderByChild("unread")
//                .startAt(true)
//                .endAt(true)
//        .addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (dataSnapshot.hasChildren()){
//                    for (DataSnapshot child :
//                            dataSnapshot.getChildren()) {
//                        child.getRef().child("unread").setValue(false);
//                    }
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Log.d(TAG,databaseError.getMessage());
//            }
//        });
//    }
    private Flowable<Message> getLastGroupMessageForUser(String userId) {
        return getLastGroupMessageIndexForUser(userId)
                .flatMap(groupId -> getGroupInfo(groupId, userId))
//                .flatMap(group -> Flowable.create(e ->
//                        mFirebase.getReference(FIREBASE_GROUP_MESSAGES).child(group.getUid())
//                                .child(group.getLastMessage()).addValueEventListener(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(DataSnapshot dataSnapshot) {
//                                Message message = dataSnapshot.getValue(Message.class);
//                                message.setPayload(group);
//                                if (dataSnapshot.child("readBy").child(userId).exists()) {
//                                    message.setUnread(false);
//                                } else {
//                                    message.setUnread(true);
//                                }
//
//                            }
//
//                            @Override
//                            public void onCancelled(DatabaseError databaseError) {
//
//                            }
//                        }), BackpressureStrategy.LATEST));
                .map(group -> {
                    Message message = new Message.Builder()
                            .setContent(group.getLastMessage())
                            .setMessageType(group.getMessageType())
                            .setSender(group.getSender())
                            .setTimestamp(group.getTimestamp())
                            .createMessage();
                    message.setPayload(group);
                    message.setUnread(group.isHasUnreadMessage());
                    message.setDestination(group.getUid());
                    return message;
                });
    }


    private Flowable<String> getLastGroupMessageIndexForUser(String userId) {
        return Flowable.create(e ->
                        mFirebase.getReference(FIREBASE_USERS).child(userId).child(FIREBASE_GROUPS)
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.hasChildren()) {
                                            for (DataSnapshot child :
                                                    dataSnapshot.getChildren()) {
                                                e.onNext(child.getKey());
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.d(TAG, databaseError.getMessage());
                                    }
                                })
                , BackpressureStrategy.BUFFER);
    }


    private Flowable<Message> getLastMessageForUser(String userId) {
        return Flowable.create(emitter ->
                mFirebase.getReference(FIREBASE_USER_CHATS).child(userId)
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.hasChildren()) {
                                    for (DataSnapshot child :
                                            dataSnapshot.getChildren()) {
                                        mFirebase.getReference(FIREBASE_USER_CHATS)
                                                .child(userId)
                                                .child(child.getKey())
                                                .orderByChild("timestamp")
                                                .limitToLast(1)
                                                .addChildEventListener(new ChildEventAdapter() {
                                                    @Override
                                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                                        Message message = dataSnapshot.getValue(Message.class);
                                                        message.setDestination(child.getKey());
                                                        mFirebase.getReference(FIREBASE_USERS)
                                                                .child(message.getDestination())
                                                                .addValueEventListener(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                                        message.setPayload(dataSnapshot.getValue(User.class));
                                                                        mFirebase.getReference(FIREBASE_PRESENCE).child(message.getPayload().getUid()).addValueEventListener(new ValueEventListener() {
                                                                            @Override
                                                                            public void onDataChange(DataSnapshot snapshot) {
                                                                                ((User) message.getPayload()).setOnline(snapshot.exists());
                                                                                emitter.onNext(message);
                                                                            }

                                                                            @Override
                                                                            public void onCancelled(DatabaseError databaseError) {

                                                                            }
                                                                        });
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(DatabaseError databaseError) {
                                                                        // e.onError(databaseError.toException().getCause());
                                                                    }
                                                                });
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {
                                                        Log.e(TAG, databaseError.getMessage());
                                                    }
                                                });

                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                // emitter.onError(databaseError.toException().getCause());
                                Log.e(TAG, databaseError.getMessage());
                            }
                        }), BackpressureStrategy.LATEST);
    }

    private Flowable<Message> getMessageForUser(String userId, String targetId) {
        return Flowable.create(emitter -> mFirebase.getReference(FIREBASE_USER_CHATS)
                .child(userId).child(targetId)
                .orderByChild("timestamp")

                .addChildEventListener(new ChildEventAdapter() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Message msg = dataSnapshot.getValue(Message.class);
                        emitter.onNext(msg);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        emitter.onError(databaseError.toException().getCause());
                    }
                }), BackpressureStrategy.BUFFER);
    }

    private Flowable<Message> getMessageForGroup(String groupId) {
        return Flowable.create(e -> mFirebase.getReference(FIREBASE_GROUP_MESSAGES)
                        .child(groupId)
                        .orderByChild("timestamp")
                        .addChildEventListener(new ChildEventAdapter() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                final Message message = dataSnapshot.getValue(Message.class);
                                e.onNext(message);
                            }
                        })
                , BackpressureStrategy.BUFFER);
    }

    private Flowable<String> getGroupIndexForUser(String userId) {
        return Flowable.create(emitter ->
                mFirebase.getReference(FIREBASE_USERS).child(userId)
                        .child("groups").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChildren()) {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                emitter.onNext(snapshot.getKey());
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, databaseError.getMessage());
                    }
                }), BackpressureStrategy.BUFFER);
    }


    private Flowable<String> getContactIndexForUser(String userId) {
        return Flowable.create(emitter ->
                mFirebase.getReference(FIREBASE_USERS).child(userId).child(FIREBASE_USER_FRIENDS)
                        .orderByValue().startAt(0).endAt(0)
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.hasChildren()) {
                                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                        emitter.onNext(snapshot.getKey());
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.d(TAG, databaseError.getMessage());
                            }
                        }), BackpressureStrategy.BUFFER);
    }

    private Flowable<String> getPeopleIndexNotInGroup(String currentUserId, String
            groupId) {
        return Flowable.create(emitter -> {
            mFirebase.getReference(FIREBASE_GROUPS).child(groupId).child(FIREBASE_USERS)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            List<String> memberIdList = new ArrayList<>();
                            if (dataSnapshot.hasChildren()) {
                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    memberIdList.add(child.getKey());

                                }
                            }
                            mFirebase.getReference(FIREBASE_USERS).child(currentUserId).child(FIREBASE_USER_FRIENDS)
                                    .orderByValue().startAt(0).endAt(0)
                                    .addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.hasChildren()) {
                                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                                    if (!memberIdList.contains(child.getKey())) {
                                                        emitter.onNext(child.getKey());
                                                    }
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            Log.d(TAG, databaseError.getMessage());
                                        }
                                    });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d(TAG, databaseError.getMessage());
                        }
                    });
        }, BackpressureStrategy.BUFFER);
    }

    private void sendMessage(String code, String messageType, String currentUserId, String targetId, boolean isGroup) {
        final String msgId = UUID.randomUUID().toString();
        Message message = new Message.Builder()
                .setContent(code)
                .setMessageType(messageType)
                .setTimestamp(System.currentTimeMillis())
                .setSender(currentUserId)
                .setUid(msgId)
                .createMessage();
        if (isGroup) {
            sendMessageToGroup(message, currentUserId, targetId);
        } else {
            sendMessageToUser(message, currentUserId, targetId);
        }
    }

}
