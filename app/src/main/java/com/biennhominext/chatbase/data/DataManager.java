package com.biennhominext.chatbase.data;

import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.data.model.User;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by bien on 7/17/2017.
 */

public interface DataManager {
    Single<User> updateUserInfo(User user);
    Flowable<User> getAndCacheUser(String userId);
    Single<String> uploadImage(String imageFile);
    boolean isUserLogined();
    Flowable<User> getAccountInfo();

    Completable doGoogleLoginCall(GoogleSignInAccount account);

    Completable doLoginWithEmailAndPassword(String email,String password);

    String getCurrentUserId();
    void setCurrentUserId(String userId);

    String getCurrentUserName();
    void setCurrentUserName(String name);

    void setCurrentProfileImage(String url);
    String getCurrentProfileImage();

    void setCurrentUserEmail(String email);
    String getCurrentUSerEmail();

    int getCurrentUserGender();
    void setCurrentUserGender(int gender);

    Flowable<Message> getMyChat();

    Flowable<User> getMyContacts();

    Flowable<User> getContactsNotInGroup(String groupId);

    Flowable<User> getPublicUserInfo(String targetId);

    Flowable<Message> getConversationForUser(String targetId);

    void sendPlainTextMessageToUser(String message, String receiver);

    Message sendMultipleImageMessage(List<String> paths, String destination, boolean isGroup);

    Message sendSingleImageMessage(String path, String destination, boolean isGroup);

    String createNewGroup(String groupName, String groupDescription);

    void logout();

    void addMembersToGroup(List<String> memberIds, String groupId, boolean shouldSendNotification);

    Flowable<Group> getMyGroups();

    Flowable<Group> getGroupInfo(String targetId);

    Flowable<Message> getGroupConversationForUser(String targetId);

    void sendPlainTextMessageToGroup(String message, String groupId);

    Single<String> getMyToken();
    void setMyToken(String token);

    Flowable<User> getAllUsers();

    void toggleFriendship(User user);
    Flowable<User> findUserByName(String query);


    void markUserMessageAsRead(String senderId, String msgId);

    Flowable<User> getUsersInGroup(String groupId);

    void initializeOnlinePresence();

    Flowable<Message> getMyInvites();

    void ignoreInvitation(String userId);

    void acceptInvitation(String userId);

    Single<Message> getInviteMessageFromUser(String uid);

    void sendStickerMessage(String code, String targetId, boolean isGroup);

    Flowable<Long> getCurrentInvitesCount();


    void setCurrentConversationId(String currentConversationId);
    void clearCurrentConversationId();

    boolean isConversationVisible(String conversationId);

    void markGroupMessageAsRead(String sender, String uid);

    void sendLocationMessage(double latitude, double longitude, String mTargetId, boolean mIsGroup);

    void leaveGroup(String groupId);

    Flowable<String> getLastMessageStatus(String messageId, String mTargetId, boolean mIsGroup);
}
