package com.biennhominext.chatbase.data.exception;

/**
 * Created by bien on 7/18/2017.
 */

public class UnInitializedProfileException extends Exception {
    public UnInitializedProfileException() {
        super("User's profile has not been created yet");
    }
}
