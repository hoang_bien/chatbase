package com.biennhominext.chatbase.data.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bien on 7/17/2017.
 */
public class Message {
    public static final String TYPE_PLAIN_TEXT = "text";
    public static final String TYPE_MEDIA_IMAGE = "image";
    public static final String TYPE_ACTION_CREATE_GROUP = "group/create";
    public static final String[] ACCEPTABLE_IMAGE_TYPES = new String[]{"image/jpg", "image/jpeg", "image/png"};
    public static final String TYPE_STICKER = "sticker";
    public static final String TYPE_LOCATION = "location";
    public static final String TYPE_ACTION_LEAVE_GROUP = "group/leave";
    public static final String TYPE_ACTION_ADD_MEMBER = "group/add_member";
    private String uid;
    private String sender;
    private String content;
    private long timestamp;
    private String messageType;
    private boolean unread;
    @Exclude
    private User user;
    @Exclude
    private String destination;
    @Exclude
    private BaseObject payload;
    @Exclude
    private boolean isSent = true;
    private List<String> readUsers;

    public Message(String uid, String sender, String content, long timestamp, String messageType) {
        this.uid = uid;
        this.sender = sender;
        this.content = content;
        this.timestamp = timestamp;
        this.messageType = messageType;
    }


    public Message() {

    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }


    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setPayload(BaseObject group) {
        this.payload = group;
    }

    public BaseObject getPayload() {
        return payload;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }


    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Message)) {
            return false;
        }
        return uid.equals(((Message) obj).uid);
    }

    public boolean isUnread() {
        return unread;
    }

    public void setUnread(boolean unread) {
        this.unread = unread;
    }

    public Map<String, Object> toMetaDataForGroup(){
        Map<String,Object> map = new HashMap<>();
        map.put("timestamp",timestamp);
        map.put("lastMessage", timestamp);
        map.put("messageType",messageType);
        map.put("sender",sender);
        map.put("content",content);
        return map;
    }

    @Override
    public String toString() {
        return content;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }

    public Object getReadUsers() {
        return readUsers;
    }

    public void setReadUsers(List<String> readUsers) {
        this.readUsers = readUsers;
    }

    public static class Builder {

        private String uid;
        private String sender;
        private String content;
        private long timestamp;
        private String messageType;
        private String tokenId;

        public Builder setUid(String uid) {
            this.uid = uid;
            return this;
        }

        public Builder setSender(String sender) {
            this.sender = sender;
            return this;
        }

        public Builder setContent(String content) {
            this.content = content;
            return this;
        }

        public Builder setTimestamp(long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder setMessageType(String messageType) {
            this.messageType = messageType;
            return this;
        }

        public Builder setTokenId(String tokenId) {
            this.tokenId = tokenId;
            return this;
        }


        public Message createMessage() {
            return new Message(uid, sender, content, timestamp, messageType);
        }
    }
}
