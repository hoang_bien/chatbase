package com.biennhominext.chatbase.data.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Created by bien on 7/17/2017.
 */
@IgnoreExtraProperties
public class User extends BaseObject {
    public static final int GENDER_UNDEFINED = 0;
    public static final int GENDER_MALE = 1;
    public static final int GENDER_FEMALE = 2;
    public static final int FLAG_FRIEND_SENT = -1;
    public static final int FLAG_FRIEND = 0;
    public static final int FLAG_FRIEND_INVITED = 1;
    public static final int FLAG_FRIEND_NONE = 2;
    public static final String DEFAULT_IMAGE_URL = "https://firebasestorage.googleapis.com/v0/b/chatbase-1e83e.appspot.com/o/default-user.png?alt=media&token=f7d49b21-557d-4a80-a863-e528300b97e0";
    private String name;
    private String profileImage;
    private String email;

    private int gender;
    private long lastOnline;
    @Exclude
    private Integer isFriend;
    @Exclude
    private boolean isOnline;

    public User(String uid, String name, String profileImage, String email, int gender) {
        this.uid = uid;
        this.name = name;
        this.profileImage = profileImage;
        this.email = email;
        this.gender = gender;
    }

    public User() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof User)) {
            return false;
        }
        return uid.equals(((User) obj).uid);
    }

    @Exclude
    public HashMap<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("uid", uid);
        map.put("name", name);
        map.put("email", email);
        map.put("profileImage", profileImage);
        return map;
    }

    public Integer getFriendStatus() {
        return isFriend;
    }

    public void setFriendStatus(Integer friend) {
        isFriend = friend;
    }

    public long getLastOnline() {
        return lastOnline;
    }


    public void setLastOnline(long lastOnline) {
        this.lastOnline = lastOnline;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public static class Builder {

        private String uid;
        private String name;
        private String profileImage = User.DEFAULT_IMAGE_URL;
        private String email;
        private int gender;

        public Builder setUid(String uid) {
            this.uid = uid;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setProfileImage(String profileImage) {
            this.profileImage = profileImage;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setGender(int gender) {
            this.gender = gender;
            return this;
        }


        public User createUser() {
            return new User(uid, name, profileImage, email, gender);
        }
    }


}
