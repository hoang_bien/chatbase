package com.biennhominext.chatbase.data.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Created by hbien on 7/30/2017.
 */
@IgnoreExtraProperties
public class Group extends BaseObject {
    private String title;
    private String description;
    private String lastMessage ;
    private String messageType;
    private long timestamp;
    private String sender;
    private HashMap<String, User> users;
    @Exclude
    private boolean hasUnreadMessage;

    public Group(String uid, String title, String description, String lastMessage, String messageType, long timestamp, String sender) {
        this.uid = uid;
        this.title = title;
        this.description = description;
        this.lastMessage = lastMessage;
        this.messageType = messageType;
        this.timestamp = timestamp;
        this.sender = sender;
    }

    public Group() {
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public HashMap<String, User> getUsers() {
        return users;
    }

    public void setUsers(HashMap<String, User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Group)) {
            return false;
        }
        return uid.equals(((Group) obj).uid);
    }

    public void setHasUnreadMessage(boolean hasUnreadMessage) {
        this.hasUnreadMessage = hasUnreadMessage;
    }

    public boolean isHasUnreadMessage() {
        return hasUnreadMessage;
    }


    public static class Builder {


        private String uid;
        private String title;
        private String description;
        private String lastMessage;
        private String messageType;
        private long timestamp;
        private String sender;

        public Builder setUid(String uid) {
            this.uid = uid;
            return this;
        }


        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setLastMessage(String lastMessage) {
            this.lastMessage = lastMessage;
            return this;
        }

        public Builder setMessageType(String messageType) {
            this.messageType = messageType;
            return this;
        }

        public Builder setTimestamp(long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder setSender(String sender) {
            this.sender = sender;
            return this;
        }

        public Group createGroup() {
            return new Group(uid, title, description, lastMessage, messageType, timestamp, sender);
        }
    }
}
