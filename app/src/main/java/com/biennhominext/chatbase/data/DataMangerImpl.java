package com.biennhominext.chatbase.data;

import android.content.Context;

import com.biennhominext.chatbase.data.firebase.FirebaseHelper;
import com.biennhominext.chatbase.data.model.Group;
import com.biennhominext.chatbase.data.model.Message;
import com.biennhominext.chatbase.data.model.User;
import com.biennhominext.chatbase.data.prefs.PreferenceHelper;
import com.biennhominext.chatbase.di.ApplicationContext;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import vc908.stickerfactory.StickersManager;

/**
 * Created by bien on 7/17/2017.
 */

public class DataMangerImpl implements DataManager {
    private final PreferenceHelper mPref;
    private final Context mContext;
    private final FirebaseHelper mFirebaseHelper;
    private final FirebaseAuth mAuth;

    @Inject
    public DataMangerImpl(@ApplicationContext Context context,
                          PreferenceHelper pref,
                          FirebaseHelper firebaseHelper, FirebaseAuth firebaseAuth) {
        mPref = pref;
        mContext = context;
        mFirebaseHelper = firebaseHelper;
        mAuth = firebaseAuth;
    }


    @Override
    public void logout() {
        final String currentUserId = getCurrentUserId();
        mAuth.signOut();
        mPref.clearUserData();
        mFirebaseHelper.removeToken(currentUserId);
    }

    @Override
    public void addMembersToGroup(List<String> memberIds, String groupId, boolean isNewlyCreated) {
        mFirebaseHelper.addPeopleToGroup(memberIds, groupId, isNewlyCreated ? null : getCurrentUserId());
    }

    @Override
    public Flowable<Group> getMyGroups() {
        return mFirebaseHelper.getGroupsForUser(getCurrentUserId());
    }

    @Override
    public Flowable<Group> getGroupInfo(String groupId) {
        return mFirebaseHelper.getGroupInfo(groupId, getCurrentUserId());
    }

    @Override
    public Flowable<Message> getGroupConversationForUser(String groupId) {
        return mFirebaseHelper.getGroupConversationForUser(getCurrentUserId(), groupId);
    }

    @Override
    public void sendPlainTextMessageToGroup(String message, String groupId) {
        mFirebaseHelper.sendPlainTextMessageToGroup(message, getCurrentUserId(), groupId);
    }

    @Override
    public Single<String> getMyToken() {
        return mFirebaseHelper.getUserToken(getCurrentUserId());
    }

    @Override
    public void setMyToken(String token) {
        mFirebaseHelper.setUserToken(getCurrentUserId(), token);
    }

    @Override
    public Flowable<User> getAllUsers() {
        return mFirebaseHelper.getAllUsers(getCurrentUserId());
    }

    @Override
    public void toggleFriendship(User user) {
        //mFirebaseHelper.toggleFriendship(getCurrentUserId(),user.getUid(),user.getFriendStatus());
    }

    @Override
    public Flowable<User> findUserByName(String query) {
        return mFirebaseHelper.findUserByName(getCurrentUserId(), query);
    }

    @Override
    public void markUserMessageAsRead(String senderId, String msgId) {
        mFirebaseHelper.markUserMessageAsRead(getCurrentUserId(), senderId, msgId);
    }

    @Override
    public Flowable<User> getUsersInGroup(String groupId) {
        return mFirebaseHelper.getUsersInGroup(groupId);
    }

    @Override
    public void initializeOnlinePresence() {
        mFirebaseHelper.initializeOnlinePresence(getCurrentUserId());
    }

    @Override
    public Flowable<Message> getMyInvites() {
        return mFirebaseHelper.getInvitesForUser(getCurrentUserId());
    }

    @Override
    public void ignoreInvitation(String userId) {
        mFirebaseHelper.ignoreInvitation(getCurrentUserId(), userId);
    }

    @Override
    public void acceptInvitation(String userId) {
        mFirebaseHelper.acceptInvitation(getCurrentUserId(), userId);
    }

    @Override
    public Single<Message> getInviteMessageFromUser(String uid) {
        return mFirebaseHelper.getInviteMessageFromUser(getCurrentUserId(), uid);
    }

    @Override
    public void sendStickerMessage(String code, String targetId, boolean isGroup) {
        mFirebaseHelper.sendStickerMessage(code, getCurrentUserId(), targetId, isGroup);
    }

    @Override
    public Flowable<Long> getCurrentInvitesCount() {
        return mFirebaseHelper.getInvitesCount(getCurrentUserId());
    }


    @Override
    public void setCurrentConversationId(String currentConversationId) {
        mPref.setCurrentConversationId(currentConversationId);
    }

    @Override
    public void clearCurrentConversationId() {
        mPref.clearCurrentConversationId();
    }

    @Override
    public boolean isConversationVisible(String conversationId) {
        return mPref.getCurrentConversationId().equals(conversationId);
    }

    @Override
    public void markGroupMessageAsRead(String groupId, String msgId) {
        mFirebaseHelper.markGroupMessageAsRead(getCurrentUserId(), groupId, msgId);
    }

    @Override
    public void sendLocationMessage(double latitude, double longitude, String targetId, boolean isGroup) {
        mFirebaseHelper.sendLocationMessage(latitude, longitude, getCurrentUserId(), targetId, isGroup);
    }

    @Override
    public void leaveGroup(String groupId) {
        mFirebaseHelper.leaveGroup(getCurrentUserId(), groupId);
    }

    @Override
    public Flowable<String> getLastMessageStatus(String messageId, String targetId, boolean isGroup) {
        return mFirebaseHelper.getLastMessageStateForUser(getCurrentUserId(),messageId,targetId,isGroup);
    }


    @Override
    public Flowable<User> getAndCacheUser(String userId) {
        return mFirebaseHelper.getUser(userId).doOnNext(this::cacheUser);
    }


    @Override
    public Single<String> uploadImage(String bitmap) {
        return mFirebaseHelper.uploadImage(bitmap);
    }

    @Override
    public boolean isUserLogined() {
        return mPref.getCurrentUserId() != null;
    }

    @Override
    public Completable doGoogleLoginCall(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);

        return Completable.create(e -> {
            mAuth.signInWithCredential(credential)
                    .addOnSuccessListener(authResult -> {
                        setCurrentUserId(mAuth.getCurrentUser().getUid());
                        setCurrentUserName(account.getDisplayName());
                        setCurrentProfileImage(account.getPhotoUrl().toString());
                        setCurrentUserEmail(account.getEmail());
                        doAfterLogin(mAuth.getCurrentUser().getUid());
                        e.onComplete();
                    }).addOnFailureListener(e1 -> {
                e.onError(e1);
            });
        });
    }

    void doAfterLogin(String userId) {
        Map<String, String> meta = new HashMap<>();
        // Put your user id when you know it
        StickersManager.setUserID(userId);
    }

    @Override
    public Flowable<User> getAccountInfo() {
        if (isUserLogined()) {
            return getAndCacheUser(mPref.getCurrentUserId());
        }
        return null;
    }


    @Override
    public Single<User> updateUserInfo(User user) {
        mPref.setCurrentUserId(user.getUid());
        mPref.setCurrentProfileImage(user.getProfileImage());
        mPref.setCurrentUserEmail(user.getEmail());
        mPref.setCurrentUserName(user.getName());
        return mFirebaseHelper.insertUser(user);
    }

    @Override
    public String getCurrentUserId() {
        return mPref.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(String userId) {
        mPref.setCurrentUserId(userId);
    }

    @Override
    public String getCurrentUserName() {

        return mPref.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String name) {
        mPref.setCurrentUserName(name);
    }

    @Override
    public void setCurrentProfileImage(String url) {
        mPref.setCurrentProfileImage(url);
    }

    @Override
    public String getCurrentProfileImage() {
        return mPref.getCurrentProfileImage();
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPref.setCurrentUserEmail(email);
    }

    @Override
    public String getCurrentUSerEmail() {
        return mPref.getCurrentUSerEmail();
    }

    private void cacheUser(User user) {
        setCurrentUserId(user.getUid());
        setCurrentUserEmail(user.getEmail());
        setCurrentUserName(user.getName());
        setCurrentProfileImage(user.getProfileImage());
    }

    @Override
    public int getCurrentUserGender() {
        return mPref.getCurrentUserGender();
    }

    @Override
    public void setCurrentUserGender(int gender) {
        mPref.setCurrentUserGender(gender);
    }

    @Override
    public Flowable<Message> getMyChat() {
        return mFirebaseHelper.getChatForUser(mPref.getCurrentUserId());
    }

    @Override
    public Flowable<User> getMyContacts() {
        return mFirebaseHelper.getContactsForUser(mPref.getCurrentUserId());
    }

    @Override
    public Flowable<User> getContactsNotInGroup(String groupId) {
        return mFirebaseHelper.getPeopleNotInGroup(getCurrentUserId(), groupId);
    }

    @Override
    public Completable doLoginWithEmailAndPassword(String email, String password) {
        return Completable.create(e ->
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                final String uid = task.getResult().getUser().getUid();
                                setCurrentUserId(uid);
                                setCurrentUserEmail(task.getResult().getUser().getEmail());
                                setCurrentProfileImage(User.DEFAULT_IMAGE_URL);
                                e.onComplete();
                                doAfterLogin(uid);
                            } else {
                                e.onError(task.getException().getCause());
                            }
                        }));
    }

    @Override
    public Flowable<User> getPublicUserInfo(String targetId) {
        return mFirebaseHelper.getUserWithFriendStatus(targetId, getCurrentUserId());
    }

    @Override
    public Flowable<Message> getConversationForUser(String targetId) {
        return mFirebaseHelper.getConversationForUser(mPref.getCurrentUserId(), targetId);
    }

    @Override
    public void sendPlainTextMessageToUser(String message, String receiver) {
        mFirebaseHelper.sendPlainTextMessageToUser(message, getCurrentUserId(), receiver);
    }

    @Override
    public Message sendMultipleImageMessage(List<String> paths, String destination, boolean isGroup) {
        return mFirebaseHelper.sendMultipleImagesMessage(paths, getCurrentUserId(), destination, isGroup);
    }

    @Override
    public Message sendSingleImageMessage(String path, String destination, boolean isGroup) {
        return mFirebaseHelper.sendSingleImageMessage(path, getCurrentUserId(), destination, isGroup);
    }

    @Override
    public String createNewGroup(String groupName, String groupDescription) {
        return mFirebaseHelper.createNewGroup(getCurrentUserId(), groupName, groupDescription);

    }
}
